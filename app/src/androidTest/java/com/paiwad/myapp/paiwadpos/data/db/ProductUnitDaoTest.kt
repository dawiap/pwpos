package com.paiwad.myapp.paiwadpos.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductUnitDao
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ProductUnitDaoTest {

    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var database: PWPOSDatabase
    private lateinit var unitDao: ProductUnitDao

    private val fakeUnits = listOf(
        UnitEntity(1,"xyz","xyz","test"),
        UnitEntity(2,"abc","abc","testABC")
    )

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PWPOSDatabase::class.java
        ).allowMainThreadQueries().build()
        unitDao = database.unitDao()
    }

    @Test
    fun insertProductUnitList() = runBlockingTest {
        unitDao.insertUnitList(fakeUnits)

        val allUnits = unitDao.getUnits()

        Truth.assertThat(allUnits).isEqualTo(allUnits)
    }

    @Test
    fun clearProductUnit() = runBlockingTest{
        unitDao.insertUnitList(fakeUnits)
        unitDao.clearAllUnit()

        val allUnits = unitDao.getUnits()

        Truth.assertThat(allUnits).isEmpty()
    }

    @After
    fun tearDown() {
        database.close()
    }
}