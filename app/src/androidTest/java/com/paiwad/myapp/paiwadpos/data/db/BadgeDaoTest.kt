package com.paiwad.myapp.paiwadpos.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.paiwad.myapp.paiwadpos.data.db.dao.BadgeDao
import com.paiwad.myapp.paiwadpos.data.db.dao.OptionDao
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductDao
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductUnitDao
import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class BadgeDaoTest {

    @get: Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: PWPOSDatabase
    private lateinit var badgeDao: BadgeDao
    private lateinit var productDao: ProductDao
    private lateinit var optionDao: OptionDao
    private lateinit var unitDao: ProductUnitDao

    private val fakeBadgeProduct1 = BadgeEntity(1,"xyz1",2,"opt001","xyz1",10.0)
    private val fakeBadgeProduct2 = BadgeEntity(2,"xyz2",5,"opt002","xyz1",20.0)

    private val fakeProducts = listOf(
        ProductEntity(1,"xyz1","c001","u001","test","",0.0,0,0,0,"xyz",
            isStock = false,
            isOption = true
        ),
        ProductEntity(2,"xyz2","c002","u002","test2","",0.0,5,0,0,"xyz",
            isStock = false,
            isOption = true
        )
    )

    private val fakeUnits = listOf(
        UnitEntity(1,"u001","xyz","test"),
        UnitEntity(2,"u002","abc","testABC")
    )

    private val fakeOptions = listOf(
        OptionEntity(1,"opt001","xyz1","test",10.0),
        OptionEntity(2,"opt002","xyz1","testABC",20.0),
        OptionEntity(3,"opt003","xyz2","testDEF",30.0)
    )

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PWPOSDatabase::class.java
        ).allowMainThreadQueries().build()
        badgeDao = database.badgeDao()
        productDao = database.productDao()
        unitDao = database.unitDao()
        optionDao = database.optionDao()
    }

    @Test
    fun insertProductToBadge() = runBlockingTest {
        insertProductsWithUnitsAndOptions()

        badgeDao.insertProductToBadge(fakeBadgeProduct1)
        badgeDao.insertProductToBadge(fakeBadgeProduct2)

        val allBadge = badgeDao.getBadgesWithProductsAndOptions()

        Truth.assertThat(allBadge.filter { it.badge == fakeBadgeProduct1 }).isNotEmpty()
        Truth.assertThat(allBadge.filter { it.badge == fakeBadgeProduct2 }).isNotEmpty()
    }

    @Test
    fun removeProductInBadge() = runBlockingTest {
        insertProductsWithUnitsAndOptions()

        badgeDao.insertProductToBadge(fakeBadgeProduct1)
        badgeDao.insertProductToBadge(fakeBadgeProduct2)

        badgeDao.removeProductInBadge(fakeBadgeProduct1)

        val allBadge = badgeDao.getBadgesWithProductsAndOptions()

        Truth.assertThat(allBadge.filter { it.badge == fakeBadgeProduct1 }).isEmpty()
        Truth.assertThat(allBadge.filter { it.badge == fakeBadgeProduct2 }).isNotEmpty()
    }

    @Test
    fun hasProductWhitProductCode() = runBlockingTest {
        insertProductsWithUnitsAndOptions()

        badgeDao.insertProductToBadge(fakeBadgeProduct1)

        val hasProduct = badgeDao.hasProductWhitProductCode("xyz1")

        Truth.assertThat(hasProduct).isEqualTo(fakeBadgeProduct1)
    }

    @Test
    fun hasProductWhitOptionCode() = runBlockingTest {
        insertProductsWithUnitsAndOptions()

        badgeDao.insertProductToBadge(fakeBadgeProduct1)

        val hasProduct = badgeDao.hasProductWhitOptionCode("opt001")

        Truth.assertThat(hasProduct).isEqualTo(fakeBadgeProduct1)
    }

    @Test
    fun updateProductCountInBadge() = runBlockingTest {
        insertProductsWithUnitsAndOptions()

        badgeDao.insertProductToBadge(fakeBadgeProduct1)
        badgeDao.insertProductToBadge(fakeBadgeProduct2)

        badgeDao.updateProductCountInBadge(10,1)
        badgeDao.updateProductCountInBadge(20,2)

        val hasProduct1 = badgeDao.hasProductWhitProductCode("xyz1")
        val hasProduct2 = badgeDao.hasProductWhitProductCode("xyz2")

        Truth.assertThat(hasProduct1?.badgeCount).isEqualTo(10)
        Truth.assertThat(hasProduct2?.badgeCount).isEqualTo(20)
    }

    @Test
    fun clearAllBadge() = runBlockingTest {
        insertProductsWithUnitsAndOptions()

        badgeDao.insertProductToBadge(fakeBadgeProduct1)
        badgeDao.insertProductToBadge(fakeBadgeProduct2)

        badgeDao.clearBadge()

        val allBadge = badgeDao.getBadgesWithProductsAndOptions()

        Truth.assertThat(allBadge).isEmpty()
    }

    private fun insertProductsWithUnitsAndOptions() = runBlockingTest{
        productDao.insertProductList(fakeProducts)
        unitDao.insertUnitList(fakeUnits)
        optionDao.insertOptionList(fakeOptions)
    }

    @After
    fun tearDown() {
        database.close()
    }
}