package com.paiwad.myapp.paiwadpos.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.paiwad.myapp.paiwadpos.data.db.dao.CategoryDao
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class CategoryDaoTest {

    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var database: PWPOSDatabase
    private lateinit var categoryDao: CategoryDao

    private val fakeCategory = listOf(
        CategoryEntity(1,"xyz","test","#00000","xyz"),
        CategoryEntity(2,"abc","testABC","#00000","abc")
    )

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PWPOSDatabase::class.java
        ).allowMainThreadQueries().build()
        categoryDao = database.categoryDao()
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun insertCategory() = runBlockingTest {
        val category = fakeCategory.first()
        categoryDao.insertCategory(category)

        val allCategory = categoryDao.getCategories()

        Truth.assertThat(allCategory).contains(category)
    }

    @Test
    fun insertCategoryList() = runBlockingTest{
        categoryDao.insertCategoryList(fakeCategory)

        val allCategory = categoryDao.getCategories()

        Truth.assertThat(allCategory).isEqualTo(fakeCategory)
    }

    @Test
    fun clearCategory() = runBlockingTest {
        categoryDao.insertCategoryList(fakeCategory)
        categoryDao.clearAllCategory()

        val allCategory = categoryDao.getCategories()

        Truth.assertThat(allCategory).isEmpty()
    }
}