package com.paiwad.myapp.paiwadpos.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.paiwad.myapp.paiwadpos.data.db.dao.OptionDao
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductDao
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductUnitDao
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ProductDaoAndOptionDaoTest {

    @get: Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: PWPOSDatabase
    private lateinit var productDao: ProductDao
    private lateinit var optionDao: OptionDao
    private lateinit var unitDao: ProductUnitDao

    private val fakeProducts = listOf(
        ProductEntity(1,"xyz1","c001","u001","test","",0.0,0,0,0,"xyz",
            isStock = false,
            isOption = true
        ),
        ProductEntity(2,"xyz2","c002","u002","test2","",0.0,5,0,0,"xyz",
            isStock = false,
            isOption = true
        )
    )

    private val fakeUnits = listOf(
        UnitEntity(1,"u001","xyz","test"),
        UnitEntity(2,"u002","abc","testABC")
    )

    private val fakeOptions = listOf(
        OptionEntity(1,"opt001","xyz1","test",10.0),
        OptionEntity(2,"opt002","xyz1","testABC",20.0),
        OptionEntity(3,"opt003","xyz2","testDEF",30.0)
    )

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PWPOSDatabase::class.java
        ).allowMainThreadQueries().build()
        productDao = database.productDao()
        optionDao = database.optionDao()
        unitDao = database.unitDao()
    }

    @Test
    fun insetProductsWithUnitsAndOptions() = runBlockingTest {
        productDao.insertProductList(fakeProducts)
        unitDao.insertUnitList(fakeUnits)
        optionDao.insertOptionList(fakeOptions)

        val allProduct = productDao.getProducts()

        Truth.assertThat(allProduct.filter{it.product.product == fakeProducts.first()}).isNotEmpty()
        Truth.assertThat(allProduct.filter{it.product.unit == fakeUnits.first()}).isNotEmpty()
        Truth.assertThat(allProduct.first { it.product.product.isOption }.options).isEqualTo(fakeOptions.dropLast(1))
        Truth.assertThat(allProduct.last() { it.product.product.isOption }.options).isEqualTo(fakeOptions.drop(2))
    }

    @Test
    fun getProductsWithUnitsAndOptionsByCategoryCode() = runBlockingTest {
        productDao.insertProductList(fakeProducts)
        unitDao.insertUnitList(fakeUnits)
        optionDao.insertOptionList(fakeOptions)

        val allProductByCategoryCodeC001 = productDao.getProductsWithUnitAndOptions("c001")
        val allProductByCategoryCodeC002 = productDao.getProductsWithUnitAndOptions("c002")
        val allProducts = productDao.getProducts()

        Truth.assertThat(allProductByCategoryCodeC001).containsAnyIn(allProducts)
        Truth.assertThat(allProductByCategoryCodeC002).containsAnyIn(allProducts)
    }

    @Test
    fun clearAllProduct() = runBlockingTest {
        productDao.insertProductList(fakeProducts)
        productDao.clearAllProduct()

        val allProduct = productDao.getProducts()

        Truth.assertThat(allProduct).isEmpty()
    }

    @Test
    fun clearAllOption() = runBlockingTest {
        optionDao.insertOptionList(fakeOptions)
        optionDao.clearAllOption()

        var allOption = optionDao.getOptions("xyz1")

        Truth.assertThat(allOption).isEmpty()

        allOption = optionDao.getOptions("xyz2")

        Truth.assertThat(allOption).isEmpty()
    }

    @After
    fun tearDown() {
        database.close()
    }
}