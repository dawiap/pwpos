package com.paiwad.myapp.paiwadpos.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.paiwad.myapp.paiwadpos.data.db.dao.UserDao
import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class UserDaoTest {

    @get: Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: PWPOSDatabase
    private lateinit var userDao: UserDao

    private val fakeUser = UserEntity(1,"xyz","@mail.com","test","0","",1,"2538")

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            PWPOSDatabase::class.java
        ).allowMainThreadQueries().build()
        userDao = database.userDao()
    }

    @Test
    fun insertUser() = runBlockingTest {
        userDao.insertUser(fakeUser)

        val user = userDao.getUser()

        Truth.assertThat(user).isEqualTo(fakeUser)
    }

    @Test
    fun checkYourPINReturnCorrect() = runBlockingTest{
        userDao.insertUser(fakeUser)

        val isCorrectPIN = userDao.checkYourPIN("2538")

        Truth.assertThat(isCorrectPIN).isEqualTo(1)
    }

    @Test
    fun checkYourPINReturnInCorrect() = runBlockingTest{
        userDao.insertUser(fakeUser)

        val isCorrectPIN = userDao.checkYourPIN("1234")

        Truth.assertThat(isCorrectPIN).isEqualTo(0)
    }

    @Test
    fun clearAllUser() = runBlockingTest {
        userDao.insertUser(fakeUser)
        userDao.clearUser()

        val user = userDao.getUser()

        Truth.assertThat(user).isNull()
    }

    @After
    fun tearDown() {
        database.close()
    }
}