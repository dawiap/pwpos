package com.paiwad.myapp.paiwadpos.data.repository

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import io.reactivex.Completable
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow

class FakeCategoryRepository: CategoryRepository {
    override suspend fun getCategories(branchId: String): Flow<Result<List<CategoryUI>>> {
        TODO("Not yet implemented")
    }

    override fun addCategory(categoryUI: CategoryUI): Completable {
        TODO("Not yet implemented")
    }

    override fun deleteCategory(categoryUI: CategoryUI): Completable {
        TODO("Not yet implemented")
    }

    override fun checkProductBeforeDeleteCategory(categoryId: String): Observable<Boolean> {
        TODO("Not yet implemented")
    }

    override fun updateCategoryInFb(categoryUI: CategoryUI): Completable {
        TODO("Not yet implemented")
    }

    override suspend fun updateCache(branchId: String): Flow<Result<List<CategoryModel>>> {
        TODO("Not yet implemented")
    }
}