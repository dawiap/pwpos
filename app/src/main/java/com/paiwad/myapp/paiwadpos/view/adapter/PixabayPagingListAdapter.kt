package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.databinding.ViewPixabayItemBinding

class PixabayPagingListAdapter: PagingDataAdapter<Hit, PixabayPagingListAdapter.PixabayItemViewHolder>(
    CharacterComparator
) {

    lateinit var onItemClick: ((Hit) -> Unit)

    override fun onBindViewHolder(holder: PixabayItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, onItemClick) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PixabayItemViewHolder {
        return PixabayItemViewHolder(
            ViewPixabayItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    inner class PixabayItemViewHolder(private val binding: ViewPixabayItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Hit, clickListener: (Hit) -> Unit) = with(binding) {

            hit = item
            root.setOnClickListener {
                clickListener(item)
            }

        }
    }

    object CharacterComparator : DiffUtil.ItemCallback<Hit>() {
        override fun areItemsTheSame(oldItem: Hit, newItem: Hit) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Hit, newItem: Hit) =
            oldItem == newItem
    }
}

