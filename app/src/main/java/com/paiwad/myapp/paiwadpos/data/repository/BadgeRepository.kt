package com.paiwad.myapp.paiwadpos.data.repository

import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.util.state.Result
import kotlinx.coroutines.flow.Flow

interface BadgeRepository {

    suspend fun getBadge(): Flow<Result<List<BadgeWithProduct>>>

    suspend fun updateProductInBadge(products: BadgeEntity): Flow<Result<Boolean>>

    suspend fun addProductToBadge(products: BadgeEntity): Flow<Result<Boolean>>

    suspend fun clearBadge()
}