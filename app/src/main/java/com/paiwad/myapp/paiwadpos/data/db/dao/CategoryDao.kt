package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity

@Dao
interface CategoryDao {

    @Query("SELECT *FROM category")
    suspend fun getCategories(): List<CategoryEntity>

    @Insert
    suspend fun insertCategory(category: CategoryEntity): Long

    @Insert
    suspend fun insertCategoryList(categories: List<CategoryEntity>): List<Long>

    @Query("DELETE FROM category")
    suspend fun clearAllCategory()
}