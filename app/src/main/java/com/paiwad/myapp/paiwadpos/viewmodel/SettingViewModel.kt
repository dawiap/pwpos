package com.paiwad.myapp.paiwadpos.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.repository.ProfileSettingRepository
import com.paiwad.myapp.paiwadpos.view.model.MenuSettingUI
import com.paiwad.myapp.paiwadpos.view.model.UserUI
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class SettingViewModel @Inject constructor(private val repository: ProfileSettingRepository): ViewModel() {

    private val _itemsSetting = MutableLiveData<List<MenuSettingUI>>()
    val itemsSetting: LiveData<List<MenuSettingUI>> = _itemsSetting

    private val _message = MutableLiveData<String?>()
    val message: LiveData<String?> = _message

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    private val _viewState = MutableLiveData<ViewState<Nothing>>()
    val viewState: LiveData<ViewState<Nothing>> = _viewState

    private val compositeDisposable = CompositeDisposable()

    private val _userUI = MutableLiveData<UserUI>()
    val userUI: LiveData<UserUI> = _userUI

    fun setUserUI(userUI: UserUI){
        _userUI.postValue(userUI)
    }

    fun setItemsSetting(items: List<MenuSettingUI>){
        _itemsSetting.postValue(items)
    }

    fun uploadChangeImage(uri: Uri){
        _viewState.postValue(ViewState.Loading)
        val disposable = repository.uploadChangeImageUser(uri)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _viewState.postValue(ViewState.Loaded(null))
            },{
                _viewState.postValue(ViewState.LoadFailure(it.message.toString()))
            })
        compositeDisposable.add(disposable)
    }

    fun updateProfile(editorType: EditorType, editName: String){
        if(_userUI.value == null){
            _viewState.postValue(ViewState.LoadFailure("UserUI is Null"))
            return
        }

        when(editorType){
            EditorType.BRANCH -> {
                _userUI.value?.branchName = editName
            }
            EditorType.PHONE -> {
                _userUI.value?.phone = editName
            }
            EditorType.EMAIL -> {
                _userUI.value?.email = editName
            }
            EditorType.PIN -> {
                _userUI.value?.PIN = editName
            }
        }

        _viewState.postValue(ViewState.Loading)
        val disposable = repository.updateUserProfile(_userUI.value!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _viewState.postValue(ViewState.Loaded(null))
            },{
                _viewState.postValue(ViewState.LoadFailure(it.message.toString()))
            })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}

sealed class ViewState<out T : Any> {
    object Loading: ViewState<Nothing>()
    data class Loaded<out T : Any>(val data: T?): ViewState<T>()
    data class LoadFailure(val errorMessage: String): ViewState<Nothing>()
}

enum class EditorType{
    BRANCH,
    PHONE,
    EMAIL,
    PIN
}