package com.paiwad.myapp.paiwadpos.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.FragmentOrderBinding
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.adapter.OrderListAdapter
import com.paiwad.myapp.paiwadpos.view.adapter.ProductCardAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OrderFragment : Fragment() {

    lateinit var orderViewModel: OrderViewModel

    lateinit var userViewModel: UserViewModel

    @Inject
    lateinit var orderListAdapter: OrderListAdapter

    @Inject
    lateinit var productCardAdapter: ProductCardAdapter

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    private lateinit var binding: FragmentOrderBinding

    private var lastTab = 0
    companion object{
        private const val LAST_TAB_KEY = "com.paiwad.myapp.paiwadpos.view.ui.last_tab_key"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        orderViewModel = ViewModelProvider(requireActivity()).get(OrderViewModel::class.java)

        userViewModel = ViewModelProvider(requireActivity()).get(UserViewModel::class.java)

        return inflater.inflate(R.layout.fragment_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentOrderBinding.bind(view)
        binding.viewModel = orderViewModel
        binding.lifecycleOwner = this

        orderViewModel.message.observeMessage()
        orderViewModel.loading.observeLoading()

        userViewModel.userProfile.observe(viewLifecycleOwner,{user ->
            user?.let {
                orderViewModel.getOrderByTabMenu(it.uid, OrderTabType.NOT_HAVE_PAYMENT)
                getTebSelected(it.uid)
            }
        })

        setAdapterList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        orderViewModel.saveStateLastTab(lastTab)
    }

    override fun onResume() {
        super.onResume()

        orderViewModel.lastTab.observe(viewLifecycleOwner,{
            binding.tabLayout.selectTab(binding.tabLayout.getTabAt(it))
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        orderViewModel.saveStateLastTab(0)
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading(){
        this.observe(viewLifecycleOwner, SingleEventObserver{
            if (it) {
                if (progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    private fun setAdapterList() {
        binding.rcvOrder.adapter = orderListAdapter
        orderListAdapter.clickListener = { _, order, _ ->
            val directions = OrderFragmentDirections.actionNavOrderToOrderDetailActivity(order)
            findNavController().navigate(directions)
        }
    }

    private fun getTebSelected(branchId: String){
        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                lastTab = tab!!.position
                when (tab.position) {
                    0 -> {
                        orderViewModel.getOrderByTabMenu(branchId, OrderTabType.NOT_HAVE_PAYMENT)
                    }
                    1 -> {
                        orderViewModel.getOrderByTabMenu(branchId, OrderTabType.HAVE_PAYMENT)
                    }
                    else -> {
                        orderViewModel.getOrderByTabMenu(branchId, OrderTabType.CANCEL_ORDER)
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }

    enum class OrderTabType{
        NOT_HAVE_PAYMENT,
        HAVE_PAYMENT,
        CANCEL_ORDER
    }
}