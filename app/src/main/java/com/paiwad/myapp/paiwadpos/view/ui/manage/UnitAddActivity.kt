package com.paiwad.myapp.paiwadpos.view.ui.manage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityUnitAddBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomSheetManageUnitBinding
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ManagePopupMenu
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.adapter.UnitAddItemAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.UnitViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class UnitAddActivity : AppCompatActivity(), UserAuthListener, ManagePopupMenu.PopupClickListener {

    private val userViewModel: UserViewModel by viewModels()

    private val unitViewModel: UnitViewModel by viewModels()

    private lateinit var binding: ActivityUnitAddBinding

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    @Inject
    lateinit var alertDialogBuilder: AlertDialogBuilder

    @Inject
    lateinit var unitAddItemAdapter: UnitAddItemAdapter

    private lateinit var bottomSheetView: BottomSheetManageUnitBinding

    private lateinit var bottomDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_unit_add)
        binding.lifecycleOwner = this
        binding.viewModel = unitViewModel

        userViewModel.getUserProfile()
        userViewModel.userProfile.observe(this,{user ->
            user?.let {
                unitViewModel.branchId = it.uid

                unitViewModel.getUnitsFromFb(it.uid)
            }
        })

        unitViewModel.authListener = this

        binding.imageClose.setOnClickListener { finish() }

        binding.buttonAdd.setOnClickListener {
            unitViewModel._isActionAdd.value = true
            unitViewModel.unitName.value = null
            unitViewModel._titleUnitManage.value = resources.getString(R.string.text_button_add_unit)
            bottomDialog.show()
        }

        setupListAdapter()
        setupBottomSheet()
    }

    private fun setupListAdapter(){
        binding.rcvUnit.adapter = unitAddItemAdapter
        unitAddItemAdapter.clickListener = {view, productUnit, i ->

            ManagePopupMenu<UnitUI>(this).apply {

                showPopupMenu(view, R.menu.manage_context_menu, productUnit, i)

                setListener(this@UnitAddActivity)

            }
        }
    }

    private fun setupBottomSheet() {
        bottomSheetView = BottomSheetManageUnitBinding.inflate(layoutInflater, null, false)
        bottomSheetView.viewModel = unitViewModel
        bottomSheetView.lifecycleOwner = this

        bottomDialog = BottomSheetDialog(this).apply {
            setContentView(bottomSheetView.root)
        }

        bottomSheetView.imageClose.setOnClickListener{bottomDialog.dismiss()}
    }

    override fun onResume() {
        super.onResume()
        unitViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun onSuccess(successType: SuccessType) {
        if (successType == SuccessType.DATA_SUCCESS)
            bottomDialog.dismiss()
        if(successType == SuccessType.WARING_HAS_DATA)
            alertDialogBuilder.showMessageAlertDialog("Failure", resources.getString(R.string.message_have_data_in_unit))
    }

    override fun onFailure(message: String) {
        alertDialogBuilder.showMessageAlertDialog("Failure", message)
    }

    override fun editClicked(item: Any?) {
        (item as UnitUI).apply {
            unitViewModel.unitItem = this
            unitViewModel.unitName.value = this.unitName
            unitViewModel._titleUnitManage.value =  resources.getString(R.string.text_button_edit_unit)
        }
        unitViewModel._isActionAdd.value = false
        bottomDialog.show()
    }

    override fun deleteClicked(item: Any?, position: Int) {
        unitViewModel.checkDataBeforeDelete((item as UnitUI))
        unitAddItemAdapter.notifyItemChanged(position)
    }
}