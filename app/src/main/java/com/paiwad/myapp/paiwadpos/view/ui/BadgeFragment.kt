package com.paiwad.myapp.paiwadpos.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityBadgeBinding
import com.paiwad.myapp.paiwadpos.databinding.FragmentBadgeBinding
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.adapter.BadgeProductListAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.BadgeViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BadgeFragment : Fragment(), View.OnClickListener, UserAuthListener {

    private lateinit var badgeViewModel: BadgeViewModel

    private lateinit var userViewModel: UserViewModel

    private lateinit var orderViewModel: OrderViewModel

    private lateinit var badgeProductListAdapter: BadgeProductListAdapter

    private lateinit var progressBuilder: ProgressBuilder

    private lateinit var alertDialogBuilder: AlertDialogBuilder

    private lateinit var binding: FragmentBadgeBinding

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.fade)
        return inflater.inflate(R.layout.fragment_badge, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as BadgeActivity).apply {
            this@BadgeFragment.badgeViewModel = badgeViewModel
            this@BadgeFragment.userViewModel = userViewModel
            this@BadgeFragment.orderViewModel = orderViewModel
            this@BadgeFragment.badgeProductListAdapter = badgeProductListAdapter
            this@BadgeFragment.progressBuilder = progressBuilder
            this@BadgeFragment.alertDialogBuilder = alertDialogBuilder
        }
        binding = FragmentBadgeBinding.bind(view)
        binding.viewModel = badgeViewModel
        binding.lifecycleOwner = this

        badgeViewModel.getBadge()

        badgeProductListAdapter.badgeViewModel = badgeViewModel

        binding.rcvProduct.adapter = badgeProductListAdapter

        badgeViewModel.message.observeMessage()
        orderViewModel.message.observeMessage()

        badgeViewModel.loading.observeLoading()
        orderViewModel.loading.observeLoading()

        binding.imageClose.setOnClickListener(this)
        binding.buttonAfterPayment.setOnClickListener(this)
        orderViewModel.authListener = this
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.imageClose -> {
                requireActivity().finish()
            }
            binding.buttonAfterPayment -> {
                orderViewModel.addOrder(badgeViewModel.toOrderModelMapper())
            }
        }
    }

    override fun onSuccess(successType: SuccessType) {
        badgeViewModel.clearBadge()
        userViewModel.userProfile.observe(viewLifecycleOwner, { user ->
            user?.let {
                orderViewModel.getOrders(it.uid)
                println(it.uid)
            }
        })
        orderViewModel.orders.observe(viewLifecycleOwner, {
            println(it)
            val directions = BadgeFragmentDirections.actionBadgeFragmentToPreviewFragment(order = it.last())
            findNavController().navigate(directions)
        })
    }

    override fun onFailure(message: String) {
        TODO("Not yet implemented")
    }
}