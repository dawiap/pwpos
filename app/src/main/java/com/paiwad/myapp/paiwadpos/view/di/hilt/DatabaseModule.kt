package com.paiwad.myapp.paiwadpos.view.di.hilt

import android.app.Application
import androidx.room.Room
import com.paiwad.myapp.paiwadpos.data.db.*
import com.paiwad.myapp.paiwadpos.data.db.dao.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Singleton
    @Provides
    fun providePWPOSDatabase(context: Application): PWPOSDatabase{
        return Room.databaseBuilder(context, PWPOSDatabase::class.java, "pwposdb")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(database: PWPOSDatabase): UserDao {
        return database.userDao()
    }

    @Singleton
    @Provides
    fun provideCategoryDao(database: PWPOSDatabase): CategoryDao {
        return database.categoryDao()
    }

    @Singleton
    @Provides
    fun provideProductDao(database: PWPOSDatabase): ProductDao {
        return database.productDao()
    }

    @Singleton
    @Provides
    fun provideOptionDao(database: PWPOSDatabase): OptionDao {
        return database.optionDao()
    }

    @Singleton
    @Provides
    fun provideProductUnitDao(database: PWPOSDatabase): ProductUnitDao {
        return database.unitDao()
    }

    @Singleton
    @Provides
    fun provideBadgeDao(database: PWPOSDatabase): BadgeDao {
        return database.badgeDao()
    }
}