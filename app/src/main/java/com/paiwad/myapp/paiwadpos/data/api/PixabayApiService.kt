package com.paiwad.myapp.paiwadpos.data.api

import com.paiwad.myapp.paiwadpos.BuildConfig
import com.paiwad.myapp.paiwadpos.data.api.model.PixabayResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface PixabayApiService {

    @GET("/api")
    suspend fun searchImageFromPixabay(
        @Query("q") q: String,
        //@Query("page") page: Int,
        @Query( "key") apikey: String = BuildConfig.API_KEY,
        //@Query("image_type") imageType: String = "photo",
        //@Query("category") category: String = "food"
    ): Response<PixabayResponse>

}