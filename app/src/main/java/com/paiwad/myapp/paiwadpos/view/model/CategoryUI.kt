package com.paiwad.myapp.paiwadpos.view.model

data class CategoryUI(
    var categoryId: Long? = null,
    var categoryCode: String,
    var categoryName: String,
    var categoryColor: String,
    var branchId: String
)