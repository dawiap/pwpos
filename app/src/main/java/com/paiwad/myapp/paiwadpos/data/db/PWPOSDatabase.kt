package com.paiwad.myapp.paiwadpos.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.paiwad.myapp.paiwadpos.data.db.dao.*
import com.paiwad.myapp.paiwadpos.data.db.entity.*

@Database(entities = [UserEntity::class,ProductEntity::class, OptionEntity::class,CategoryEntity::class,UnitEntity::class,BadgeEntity::class], version = 23, exportSchema = false)
@TypeConverters(Converters::class)
abstract class PWPOSDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun productDao(): ProductDao
    abstract fun optionDao(): OptionDao
    abstract fun categoryDao(): CategoryDao
    abstract fun unitDao(): ProductUnitDao
    abstract fun badgeDao(): BadgeDao
}