package com.paiwad.myapp.paiwadpos.data

import android.net.Uri
import android.util.Log
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.FirebaseStorageReference
import com.paiwad.myapp.paiwadpos.data.api.model.ProductModel
import com.paiwad.myapp.paiwadpos.data.db.dao.OptionDao
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductDao
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.mappers.option.OptionToEntityMapper
import com.paiwad.myapp.paiwadpos.data.mappers.product.ProductToEntityMapper
import com.paiwad.myapp.paiwadpos.data.mappers.product.ProductToUIMapper
import com.paiwad.myapp.paiwadpos.data.repository.ProductRepository
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onEach

@ExperimentalCoroutinesApi
class ProductRepositoryImpl(
    private val productDao: ProductDao,
    private val optionDao: OptionDao,
    private val firebaseRef: FirebaseDatabaseReference,
    private val storageRef: FirebaseStorageReference
    ):
        ProductRepository {

    private val optionToEntityMapper = OptionToEntityMapper()
    private val productToUIMapper = ProductToUIMapper()
    private val productToEntityMapper = ProductToEntityMapper()

    override suspend fun getProducts(
        branchId: String,
        categoryCode: String
    ): Flow<Result<List<ProductUI>>> = flow {
        try {
            val cacheData = productDao.getProducts()
            if (cacheData.isNullOrEmpty()) {

                updateCache(branchId, categoryCode).collect { result ->
                    when (result) {
                        is Result.Success -> {
                            val productUI = result.data?.map { productToUIMapper.fromWayOne(it) }!!
                                .filter { it.categoryCode == categoryCode }
                            emit(Result.Success(productUI))
                        }
                        is Result.Error -> {
                            emit(Result.Error(result.exception))
                        }
                    }
                }

            } else {
                val productsWithUnitAndOptions =
                    productDao.getProductsWithUnitAndOptions(categoryCode)
                Log.i("ProductsUnitOptions : ", productsWithUnitAndOptions.toString())

                val productUIList = ArrayList<ProductUI>()
                productsWithUnitAndOptions.forEach {
                    productUIList.add(productToUIMapper.fromWayTwo(it))
                }
                emit(Result.Success(productUIList))
            }
        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    override suspend fun getProducts(): Result<List<ProductUI>> {
        return try {
            val searchProducts = productDao.getProducts()
            val productUIList = ArrayList<ProductUI>()
            searchProducts.forEach {
                productUIList.add(productToUIMapper.fromWayTwo(it))
            }
            Result.Success(productUIList)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun addProduct(productModel: ProductModel) =
        firebaseRef.addDataProductWithFlow(productModel)

    override suspend fun updateProductInFb(productEntity: ProductEntity): Flow<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun deleteProduct(productUI: ProductUI) =
        firebaseRef.deleteDataProductWithFlow(productUI.productCode)

    override fun uploadImageToFbStorage(uri: Uri) = storageRef.upLoadImageToFirebaseStorage(uri)

    override suspend fun updateCache(
        branchId: String,
        categoryCode: String
    ): Flow<Result<List<ProductModel>>> = flow {
        firebaseRef.getDataProductsWithFlow(branchId, categoryCode)
            .onEach { result ->
                when (result) {
                    is Result.Success -> {
                        Log.i("Product From Network: ", result.data.toString())
                        productDao.updateProduct(result.data!!.map {
                            productToEntityMapper.fromWayOne(it)
                        })

                        optionDao.clearAllOption()

                        result.data.map { model ->
                            val optionEntity = model.options?.map { opt ->
                                optionToEntityMapper.fromWayOne(opt)
                            }
                            optionEntity?.let { optionDao.insertOptionList(it) }
                        }
                    }
                    else -> {
                    }
                }
            }
            .collect {
                emit(it)
            }
    }

    override suspend fun searchProduct(q: String): Result<List<ProductUI>> {
        return try {
            val searchProducts = productDao.searchProducts(q)
            val productUIList = ArrayList<ProductUI>()
            searchProducts.forEach {
                productUIList.add(productToUIMapper.fromWayTwo(it))
            }
            Result.Success(productUIList)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}