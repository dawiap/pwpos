package com.paiwad.myapp.paiwadpos.data.api

import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.*
import com.paiwad.myapp.paiwadpos.data.api.model.*
import com.paiwad.myapp.paiwadpos.data.model.*
import com.paiwad.myapp.paiwadpos.util.state.Result
import io.reactivex.Completable
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.isActive
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@ExperimentalCoroutinesApi
class FirebaseDatabaseReference {
    private val firebaseRef = FirebaseDatabase.getInstance().reference

    fun createUser(user: UserModel) = Completable.create { emitter ->
        firebaseRef.child(UserNode).child(BranchUserNode).child(user.uid!!).setValue(user)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }
                }
    }

    fun getDataUser(uid: String): Flow<Result<UserModel>> = callbackFlow  {
        val listener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(UserModel::class.java)
                sendBlocking(Result.Success(user))
            }
            override fun onCancelled(error: DatabaseError) {
                sendBlocking(Result.Error(error.toException()))
            }
        }
        val ref = firebaseRef.child(UserNode).child(BranchUserNode).child(uid)
        ref.addValueEventListener(listener)

        awaitClose {
            ref.removeEventListener(listener)
        }
    }

    fun updateUserLogout(user: UserModel) = Completable.create { emitter ->
        firebaseRef.child(UserNode).child(BranchUserNode).child(user.uid!!).setValue(user)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }
                }
    }

    fun addDataCategory(categoryModel: CategoryModel) = Completable.create { emitter ->
        categoryModel.categoryId = firebaseRef.push().key
        firebaseRef.child(CategoryNode).child(categoryModel.categoryId!!).setValue(categoryModel)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }

                }
    }

    fun getDataCategories(branchId: String): Flow<Result<List<CategoryModel>>> = callbackFlow {

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val categoryModelList = mutableListOf<CategoryModel>()
                for (cate in snapshot.children) {
                    val categoryModel = cate.getValue(CategoryModel::class.java)
                    if (categoryModel != null) {
                        categoryModelList.add(categoryModel)
                    }
                }
                Log.i("Ref, Category", categoryModelList.toString())
                sendBlocking(Result.Success(categoryModelList))
            }
            override fun onCancelled(error: DatabaseError) {
                sendBlocking(Result.Error(Exception()))
            }
        }

        val ref = firebaseRef.child(CategoryNode).orderByChild("branchId").equalTo(branchId)
        ref.addListenerForSingleValueEvent(listener)

        awaitClose{
            ref.removeEventListener(listener)
        }
    }

    @ExperimentalCoroutinesApi
    fun updateCategoryWithFlow(categoryModel: CategoryModel): Flow<Boolean> = callbackFlow {

        val listener = OnCompleteListener<Void> { task ->
            if (task.isSuccessful) {
                if(isActive)
                    offer(task.isSuccessful)
            } else {
                close(task.exception!!)
            }
        }

        val ref = firebaseRef.child(CategoryNode).child(categoryModel.categoryId!!).setValue(categoryModel)
        ref.addOnCompleteListener(listener)

        awaitClose{

        }
    }

    fun updateCategory(categoryModel: CategoryModel) = Completable.create { emitter ->
        firebaseRef.child(CategoryNode).child(categoryModel.categoryId!!).setValue(categoryModel)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }

                }
    }

    @ExperimentalCoroutinesApi
    fun deleteCategoryWithFlow(categoryCode: String): Flow<Boolean> = callbackFlow {
        firebaseRef.child(CategoryNode).child(categoryCode).setValue(null)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if (isActive)
                            offer(task.isSuccessful)
                    } else {
                        close(task.exception!!)
                    }
                }
        awaitClose()
    }

    fun deleteCategory(categoryCode: String) = Completable.create { emitter ->
        firebaseRef.child(CategoryNode).child(categoryCode).setValue(null)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }

                }
    }

    fun checkProductForDeleteCategoryWithFlow(categoryCode: String): Flow<Boolean> = callbackFlow {
        firebaseRef.child(ProductNode).orderByChild("categoryCode").equalTo(categoryCode)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val data = snapshot.children.count()
                        val hasProduct = data > 0
                        offer(hasProduct)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        close(error.toException())
                    }
                })
        awaitClose()
    }

    fun checkProductForDeleteCategory(categoryCode: String) = Observable.create<Boolean> { emitter ->
        firebaseRef.child(ProductNode).orderByChild("categoryCode").equalTo(categoryCode)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val data = snapshot.children.count()
                        val hasProduct = data > 0
                        emitter.onNext(hasProduct)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        emitter.onError(error.toException())
                    }
                })
    }

    fun checkProductForDeleteUnit(unitCode: String) = Observable.create<Boolean> { emitter ->
        firebaseRef.child(ProductNode).orderByChild("unitCode").equalTo(unitCode)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val data = snapshot.children.count()
                        val hasProduct = data > 0
                        emitter.onNext(hasProduct)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        emitter.onError(error.toException())
                    }
                })
    }

    fun getDataProductsWithFlow(branchId: String, categoryCode: String): Flow<Result<List<ProductModel>>> = callbackFlow {

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                val productModelList = mutableListOf<ProductModel>()

                for (data in snapshot.children) {

                    val productModel = data.getValue(ProductModel::class.java)

                    if (productModel != null) {
                        //if (productModel.categoryCode == categoryCode)
                            productModelList.add(productModel)
                    }
                }
                sendBlocking(Result.Success(productModelList))
            }

            override fun onCancelled(error: DatabaseError) {
                sendBlocking(error.toException())
            }
        }

        val ref = firebaseRef.child(ProductNode).orderByChild("branchId").equalTo(branchId)
        ref.addValueEventListener(listener)

        awaitClose{
            ref.removeEventListener(listener)
        }
    }

    fun addDataProductWithFlow(productModel: ProductModel): Flow<Boolean> = callbackFlow {
        productModel.productId = firebaseRef.push().key
        productModel.options?.forEach {
            it.optionId = firebaseRef.push().push().key
            it.productCode = productModel.productId
        }
        firebaseRef.child(ProductNode).child(productModel.productId!!).setValue(productModel)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        offer(task.isSuccessful)
                    } else {
                        close(task.exception!!)
                    }

                }
        awaitClose()
    }

    fun deleteDataProductWithFlow(productId: String): Flow<Boolean> = callbackFlow {
        firebaseRef.child(ProductNode).child(productId).setValue(null)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        offer(task.isSuccessful)
                    } else {
                        close(task.exception)
                    }

                }
        awaitClose()
    }

    fun getDataUnits(branchId: String): Flow<Result<List<UnitModel>>> = callbackFlow {
        val listener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val unitModelList = mutableListOf<UnitModel>()

                for (data in snapshot.children) {

                    val unitModel = data.getValue(UnitModel::class.java)

                    if (unitModel != null) {

                        unitModelList.add(unitModel)
                    }
                }
                Log.i("Ref, Unit", unitModelList.toString())
                sendBlocking(Result.Success(unitModelList))
            }

            override fun onCancelled(error: DatabaseError) {
                sendBlocking(Result.Error(error.toException()))
            }
        }

        val ref = firebaseRef.child(UnitNode).orderByChild("branchId").equalTo(branchId)
        ref.addValueEventListener(listener)

        awaitClose { ref.removeEventListener(listener) }
    }

    fun addDataUnit(unitModel: UnitModel) = Completable.create { emitter ->
        unitModel.unitId = firebaseRef.push().key
        firebaseRef.child(UnitNode).child(unitModel.unitId!!).setValue(unitModel)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }

                }
    }

    fun deleteDataUnit(unitId: String) = Completable.create { emitter ->
        firebaseRef.child(UnitNode).child(unitId).setValue(null)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }

                }
    }

    fun updateDataUnit(unitModel: UnitModel) = Completable.create { emitter ->
        firebaseRef.child(UnitNode).child(unitModel.unitId!!).setValue(unitModel)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(task.exception!!)
                    }

                }
    }

    fun getDataOrders(branchId: String): Flow<Result<List<OrderModel>>> = callbackFlow {
        val listener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                val unitModelList = mutableListOf<OrderModel>()

                for (data in snapshot.children) {

                    val orderModel = data.getValue(OrderModel::class.java)

                    if (orderModel != null) {
                        unitModelList.add(orderModel)
                    }
                }
                Log.i("Ref, Order", unitModelList.toString())
                sendBlocking(Result.Success(unitModelList))
            }

            override fun onCancelled(error: DatabaseError) {
                sendBlocking(Result.Error(error.toException()))
            }
        }

        val ref = firebaseRef.child(OrderNode).orderByChild("branchId").equalTo(branchId)
        ref.addValueEventListener(listener)

        awaitClose { ref.removeEventListener(listener) }
    }

    fun getDataOrderByOrderId(orderId: String): Flow<Result<OrderModel>> = callbackFlow {
        val listener = object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                for (data in snapshot.children) {

                    val orderModel = data.getValue(OrderModel::class.java)

                    if (orderModel != null)
                        sendBlocking(Result.Success(orderModel))
                    Log.i("Ref, Single Order", orderModel.toString())
                }
            }

            override fun onCancelled(error: DatabaseError) {
                sendBlocking(Result.Error(error.toException()))
            }
        }

        val ref = firebaseRef.child(OrderNode).orderByChild("orderId").equalTo(orderId)
        ref.addValueEventListener(listener)

        awaitClose { ref.removeEventListener(listener) }
    }

    fun addDataOrder(orderModel: OrderModel) = Completable.create { emitter ->
        orderModel.orderId = firebaseRef.push().key
        orderModel.orderDetailModel?.map { it.orderDetailId = firebaseRef.push().push().key }
        firebaseRef.child(OrderNode).child(orderModel.orderId!!).setValue(orderModel)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    emitter.onComplete()
                } else {
                    emitter.onError(task.exception!!)
                }
            }
    }

    fun updateDataOrder(orderModel: OrderModel) = Completable.create { emitter ->
        firebaseRef.child(OrderNode).child(orderModel.orderId!!).setValue(orderModel)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    emitter.onComplete()
                } else {
                    emitter.onError(task.exception!!)
                }
            }
    }
}