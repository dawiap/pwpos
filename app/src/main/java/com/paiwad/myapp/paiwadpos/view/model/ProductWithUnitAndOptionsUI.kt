package com.paiwad.myapp.paiwadpos.view.model

data class ProductWithUnitAndOptionsUI(
        val product: ProductUI,
        val unit: UnitUI,
        val options: List<OptionUI?>,
        )
