package com.paiwad.myapp.paiwadpos.view.di.hilt

import android.app.Activity
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.InternetCheck
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@Module
@InstallIn(ActivityComponent::class)
class UtilModule {
    @Provides
    fun provideProgressBuilder(activity: Activity): ProgressBuilder {
        return ProgressBuilder(activity)
    }

    @Provides
    fun provideAlertDialogBuilder(activity: Activity): AlertDialogBuilder {
        return AlertDialogBuilder(activity)
    }

    @Provides
    fun provideInternetCheck(activity: Activity): InternetCheck {
        return InternetCheck(activity)
    }
}