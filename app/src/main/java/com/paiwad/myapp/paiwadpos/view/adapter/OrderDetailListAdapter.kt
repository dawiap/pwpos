package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ShrOrderDetailListBinding
import com.paiwad.myapp.paiwadpos.databinding.ShrOrderListBinding
import com.paiwad.myapp.paiwadpos.view.model.OrderDetailUI
import com.paiwad.myapp.paiwadpos.view.model.OrderUI

class OrderDetailListAdapter: RecyclerView.Adapter<OrderDetailListAdapter.OrderDetailViewHolder>() {

    inner class OrderDetailViewHolder(val binding: ShrOrderDetailListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: OrderDetailUI) {
            binding.orderDetailUI = item
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<OrderDetailUI>() {
        override fun areItemsTheSame(oldItem: OrderDetailUI, newItem: OrderDetailUI): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: OrderDetailUI, newItem: OrderDetailUI): Boolean {
            return oldItem.orderDetailCode == newItem.orderDetailCode
        }

    }

    val differ = AsyncListDiffer(this, callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderDetailViewHolder {
        return OrderDetailViewHolder(
            ShrOrderDetailListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: OrderDetailViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size
}