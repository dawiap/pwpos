package com.paiwad.myapp.paiwadpos.viewmodel

import android.content.Intent
import android.view.View
import androidx.lifecycle.*
import com.paiwad.myapp.paiwadpos.view.ui.manage.AddCategoryActivity
import com.paiwad.myapp.paiwadpos.view.ui.manage.AddProductActivity
import com.paiwad.myapp.paiwadpos.view.ui.manage.UnitAddActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ManageViewModel @Inject constructor() : ViewModel() {

    fun goToManageCategory(v: View){
        v.context.startActivity(Intent(v.context, AddCategoryActivity::class.java))
    }

    fun goToManageUnit(v: View){
        v.context.startActivity(Intent(v.context, UnitAddActivity::class.java))
    }

    fun goToProductAdd(v: View){
        v.context.startActivity(Intent(v.context, AddProductActivity::class.java))
    }
}