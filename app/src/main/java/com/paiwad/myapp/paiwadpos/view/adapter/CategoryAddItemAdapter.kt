package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ViewAddCategoryItemBinding
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI

class CategoryAddItemAdapter: RecyclerView.Adapter<CategoryAddItemAdapter.CategoryAddItemViewHolder>() {

    lateinit var clickListener: ((View, CategoryUI, Int) -> Unit)

    inner class CategoryAddItemViewHolder(val binding: ViewAddCategoryItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CategoryUI, clickListener: (View, CategoryUI, Int) -> Unit) {
            binding.category = item
            binding.imgRemove.setOnClickListener {
                clickListener(binding.root, item, adapterPosition)
            }
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<CategoryUI>(){
        override fun areItemsTheSame(oldItem: CategoryUI, newItem: CategoryUI): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: CategoryUI, newItem: CategoryUI): Boolean {
            return oldItem.categoryId == newItem.categoryId
        }

    }

    val differ = AsyncListDiffer(this, callBack)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAddItemViewHolder {
        return CategoryAddItemViewHolder(
            ViewAddCategoryItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: CategoryAddItemViewHolder, position: Int) {
        holder.bind(differ.currentList[position], clickListener)
    }

    override fun getItemCount(): Int = differ.currentList.size
}