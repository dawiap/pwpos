package com.paiwad.myapp.paiwadpos.data

import android.util.Log
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseUser
import com.paiwad.myapp.paiwadpos.data.api.FirebaseAuthService
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.db.dao.*
import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity
import com.paiwad.myapp.paiwadpos.data.mappers.user.UserEntityNetworkNullMapper
import com.paiwad.myapp.paiwadpos.data.repository.UserRepository
import io.reactivex.Completable
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import com.paiwad.myapp.paiwadpos.util.state.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class UserRepositoryImpl(
        private val firebaseAuth: FirebaseAuthService,
        private val firebaseRef: FirebaseDatabaseReference,
        private val userDao: UserDao,
        private val categoryDao: CategoryDao,
        private val productDao: ProductDao,
        private val optionDao: OptionDao,
        private val unitDao: ProductUnitDao
): UserRepository {
    override fun loginWithEmailAndPassword(email: String, password: String): Completable {
        return firebaseAuth.loginWithEmailAndPassword(email, password)
    }

    override fun loginWithGoogle(credential: AuthCredential): Completable {
        return firebaseAuth.loginWithGoogle(credential)
    }

    override fun loginWithFacebook(credential: AuthCredential): Completable {
        return firebaseAuth.loginWithFacebook(credential)
    }

    override fun registerWithEmailAndPassword(email: String, password: String): Completable {
        return firebaseAuth.registerWithEmailAndPassword(email, password)
    }

    override fun resetPasswordWithEmail(email: String): Completable {
        return firebaseAuth.resetPasswordWithEmail(email)
    }

    override fun createUserToDb(user: UserEntity): Completable {
        return firebaseRef.createUser(toUserModel(user))
    }

    override fun currentUser(): FirebaseUser?{
        return firebaseAuth.currentUser()
    }


    override fun logout() {
        clearCache()
        firebaseAuth.logout()
    }

    override fun getDataUser(uid: String): Flow<Result<UserEntity>?> = flow{
        val user = userDao.getUser()
        if(user == null){
            firebaseRef.getDataUser(uid)
                .onEach { result ->
                    when(result) {
                        is Result.Success -> {
                            userDao.clearUser()

                            val userEntity = UserEntityNetworkNullMapper().upstream(result.data)
                            userEntity?.let {
                                userDao.insertUser(it)
                            }
                            Log.i("User from Network","User updated..")

                        }
                        else -> {}
                    }
                }
                .collect { result ->
                when(result){
                    is Result.Success -> {
                        val userEntity = UserEntityNetworkNullMapper().upstream(result.data)
                        emit(Result.Success(userEntity))
                    }
                    is Result.Error -> {
                        emit(Result.Error(result.exception))
                    }
                }
            }
        }else{
            Log.i("User from cache",user.toString())
            emit(Result.Success(user))
        }
    }

    override fun updateUserLogout(user: UserEntity): Completable {
        return firebaseRef.updateUserLogout(toUserModel(user))
    }

    override fun checkYourPIN(pin: String): Flow<Result<Boolean>> = flow {
        try {
            val user = userDao.checkYourPIN(pin)
            if (user != 0)
                emit(Result.Success(true))
            else
                emit(Result.Success(false))
        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    private fun toUserModel(user: UserEntity) = UserEntityNetworkNullMapper().downstream(user)

    private fun clearCache(){
        CoroutineScope(Dispatchers.IO).launch {
            categoryDao.clearAllCategory()
            productDao.clearAllProduct()
            optionDao.clearAllOption()
            unitDao.clearAllUnit()
            userDao.clearUser()
        }
    }
}