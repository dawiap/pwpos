package com.paiwad.myapp.paiwadpos.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.ui.*
import com.google.android.material.navigation.NavigationView
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityMainBinding
import com.paiwad.myapp.paiwadpos.databinding.CustomActionCartItemLayoutBinding
import com.paiwad.myapp.paiwadpos.databinding.NavHeaderMainBinding
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.InternetCheck
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.view.ui.user.CreateUserActivity
import com.paiwad.myapp.paiwadpos.view.ui.user.LoginActivity
import com.paiwad.myapp.paiwadpos.viewmodel.*
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, AlertDialogBuilder.PositiveClickListener,
    UserAuthListener {

    val userViewModel: UserViewModel by viewModels()

    val categoryViewModel: CategoryViewModel by viewModels()

    val productViewModel: ProductViewModel by viewModels()

    val unitViewModel: UnitViewModel by viewModels()

    val menuViewModel: MenuViewModel by viewModels()

    val badgeViewModel: BadgeViewModel by viewModels()

    val orderViewModel: OrderViewModel by viewModels()

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    @Inject
    lateinit var alertDialogBuilder: AlertDialogBuilder

    @Inject
    lateinit var internetCheck: InternetCheck

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var navHeadBinding: NavHeaderMainBinding
    private lateinit var badgeBinding: CustomActionCartItemLayoutBinding

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navHeadBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.nav_header_main,
            binding.navView,
            false
        )
        navHeadBinding.lifecycleOwner = this
        binding.navView.addHeaderView(navHeadBinding.root)

        userViewModel.getUserProfile()

        setSupportActionBar(binding.toolbar)

        setupNavigationView()

        userViewModel.authListener = this

        initCategoryAndProduct()

        navHeadBinding.buttonLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()

        badgeViewModel.getBadge()

        userViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    private fun initCategoryAndProduct() {
        userViewModel.userProfile.observe(this, { user ->

            navHeadBinding.user = user

            user?.let {
                categoryViewModel.getCategoriesFromFb(it.uid, internetCheck.isNetworkAvailable())
                unitViewModel.getUnitsFromFb(it.uid)
                //orderViewModel.getOrders(it.uid)
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val menuItem = menu.findItem(R.id.action_cart)
        badgeBinding = CustomActionCartItemLayoutBinding.inflate(layoutInflater)
        badgeBinding.viewModel = badgeViewModel
        badgeBinding.lifecycleOwner = this

        menuItem.actionView = badgeBinding.root
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_search -> {
                startActivity(Intent(this, SearchProductActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_logout -> {
                alertDialogBuilder.showAlertDialog(
                    getString(R.string.text_logout_title),
                    getString(R.string.text_logout_message),
                    getString(R.string.text_button_logout),
                    getString(R.string.text_button_cancel)
                )
                alertDialogBuilder.setPositiveClickListener(this)
            }
            else -> item.onNavDestinationSelected(navController)
        }
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
        return true
    }

    override fun onBackPressed() {
        println("do someting")
    }

    private fun setupNavigationView() {
        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_menu, R.id.nav_manage, R.id.nav_order, R.id.nav_setting, R.id.nav_logout
            ), binding.drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)

        binding.toolbar.setNavigationIcon(R.drawable.ic_action_drawer)
        navController.addOnDestinationChangedListener { _, _, _ ->
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_action_drawer)
        }

        binding.navView.setNavigationItemSelectedListener(this)

        val menuItem = binding.navView.menu.findItem(R.id.nav_logout)

        userViewModel.userProfile.observe(this, {
            it?.let {
                menuItem.isVisible = true
            } ?: run {
                menuItem.isVisible = false
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun positiveButtonClick() {
        userViewModel.userProfile.observe(this, { user ->
            user?.signOn = 0
            userViewModel.userLogout(user)
        })
    }

    override fun onSuccess(successType: SuccessType) {

        when (successType) {
            SuccessType.LOGIN_SUCCESS -> {
                binding.navView.setCheckedItem(0)
            }
            SuccessType.LOGOUT_SUCCESS -> {
                binding.navView.setCheckedItem(0)
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
            SuccessType.ON_CREATE_USER -> {
                startActivity(Intent(this, CreateUserActivity::class.java))
                finish()
            }
            else -> {
            }
        }

    }

    override fun onFailure(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}