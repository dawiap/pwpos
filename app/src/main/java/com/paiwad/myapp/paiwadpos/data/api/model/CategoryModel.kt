package com.paiwad.myapp.paiwadpos.data.api.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class CategoryModel(
    var categoryId: String? = null,
    var categoryName: String? = null,
    var categoryColor: String? = null,
    var branchId: String? = null
)