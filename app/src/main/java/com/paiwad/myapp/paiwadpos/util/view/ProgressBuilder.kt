package com.paiwad.myapp.paiwadpos.util.view

import android.app.Activity
import android.app.Dialog
import android.view.Window
import com.paiwad.myapp.paiwadpos.R

class ProgressBuilder (context: Activity) {

    private var dialog: Dialog = Dialog(context)

    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_progress_background)
        dialog.setContentView(R.layout.dialog_spinner)
        dialog.setCancelable(false)
    }

    fun showProgressDialog() = dialog.show()

    fun dismissProgressDialog() = dialog.dismiss()

    fun isShowing(): Boolean = dialog.isShowing

}