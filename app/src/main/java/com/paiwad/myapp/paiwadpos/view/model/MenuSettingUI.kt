package com.paiwad.myapp.paiwadpos.view.model

data class MenuSettingUI(
    val menuName: String,
    val menuIcon: Int
)