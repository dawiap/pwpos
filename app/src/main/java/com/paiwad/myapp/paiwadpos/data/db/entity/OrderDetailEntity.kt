package com.paiwad.myapp.paiwadpos.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.paiwad.myapp.paiwadpos.data.api.model.OptionModel
import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel

@Entity(tableName = "ops_orderdetail")
data class OrderDetailEntity(
        @PrimaryKey(autoGenerate = true)
        val orderDetailId: Long = 0,
        val orderCode: String? = null,
        val productCode: String,
        val productName: String,
        val imagePath: String,
        val productPrice: String,
        val productCount: Int,
        val productTotalAmount: Double,
        val unitModel: UnitModel,
        val optionModel: OptionModel
)
