package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ShrOrderListBinding
import com.paiwad.myapp.paiwadpos.view.model.OrderUI

class OrderListAdapter: RecyclerView.Adapter<OrderListAdapter.OrderViewHolder>() {

    lateinit var clickListener: ((View, OrderUI, Int) -> Unit)

    inner class OrderViewHolder(val binding: ShrOrderListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: OrderUI, position: Int, clickListener: (View, OrderUI, Int) -> Unit) {
            binding.order = item
            binding.root.setOnClickListener {
                clickListener(binding.root, item, position)
            }
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<OrderUI>() {
        override fun areItemsTheSame(oldItem: OrderUI, newItem: OrderUI): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: OrderUI, newItem: OrderUI): Boolean {
            return oldItem.orderId == newItem.orderId
        }

    }

    val differ = AsyncListDiffer(this, callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        return OrderViewHolder(
                ShrOrderListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        holder.bind(differ.currentList[position], position, clickListener)
    }

    override fun getItemCount(): Int = differ.currentList.size
}