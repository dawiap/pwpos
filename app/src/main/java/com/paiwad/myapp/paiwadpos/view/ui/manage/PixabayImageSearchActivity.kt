package com.paiwad.myapp.paiwadpos.view.ui.manage

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.databinding.ActivityPixabayImageSearchBinding
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.adapter.PixabayListAdapter
import com.paiwad.myapp.paiwadpos.view.adapter.PixabayPagingListAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.PixabayViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@AndroidEntryPoint
class PixabayImageSearchActivity : AppCompatActivity() {
    @Inject
    lateinit var pixabayViewModel: PixabayViewModel
    @Inject
    lateinit var adapter: PixabayListAdapter

    private lateinit var binding: ActivityPixabayImageSearchBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pixabay_image_search)
        binding.lifecycleOwner = this
        binding.viewModel = pixabayViewModel

        onSearchImageFromPixabay()
        initView()
    }

    private fun initView() {
        binding.editQuery.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                hideKeyboard(view)
                updateRepoListFromInput()
                true
            } else {
                false
            }
        }
        binding.editQuery.setOnKeyListener { view, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                hideKeyboard(view)
                updateRepoListFromInput()
                true
            } else {
                false
            }
        }
    }

    private fun updateRepoListFromInput() {
        binding.editQuery.text.trim().let {
            if (it.isNotEmpty()) {
                binding.rcvHit.scrollToPosition(0)
                onSearchImageFromPixabay()
            }
        }
    }

    private fun onSearchImageFromPixabay() {
        binding.gifImageView.visibility = View.VISIBLE
        lifecycleScope.launchWhenCreated {
            /*pixabayViewModel.searchPixabay().collectLatest { pagingData ->
                binding.gifImageView.visibility = View.GONE
                setupRecycleviewAdapter(pagingData)
            }*/
            pixabayViewModel.searchImage().observe(this@PixabayImageSearchActivity,{
                when(it){
                    is Result.Success -> {
                        binding.gifImageView.visibility = View.GONE
                        setupRecycleviewAdapter(it.data!!)
                    }
                    is Result.Error -> {
                        Toast.makeText(this@PixabayImageSearchActivity, it.exception.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    /*private fun setupRecycleviewAdapter(pagingData: PagingData<Hit>){
        val layoutManager = GridLayoutManager(this@PixabayImageSearchActivity, 3)
        binding.rcvHit.layoutManager = layoutManager
        adapter.submitData(pagingData)
        binding.rcvHit.adapter = adapterPaging

        adapterPaging.onItemClick = { hit ->
            getSelectedItem(hit)
        }
    }*/

    private fun setupRecycleviewAdapter(hits: List<Hit>){
        val layoutManager = GridLayoutManager(this@PixabayImageSearchActivity, 3)
        binding.rcvHit.layoutManager = layoutManager
        adapter.differ.submitList(hits)
        binding.rcvHit.adapter = adapter

        adapter.onItemClick = { hit ->
            getSelectedItem(hit)
        }
    }

    private fun getSelectedItem(item: Hit){
        val intent = Intent()
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("pixabay", item)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}