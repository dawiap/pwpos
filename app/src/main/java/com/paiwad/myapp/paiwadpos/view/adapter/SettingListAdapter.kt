package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ViewMenuSettingItemBinding
import com.paiwad.myapp.paiwadpos.view.model.MenuSettingUI

class SettingListAdapter: RecyclerView.Adapter<SettingListAdapter.SettingItemViewHolder>() {

    lateinit var onItemClickListener: ((MenuSettingUI) -> Unit)

    var items = emptyList<MenuSettingUI>()

    inner class SettingItemViewHolder(private val binding: ViewMenuSettingItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: MenuSettingUI, itemClick: ((MenuSettingUI) -> Unit)){
            binding.menu = item
            binding.root.setOnClickListener {
                itemClick(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingItemViewHolder {
        return SettingItemViewHolder(ViewMenuSettingItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: SettingItemViewHolder, position: Int) {
        holder.bind(items[position],onItemClickListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}