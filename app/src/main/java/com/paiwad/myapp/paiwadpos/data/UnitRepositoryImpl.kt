package com.paiwad.myapp.paiwadpos.data

import android.util.Log
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.data.db.dao.ProductUnitDao
import com.paiwad.myapp.paiwadpos.data.mappers.unit.UnitToEntityMapper
import com.paiwad.myapp.paiwadpos.data.mappers.unit.UnitToModelMapper
import com.paiwad.myapp.paiwadpos.data.mappers.unit.UnitToUIMapper
import com.paiwad.myapp.paiwadpos.data.repository.UnitRepository
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import io.reactivex.Completable
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onEach

class UnitRepositoryImpl(
    private val unitDao: ProductUnitDao,
    private val firebaseRef: FirebaseDatabaseReference
    ):
        UnitRepository {

    private val entityMapper = UnitToEntityMapper()
    private val uiMapper = UnitToUIMapper()
    private val modelMapper = UnitToModelMapper()

    override fun getUnitsFromFb(branchId: String): Flow<Result<List<UnitUI>>> = flow {
        try {
            val dataCache = unitDao.getUnits()
            if (dataCache.isNullOrEmpty()) {

                updateCache(branchId).collect { result ->
                    when (result) {
                        is Result.Success -> {
                            Log.i("Unit From Network: ", result.data.toString())
                            val unitUIList =
                                    result.data?.map { uiMapper.fromWayTwo(it) }
                            emit(Result.Success(unitUIList))
                        }
                        is Result.Error -> {
                            emit(Result.Error(result.exception))
                        }
                    }
                }

            } else {
                Log.i("Unit From Cache: ", dataCache.toString())
                val unitUI = dataCache.map { uiMapper.fromWayOne(it) }
                emit(Result.Success(unitUI))
            }
        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    override fun insertUnitToFb(unitUI: UnitUI): Completable {
        return firebaseRef.addDataUnit(toUnitModel(unitUI))
    }

    override fun updateUnitToFb(unitUI: UnitUI): Completable {
        return firebaseRef.updateDataUnit(toUnitModel(unitUI))
    }

    override fun deleteUnitToFb(unit: String): Completable {
        return firebaseRef.deleteDataUnit(unit)
    }

    override fun checkProductBeforeDeleteUnitInFb(unitId: String): Observable<Boolean> {
        return firebaseRef.checkProductForDeleteUnit(unitId)
    }

    override suspend fun updateCache(branchId: String): Flow<Result<List<UnitModel>>>  = flow {
        firebaseRef.getDataUnits(branchId)
                .onEach { result ->
                    when (result) {
                        is Result.Success -> {
                            println("updateCache")
                            unitDao.clearAllUnit()
                            unitDao.insertUnitList(result.data!!.map { entityMapper.fromWayOne(it) })
                        }
                        else -> {
                        }
                    }
                }
                .collect {
                    emit(it)
                }
    }

    private fun toUnitModel(unitUI: UnitUI) = modelMapper.toData(unitUI)
}