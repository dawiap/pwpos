package com.paiwad.myapp.paiwadpos.data

import com.paiwad.myapp.paiwadpos.data.api.PixabayApiService
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.data.repository.PixabayRepository
import com.paiwad.myapp.paiwadpos.util.state.Result

class PixabayRepositoryImpl(
        private val pixabayApiService: PixabayApiService): PixabayRepository
{
    override suspend fun searchImage(q: String): Result<List<Hit>> {
        return try {
            val response = pixabayApiService.searchImageFromPixabay(q)
            if(response.isSuccessful){
                response.body()?.let {
                    return@let Result.Success(it.hits)
                } ?: Result.Error(Exception("An unknown error occurred"))
            }else{
                Result.Error(Exception("An unknown error occurred"))
            }
        }catch (e: Exception){
            Result.Error(Exception("Network error, Check your internet connection!"))
        }
    }
}