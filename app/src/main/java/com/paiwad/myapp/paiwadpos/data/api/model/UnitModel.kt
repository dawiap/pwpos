package com.paiwad.myapp.paiwadpos.data.api.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class UnitModel (
        var unitId: String? = null,
        var branchId: String? = null,
        var unitName: String? = null,
)