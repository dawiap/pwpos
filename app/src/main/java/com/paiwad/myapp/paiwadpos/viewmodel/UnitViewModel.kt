package com.paiwad.myapp.paiwadpos.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.paiwad.myapp.paiwadpos.data.repository.UnitRepository
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UnitViewModel @Inject constructor(
    private val unitRepository: UnitRepository
): ViewModel() {

    val unitName = MutableLiveData<String?>()

    var branchId: String? = null

    var authListener: UserAuthListener? = null

    val _titleUnitManage = MutableLiveData<String>()

    val _isActionAdd = MutableLiveData<Boolean>()

    val isAction: LiveData<Boolean> = _isActionAdd

    val titleUnitManage: LiveData<String> = _titleUnitManage

    private val _unitList = MutableLiveData<List<UnitUI>>()

    val unitList: LiveData<List<UnitUI>> = _unitList

    private val disposables = CompositeDisposable()

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()

    val loading: LiveData<SingleEvent<Boolean>> = _loading

    var unitItem: UnitUI? = null

    fun getUnitsFromFb(branchId: String) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        unitRepository.getUnitsFromFb(branchId).collect { result ->
            _loading.value = SingleEvent(false)
            when (result) {
                is Result.Success -> {
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                    _unitList.value = result.data!!
                }
                is Result.Error -> {
                    authListener?.onFailure(result.exception.message!!)
                }
            }
        }
    }

    fun addUnitToFb() {
        if (unitName.value.isNullOrEmpty() || branchId.isNullOrEmpty()) {
            authListener?.onFailure("Please enter require field")
            return
        }

        _loading.value = SingleEvent(true)
        val disposable = unitRepository.insertUnitToFb(toUnitUIMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)

                    updateCache(toUnitUIMapper().branchId)

                    unitName.value = null
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun checkDataBeforeDelete(unitUI: UnitUI) {
        _loading.value = SingleEvent(true)
        val disposable = unitRepository.checkProductBeforeDeleteUnitInFb(unitUI.unitCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it) {
                        authListener?.onSuccess(SuccessType.WARING_HAS_DATA)
                        _loading.value = SingleEvent(false)
                    } else {
                        deleteUnitInFb(unitUI)
                    }
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    private fun deleteUnitInFb(unitUI: UnitUI) {
        val disposable = unitRepository.deleteUnitToFb(unitUI.unitCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)
                    updateCache(unitUI.branchId)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun updateUnitInFb() {
        if (unitName.value.isNullOrEmpty() || branchId.isNullOrEmpty() || unitItem == null) {
            authListener?.onFailure("Please enter require field")
            return
        }

        _loading.value = SingleEvent(true)
        val disposable = unitRepository.updateUnitToFb(fUnitUIUpdateMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)

                    updateCache(fUnitUIUpdateMapper().branchId)

                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    private fun updateCache(branchId: String) = viewModelScope.launch {
        unitRepository.updateCache(branchId).collect { result ->
            when(result){
                is Result.Success -> {
                    getUnitsFromFb(branchId)
                }
                is Result.Error -> {
                    Log.e("VM Update Error", result.exception.message.toString())
                }
            }
        }
    }

    private fun toUnitUIMapper(): UnitUI = UnitUI(
            unitCode = "",
            unitName = unitName.value!!,
            branchId = branchId!!,
    )

    private fun fUnitUIUpdateMapper(): UnitUI = UnitUI(
            unitCode = unitItem!!.unitCode,
            unitName = unitName.value!!,
            branchId = branchId!!
    )

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}