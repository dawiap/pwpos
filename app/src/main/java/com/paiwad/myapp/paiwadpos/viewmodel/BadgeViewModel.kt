package com.paiwad.myapp.paiwadpos.viewmodel

import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.data.mappers.order.OrderToModelMapper
import com.paiwad.myapp.paiwadpos.data.repository.BadgeRepository
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.ui.BadgeActivity
import com.paiwad.myapp.paiwadpos.view.ui.BadgeFragmentDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BadgeViewModel @Inject constructor(
    private val badgeRepository: BadgeRepository
): ViewModel() {

    private val _badge = MutableLiveData<List<BadgeWithProduct>>()
    val badge: LiveData<List<BadgeWithProduct>> = _badge

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    private val _totalAmount = MutableLiveData<Double>()
    val totalAmount: LiveData<Double> = _totalAmount

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()
    val loading: LiveData<SingleEvent<Boolean>> = _loading

    var authListener: UserAuthListener? = null

    fun getBadge() = viewModelScope.launch {
        _loading.value = SingleEvent(false)
        badgeRepository.getBadge().collect {
            _loading.value = SingleEvent(false)
            when (it) {
                is Result.Success -> {
                    it.data?.let { badge ->
                        _badge.value = badge
                        _totalAmount.value = badge.sumByDouble {
                            if (it.product!!.isOption)
                                it.badge.badgeCount * it.badge.badgeOptionPrice!!
                            else
                                it.badge.badgeCount * it.product.productPrice
                        }
                    }
                }
                is Result.Error -> {
                    _message.value = SingleEvent(it.exception.message.toString())
                }
            }
        }
    }

    fun addProductsToBadge(badgeEntity: BadgeEntity) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        badgeRepository.addProductToBadge(badgeEntity).collect {
            _loading.value = SingleEvent(false)
            when (it) {
                is Result.Success -> {
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                    _message.value = SingleEvent("เพิ่มลงตะกร้าแล้ว")
                    getBadge()
                }
                is Result.Error -> {
                    println(it.exception.message.toString())
                    _message.value = SingleEvent(it.exception.message.toString())
                }
            }
        }
    }

    fun updateProductBadgeAdd(badgeEntity: BadgeEntity) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        val badge =
                _badge.value!!.last {
                    if (it.product!!.isOption)
                        it.badge.badgeOptionCode == badgeEntity.badgeOptionCode
                    else
                        it.badge.badgeProductCode == badgeEntity.badgeProductCode
                }
        badge.badge.badgeCount++
        callUpdateProductBadge(badge.badge)
    }

    fun updateProductBadgeMinus(badgeEntity: BadgeEntity) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        val badge =
                _badge.value!!.last {
                    if (it.product!!.isOption)
                        it.badge.badgeOptionCode == badgeEntity.badgeOptionCode
                    else
                        it.badge.badgeProductCode == badgeEntity.badgeProductCode
                }
        badge.badge.badgeCount--
        if (badge.badge.badgeCount >= 0)
            callUpdateProductBadge(badge.badge)
    }

    private fun callUpdateProductBadge(badgeEntity: BadgeEntity) = viewModelScope.launch {
        badgeRepository.updateProductInBadge(badgeEntity).collect {
            _loading.value = SingleEvent(false)
            when (it) {
                is Result.Success -> getBadge()

                is Result.Error -> _message.value = SingleEvent(it.exception.message.toString())
            }
        }
    }

    fun clearBadge() = viewModelScope.launch {
        badgeRepository.clearBadge()
        getBadge()
    }

    fun goToBadge(v: View) {
        v.context.startActivity(
                Intent(
                        v.context, BadgeActivity::class.java
                )
        )
    }

    fun goToPaymentFragment(v: View) {
        _totalAmount.value?.let {
            v.findNavController().navigate(
                    BadgeFragmentDirections.actionBadgeFragmentToPaymentFragment2()
            )
        }

        //v.findNavController().navigate(BadgeFragmentDirections.actionBadgeFragmentToSendPaymentGraph2())
    }

    fun toOrderModelMapper(): OrderModel{
        val order = orderToModelMapper.orderModel(_badge.value!!)
        order.isPayment = 0
        order.totalAmount = _totalAmount.value!!.toFloat()
        return order
    }
    private val orderToModelMapper = OrderToModelMapper()
}