package com.paiwad.myapp.paiwadpos.viewmodel

import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.mappers.order.OrderToModelMapper
import com.paiwad.myapp.paiwadpos.data.repository.OrderRepository
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import com.paiwad.myapp.paiwadpos.view.ui.OrderDetailFragmentDirections
import com.paiwad.myapp.paiwadpos.view.ui.OrderFragment
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OrderViewModel @Inject constructor(
    private val orderRepository: OrderRepository
): ViewModel() {

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()
    val loading: LiveData<SingleEvent<Boolean>> = _loading

    private val _orders = MutableLiveData<List<OrderUI>>()
    private val _singleOrder = MutableLiveData<OrderUI>()
    val orders: LiveData<List<OrderUI>> = _orders
    val singleOrder: LiveData<OrderUI> = _singleOrder

    private val _lastTab = MutableLiveData(0)
    val lastTab: LiveData<Int> = _lastTab

    fun saveStateLastTab(tabPosition: Int){
        _lastTab.value = tabPosition
    }

    private val disposables = CompositeDisposable()

    var authListener: UserAuthListener? = null

    fun getOrders(branchId: String) = viewModelScope.launch{
        try {
            _loading.value = SingleEvent(true)
            orderRepository.getOrders(branchId).collect {
                when(it){
                    is Result.Success -> {
                        _loading.value = SingleEvent(false)
                        _orders.value = it.data!!
                    }
                    is Result.Error -> {
                        _loading.value = SingleEvent(false)
                        _message.value = SingleEvent(it.exception.toString())
                    }
                }
            }

        }catch (e: Exception){
            println("err : ${e.localizedMessage}")
        }
    }

    fun getOrderByTabMenu(branchId: String,orderTabType: OrderFragment.OrderTabType) = viewModelScope.launch{
        try {
            _loading.value = SingleEvent(true)
            orderRepository.getOrders(branchId).collect {
                when(it){
                    is Result.Success -> {
                        _loading.value = SingleEvent(false)
                        when(orderTabType){
                            OrderFragment.OrderTabType.NOT_HAVE_PAYMENT -> {
                                _orders.value = it.data!!.filter { it.isPayment == 0 && it.isCancelOrder == 0 }
                            }
                            OrderFragment.OrderTabType.HAVE_PAYMENT -> {
                                _orders.value = it.data!!.filter { it.isPayment == 1 && it.isCancelOrder == 0 }
                            }
                            else -> {
                                _orders.value = it.data!!.filter { it.isCancelOrder == 1 }
                            }
                        }
                    }
                    is Result.Error -> {
                        _loading.value = SingleEvent(false)
                        _message.value = SingleEvent(it.exception.toString())
                    }
                }
            }

        }catch (e: Exception){
            println("err : ${e.localizedMessage}")
        }
    }

    fun addOrder(orderModel: OrderModel) {
        val disposable = orderRepository.addOrder(orderModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _loading.value = SingleEvent(false)
                authListener?.onSuccess(SuccessType.DATA_SUCCESS)
            }, { thw ->
                _loading.value = SingleEvent(false)
                thw.message?.let { _message.value = SingleEvent(it) }
            })

        disposables.add(disposable)
    }

    fun goToPaymentFragment(v: View, orderUI: OrderUI){
        val directions = OrderDetailFragmentDirections.actionOrderDetailFragmentToPaymentFragment2(
            order = orderUI
        )
        v.findNavController().navigate(directions)
    }

    fun goToPINFragment(v: View, orderUI: OrderUI){
        v.findNavController().navigate(OrderDetailFragmentDirections.actionOrderDetailFragmentToPinFragment(orderUI))
    }

    fun onAfterPayment(v: View, orderUI: OrderUI, cashReceived: String, cashChange: Float){
        val orderModel = OrderToModelMapper().orderModel(orderUI)
        orderModel.isPayment = 1
        orderModel.cashReceived = cashReceived.toInt()
        orderModel.cashChange = cashChange

        _loading.value = SingleEvent(true)
        val disposable = orderRepository.updateOrderToFb(orderModel)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                //_loading.value = SingleEvent(false)

                viewModelScope.launch {
                    try{
                        getSingleOrder(orderUI.orderCode).collect {orderUI ->
                            val bundle = bundleOf("orderUI" to orderUI)
                            v.findNavController().navigate(R.id.action_paymentFragment_to_orderDetailFragment,bundle)
                        }
                    }catch (e: Exception){
                        _message.value = SingleEvent(e.message.toString())
                    }
                }

            }, { thw ->
                _loading.value = SingleEvent(false)
                thw.message?.let { _message.value = SingleEvent(it) }
            })

        disposables.add(disposable)
    }

    fun onCancelOrder(orderUI: OrderUI){
        orderUI.isCancelOrder = 1
        val orderModel = OrderToModelMapper().orderModel(orderUI)

        _loading.value = SingleEvent(true)
        val disposable = orderRepository.updateOrderToFb(orderModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewModelScope.launch {
                        try{
                            getSingleOrder(orderUI.orderCode).collect {
                                _loading.value = SingleEvent(false)

                                _singleOrder.value = it
                                authListener?.onSuccess(SuccessType.CANCEL_ORDER_SUCCESS)
                            }
                        }catch (e: Exception){
                            _message.value = SingleEvent(e.message.toString())
                        }
                    }

                }, { thw ->
                    _loading.value = SingleEvent(false)
                    thw.message?.let { _message.value = SingleEvent(it) }
                })

        disposables.add(disposable)
    }

    private fun getSingleOrder(orderId: String) = flow{
        orderRepository.getOrderByOrderId(orderId).collect {
            when(it){
                is Result.Success -> {
                    emit(it.data!!)
                }
                is Result.Error -> {
                    _loading.value = SingleEvent(false)
                    _message.value = SingleEvent(it.exception.toString())
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}