package com.paiwad.myapp.paiwadpos.data.db.entity
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity (
        @PrimaryKey(autoGenerate = true)
        var userId: Long? = 0,
    val uid: String,
    val email: String,
    val branchName: String,
    val phone: String,
    val photoUrl: String?,
    val signOn: Int,
        var PIN: String? = null
        )