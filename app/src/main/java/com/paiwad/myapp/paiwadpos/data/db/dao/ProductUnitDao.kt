package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity

@Dao
interface ProductUnitDao {
    @Query("SELECT *FROM productUnit")
    suspend fun getUnits(): List<UnitEntity>

    @Insert
    suspend fun insertUnitList(units: List<UnitEntity>): List<Long>

    @Query("DELETE FROM productUnit")
    suspend fun clearAllUnit()
}