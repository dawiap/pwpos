package com.paiwad.myapp.paiwadpos.util.state

enum class SuccessType {
    LOGIN_SUCCESS,
    CREATE_SUCCESS,
    LOGOUT_SUCCESS,
    SIGN_UP_SUCCESS,
    SEND_TO_RMAIL_SUCCESS,
    ON_CREATE_USER,
    DATA_SUCCESS,
    CANCEL_ORDER_SUCCESS,
    WARING_HAS_DATA,
}