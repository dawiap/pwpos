package com.paiwad.myapp.paiwadpos.data.mappers.category

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.Mapper
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI

class CategoryToEntityMapper: FromToMapper<CategoryModel, CategoryUI, CategoryEntity> {
    override fun fromWayOne(data: CategoryModel) = CategoryEntity(
            categoryCode = data.categoryId,
            categoryName = data.categoryName!!,
            categoryColor = data.categoryColor!!,
            branchId = data.branchId!!
    )

    override fun fromWayTwo(data: CategoryUI) = CategoryEntity(
            categoryCode = data.categoryCode,
            categoryName = data.categoryName,
            categoryColor = data.categoryColor,
            branchId = data.branchId
    )
}