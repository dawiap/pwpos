package com.paiwad.myapp.paiwadpos.viewmodel

import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.data.mappers.order.OrderToModelMapper
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import com.paiwad.myapp.paiwadpos.view.ui.PaymentFragmentDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(): ViewModel() {

    private val _cashReceived = MutableLiveData("")
    val cashReceived: LiveData<String> = _cashReceived

    private val _totalAmount = MutableLiveData<Float>()
    val totalAmount: LiveData<Float> = _totalAmount

    private val _change = MutableLiveData(0.toFloat())
    val change: LiveData<Float> = _change

    private val _isAfterPayment = MutableLiveData(false)
    val isAfterPayment: LiveData<Boolean> = _isAfterPayment

    private val _badgeWithProduct = MutableLiveData<List<BadgeWithProduct>>()

    fun setInitTotal(total: Float) {
        _totalAmount.value = total
    }

    fun setInitBadges(badgeWithProducts: List<BadgeWithProduct>) {
        _badgeWithProduct.value = badgeWithProducts
    }

    fun setIsAfterPayment(){
        _isAfterPayment.value = true
    }

    fun getNumber(newText: String) {
        val oldText = cashReceived.value
        _cashReceived.value = oldText + newText

        if (_cashReceived.value!!.toFloat() > _totalAmount.value!!) {
            _change.value = _cashReceived.value!!.toFloat() - _totalAmount.value!!
        }
    }

    fun onClearNumber() {
        _cashReceived.value = ""
        _change.value = 0.toFloat()
    }

    fun backToBadgeFragment(v: View) {
        v.findNavController()
                .navigate(PaymentFragmentDirections.actionPaymentFragmentToBadgeFragment())
    }

    fun backToOrderDetailFragment(v: View){
        v.findNavController()
            .navigate(R.id.action_paymentFragment_to_orderDetailFragment)
    }

    fun toOrderModelMapper(): OrderModel {
        val order = orderToModelMapper.orderModel(_badgeWithProduct.value!!)
        order.isPayment =1
        order.totalAmount = _totalAmount.value!!
        order.cashReceived = _cashReceived.value!!.toInt()
        order.cashChange = _change.value!!
        return order
    }

    private val orderToModelMapper = OrderToModelMapper()

}