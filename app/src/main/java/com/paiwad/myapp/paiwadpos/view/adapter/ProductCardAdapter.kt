package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ShrProductCardBinding
import com.paiwad.myapp.paiwadpos.view.model.ProductUI

class ProductCardAdapter: RecyclerView.Adapter<ProductCardAdapter.ProductViewHolder>() {

    lateinit var clickListener: ((ProductUI) -> Unit)

    inner class ProductViewHolder(val binding: ShrProductCardBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: ProductUI, clickListener: (ProductUI) -> Unit){
            binding.product = item
            binding.root.setOnClickListener {
                clickListener(item)
            }
        }
    }

    /*private val callBack = object : DiffUtil.ItemCallback<Product>(){
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.productId == newItem.productId
        }

    }

    val differ = AsyncListDiffer(this, callBack)*/
    private var products = emptyList<ProductUI>()

    fun setItems(products: List<ProductUI>){
        this.products = products
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
                ShrProductCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(products[position], clickListener)
    }

    override fun getItemCount(): Int = products.size
}