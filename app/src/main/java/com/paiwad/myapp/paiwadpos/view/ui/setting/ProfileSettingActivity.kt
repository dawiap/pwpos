package com.paiwad.myapp.paiwadpos.view.ui.setting

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.databinding.ActivityProfileSettingBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomSelectImageLayoutBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomSheetProfileEditorBinding
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.ui.manage.AddProductActivity
import com.paiwad.myapp.paiwadpos.viewmodel.EditorType
import com.paiwad.myapp.paiwadpos.viewmodel.SettingViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.ViewState
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class ProfileSettingActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityProfileSettingBinding

    private val userViewModel: UserViewModel by viewModels()

    private val settingsViewModel: SettingViewModel by viewModels()

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    private lateinit var bottomSelectImageLayoutBinding: BottomSelectImageLayoutBinding

    private lateinit var bottomSheetProfileEditorBinding: BottomSheetProfileEditorBinding

    private lateinit var bottomImageDialog: BottomSheetDialog

    private lateinit var bottomEditorDialog: BottomSheetDialog

    private var photoPath: String = ""

    private val REQUEST_STORAGE_CODE = 12

    private val REQUEST_CAPTURE_CODE = 13

    lateinit var editorType: EditorType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_setting)
        binding.viewModel = settingsViewModel
        binding.lifecycleOwner = this

        userViewModel.getUserProfile()
        userViewModel.userProfile.observe(this, { user ->
            user?.let {
                settingsViewModel.setUserUI(it)
            }
        })

        setupBottomSheetImageSelector()
        binding.buttonChange.setOnClickListener {
            bottomImageDialog.show()
        }

        binding.layout1.setOnClickListener(this)
        binding.layout2.setOnClickListener(this)
        binding.layout3.setOnClickListener(this)
        binding.imageClose.setOnClickListener(this)

        settingsViewModel.viewState.observe(this, {
            when (it) {
                is ViewState.Loaded -> {
                    progressBuilder.dismissProgressDialog()
                    userViewModel.getUserProfile()
                }
                is ViewState.LoadFailure -> {
                    progressBuilder.dismissProgressDialog()
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_LONG).show()
                }
                is ViewState.Loading -> {
                    progressBuilder.showProgressDialog()
                }
            }
        })
    }

    private fun setupBottomSheetImageSelector() {
        bottomSelectImageLayoutBinding =
            BottomSelectImageLayoutBinding.inflate(layoutInflater, null, false)
        bottomImageDialog = BottomSheetDialog(this).apply {
            setContentView(bottomSelectImageLayoutBinding.root)
        }
        bottomSelectImageLayoutBinding.buttonPixabay.visibility = View.GONE

        bottomSelectImageLayoutBinding.buttonDirectory.setOnClickListener(this)
        bottomSelectImageLayoutBinding.buttonCapture.setOnClickListener(this)
        bottomSelectImageLayoutBinding.imageClose.setOnClickListener(this)
    }

    private fun setupBottomSheetEditor(hint: String) {
        bottomSheetProfileEditorBinding =
            BottomSheetProfileEditorBinding.inflate(layoutInflater, null, false)
        bottomEditorDialog = BottomSheetDialog(this).apply {
            setContentView(bottomSheetProfileEditorBinding.root)
        }
        bottomSheetProfileEditorBinding.textInputLayoutName.hint = hint
        bottomSheetProfileEditorBinding.buttonSave.setOnClickListener(this)
        bottomSheetProfileEditorBinding.imageClose.setOnClickListener(this)
    }

    private fun takePhoto() {
        val extDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        val photoFile = File.createTempFile("IMG${System.currentTimeMillis()}", ".jpg", extDir)

        photoPath = photoFile.absolutePath

        val uri = FileProvider.getUriForFile(this, "com.paiwad.myapp.paiwadpos", photoFile)

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_CAPTURE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK)
            return

        when (requestCode) {
            REQUEST_STORAGE_CODE -> {
                val uri = data?.data
                Glide.with(this@ProfileSettingActivity)
                    .load(uri)
                    .into(binding.imageProfile)

                uri?.let {
                    settingsViewModel.uploadChangeImage(it)
                }
            }
            REQUEST_CAPTURE_CODE -> {
                val photoFile = File(photoPath)
                val uri = Uri.fromFile(photoFile)
                Glide.with(this@ProfileSettingActivity)
                    .load(uri)
                    .into(binding.imageProfile)

                uri?.let {
                    settingsViewModel.uploadChangeImage(it)
                }
            }

        }
    }

    override fun onClick(v: View?) {
        when (v) {
            bottomSelectImageLayoutBinding.buttonDirectory -> {
                bottomImageDialog.dismiss()

                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "image/*"
                startActivityForResult(intent, REQUEST_STORAGE_CODE)
            }

            bottomSelectImageLayoutBinding.buttonCapture -> {
                bottomImageDialog.dismiss()
                takePhoto()
            }

            bottomSelectImageLayoutBinding.imageClose -> {
                bottomImageDialog.dismiss()
            }

            binding.layout1 -> {
                setupBottomSheetEditor(resources.getString(R.string.edit_text_branch_name_hint))
                editorType = EditorType.BRANCH
                bottomEditorDialog.show()
            }

            binding.layout2 -> {
                setupBottomSheetEditor(resources.getString(R.string.edit_text_phone_number_hint))
                editorType = EditorType.PHONE
                bottomEditorDialog.show()
            }

            binding.layout3 -> {
                setupBottomSheetEditor(resources.getString(R.string.edit_text_email_hint))
                editorType = EditorType.EMAIL
                bottomEditorDialog.show()
            }

            binding.imageClose -> {
                finish()
            }

            bottomSheetProfileEditorBinding.imageClose -> {
                bottomEditorDialog.dismiss()
            }

            bottomSheetProfileEditorBinding.buttonSave -> {
                bottomEditorDialog.dismiss()
                settingsViewModel.updateProfile(
                    editorType,
                    bottomSheetProfileEditorBinding.editName.text.toString()
                )
            }
        }
    }

    override fun onBackPressed() {
        Log.e("Back TAG", "onBackPressed")
    }
}