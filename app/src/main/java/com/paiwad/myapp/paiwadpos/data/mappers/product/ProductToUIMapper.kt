package com.paiwad.myapp.paiwadpos.data.mappers.product

import com.paiwad.myapp.paiwadpos.data.api.model.ProductModel
import com.paiwad.myapp.paiwadpos.data.db.relation.ProductWithUnitAndOptions
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.view.model.OptionUI
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

class ProductToUIMapper: FromToMapper<ProductModel, ProductWithUnitAndOptions, ProductUI> {
    override fun fromWayOne(data: ProductModel) = ProductUI(
            productCode = data.productId!!,
            productName = data.productName!!,
            productPrice = data.productPrice!!.toDouble(),
            imagePath = data.imagePath!!,
            categoryCode = data.categoryCode!!,
            branchId = data.branchId!!,
            inStock = data.inStock!!,
            outStock = data.outStock!!,
            demandStock = data.demandStock!!,
            isOption = data.isOption,
            isStock = data.isStock,
            unitUI = UnitUI(
                    unitCode = data.unitModel?.unitId!!,
                    unitName = data.unitModel?.unitName!!,
                    branchId = data.unitModel?.branchId!!,
            ),
            optionUI = data.options?.map {
                OptionUI(
                        optionCode = it.optionId,
                        productCode = it.productCode,
                        optionName = it.optionName!!,
                        optionPrice = it.optionPrice!!.toDouble()
                )
            }
    )

    override fun fromWayTwo(data: ProductWithUnitAndOptions) = ProductUI(
            productId = data.product.product.productId,
            productCode = data.product.product.productCode!!,
            productName = data.product.product.productName,
            productPrice = data.product.product.productPrice.toDouble(),
            imagePath = data.product.product.imagePath!!,
            categoryCode = data.product.product.categoryCode,
            branchId = data.product.product.branchId,
            inStock = data.product.product.inStock,
            outStock = data.product.product.outStock,
            demandStock = data.product.product.demandStock,
            isOption = data.product.product.isOption,
            isStock = data.product.product.isStock,
            unitUI = UnitUI(
                    unitCode = data.product.unit.unitCode!!,
                    unitName = data.product.unit.unitName,
                    branchId = data.product.unit.branchId,
            ),
            optionUI = data.options.map {
                OptionUI(
                        optionCode = it.optionCode,
                        productCode = it.productCode,
                        optionName = it.optionName,
                        optionPrice = it.optionPrice
                )
            }
    )
}