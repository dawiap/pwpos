package com.paiwad.myapp.paiwadpos.data

import android.util.Log
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.db.dao.CategoryDao
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity
import com.paiwad.myapp.paiwadpos.data.mappers.category.CategoryToEntityMapper
import com.paiwad.myapp.paiwadpos.data.mappers.category.CategoryToModelMapper
import com.paiwad.myapp.paiwadpos.data.mappers.category.CategoryToUIMapper
import com.paiwad.myapp.paiwadpos.data.repository.CategoryRepository
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import io.reactivex.Completable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CategoryRepositoryImpl(
    private val categoryDao: CategoryDao,
    private val firebaseRef: FirebaseDatabaseReference
    ):
        CategoryRepository {

    private val entityMapper = CategoryToEntityMapper()
    private val uiMapper = CategoryToUIMapper()
    private val modelMapper = CategoryToModelMapper()

    override suspend fun getCategories(branchId: String) = flow {
        try {
            val cacheData = categoryDao.getCategories()
            if (cacheData.isNullOrEmpty()) {

                updateCache(branchId).collect { result ->
                    when (result) {
                        is Result.Success -> {
                            Log.i("Category From Network: ", result.data.toString())
                            val categoryUI: List<CategoryUI> =
                                    result.data!!.map { uiMapper.fromWayOne(it) }
                            emit(Result.Success(categoryUI))
                        }
                        is Result.Error -> {
                            Log.i("Error : ", result.exception.toString())
                            emit(Result.Error(result.exception))
                        }
                    }
                }

            } else {
                Log.i("Category From Cache: ", cacheData.toString())
                val categoryUI = cacheData.map { uiMapper.fromWayTwo(it) }
                emit(Result.Success(categoryUI))
            }
        } catch (e: Exception) {
            Log.i("Error : ", e.message.toString())
            emit(Result.Error(e))
        }
    }

    override fun addCategory(categoryUI: CategoryUI) = firebaseRef.addDataCategory(toCategoryModel(categoryUI))

    override fun deleteCategory(categoryUI: CategoryUI) = firebaseRef.deleteCategory(categoryUI.categoryCode)

    override fun checkProductBeforeDeleteCategory(categoryId: String) =
            firebaseRef.checkProductForDeleteCategory(categoryId)


    override fun updateCategoryInFb(categoryUI: CategoryUI) =
            firebaseRef.updateCategory(toCategoryModel(categoryUI))

    override suspend fun updateCache(branchId: String): Flow<Result<List<CategoryModel>>> = flow {
        firebaseRef.getDataCategories(branchId)
                .onEach { result ->
                    when (result) {
                        is Result.Success -> {
                            categoryDao.clearAllCategory()

                            categoryDao.insertCategoryList(result.data!!.map {
                                entityMapper.fromWayOne(
                                        it
                                )
                            })
                            Log.i("Category action", "category updated!")
                        }
                        else -> {
                        }
                    }
                }
                .collect {
                    emit(it)
                }
    }

    private fun toCategoryModel(categoryUI: CategoryUI) = modelMapper.toData(categoryUI)
}