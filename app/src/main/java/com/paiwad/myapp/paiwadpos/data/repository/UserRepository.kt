package com.paiwad.myapp.paiwadpos.data.repository

import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseUser
import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity
import com.paiwad.myapp.paiwadpos.util.state.Result
import io.reactivex.Completable
import kotlinx.coroutines.flow.Flow

interface UserRepository{
    fun loginWithEmailAndPassword(email: String, password: String): Completable

    fun loginWithGoogle(credential: AuthCredential): Completable

    fun loginWithFacebook(credential: AuthCredential): Completable

    fun registerWithEmailAndPassword(email: String, password: String): Completable

    fun resetPasswordWithEmail(email: String): Completable

    fun createUserToDb(user: UserEntity): Completable

    fun currentUser(): FirebaseUser?

    fun logout()

    fun getDataUser(uid: String): Flow<Result<UserEntity>?>

    fun updateUserLogout(user: UserEntity): Completable

    fun checkYourPIN(pin: String): Flow<Result<Boolean>>
}