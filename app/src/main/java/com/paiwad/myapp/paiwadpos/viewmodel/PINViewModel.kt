package com.paiwad.myapp.paiwadpos.viewmodel

import android.view.View
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.paiwad.myapp.paiwadpos.data.repository.UserRepository
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import com.paiwad.myapp.paiwadpos.view.ui.PINFragmentDirections
import com.paiwad.myapp.paiwadpos.view.ui.user.CreatePinFragmentDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PINViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel() {

    private val pins = mutableListOf<Int>()
    private val _pinIndex = MutableLiveData(-1)
    val pinIndex: LiveData<Int> = _pinIndex

    private val _yourPIN = MutableLiveData("")
    val yourPIN: LiveData<String> = _yourPIN

    private val _isCorrect = MutableLiveData<SingleEvent<Boolean>>()
    val isCorrect: LiveData<SingleEvent<Boolean>> = _isCorrect

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    fun getNumber(newPIN: String) {

        if (pins.size < 4) {
            pins.add(newPIN.toInt())
            _pinIndex.value = pins.size
        }

        if (pins.size == 4) {
            _yourPIN.value = ""
            pins.forEach {
                _yourPIN.value += it
            }
        }
    }

    fun clearPIN(){
        pins.clear()
        _pinIndex.value = pins.size
        _yourPIN.value = ""
    }

    fun onRemoveNumber() {
        if (pins.size > 0) {
            pins.removeLast()
            _pinIndex.value = pins.size
        }
    }

    fun checkYourPIN(pin: String) = viewModelScope.launch {
        try {
            userRepository.checkYourPIN(pin).collect {
                when (it) {
                    is Result.Success -> {
                        _isCorrect.postValue(SingleEvent(it.data!!))
                        if(!it.data)
                            _message.postValue(SingleEvent("Invalid your PIN!!"))
                    }
                    is Result.Error -> _message.postValue(SingleEvent(it.exception.message.toString()))
                }
            }
        }catch (e: Exception){
            println("e : "+e.message)
        }

    }

    fun backToCreateUserFragment(v: View) {
        v.findNavController()
            .navigate(CreatePinFragmentDirections.actionCreatePINFragmentToCreateUserFragment())
    }

    fun backToOrderDetailFragment(v: View) {
        v.findNavController()
            .navigate(PINFragmentDirections.actionNavPinToOrderDetailFragment(null))
    }
}