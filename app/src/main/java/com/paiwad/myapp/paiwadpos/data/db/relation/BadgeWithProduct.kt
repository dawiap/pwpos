package com.paiwad.myapp.paiwadpos.data.db.relation

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class BadgeWithProduct(
        @Embedded var badge: BadgeEntity,
        @Relation(
                parentColumn = "badgeProductCode",
                entityColumn = "productCode"
        )
        val product: ProductEntity?
        ): Parcelable