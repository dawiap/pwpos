package com.paiwad.myapp.paiwadpos.view.ui.manage

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityAddCategoryBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomSheetManageCatgoryBinding
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.InternetCheck
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.adapter.CategoryAddItemAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.CategoryViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.internal.toHexString
import petrov.kristiyan.colorpicker.ColorPicker
import javax.inject.Inject

@AndroidEntryPoint
class AddCategoryActivity : AppCompatActivity(), View.OnClickListener, UserAuthListener {

    private val userViewModel: UserViewModel by viewModels()

    private val categoryViewModel: CategoryViewModel by viewModels()

    @Inject
    lateinit var categoryAddItemAdapter: CategoryAddItemAdapter

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    @Inject
    lateinit var alertDialogBuilder: AlertDialogBuilder

    @Inject lateinit var internetCheck: InternetCheck

    private lateinit var binding: ActivityAddCategoryBinding

    private lateinit var bottomSheetView: BottomSheetManageCatgoryBinding

    private lateinit var bottomDialog: BottomSheetDialog

    private var colors = ArrayList<String>()
    private var colorSelected: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_category)
        binding.lifecycleOwner = this
        binding.viewModel = categoryViewModel


        userViewModel.getUserProfile()
        userViewModel.userProfile.observe(this,{user ->
            user?.let {
                categoryViewModel.branchId = it.uid
                categoryViewModel.getCategoriesFromFb(it.uid, internetCheck.isNetworkAvailable())
            }

        })

        categoryViewModel.authListener = this

        val stringColors = resources.getStringArray(R.array.colors).toList()
        colors.addAll(stringColors)

        setupBottomSheet()

        binding.buttonAdd.setOnClickListener(this)
        binding.imageClose.setOnClickListener(this)

        binding.rcvCategory.adapter = categoryAddItemAdapter
        categoryAddItemAdapter.clickListener = {view, item, position ->
            showMenu(view, R.menu.manage_context_menu, item, position)
        }
    }

    override fun onResume() {
        super.onResume()
        categoryViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    private fun setupBottomSheet() {
        bottomSheetView = BottomSheetManageCatgoryBinding.inflate(layoutInflater, null, false)
        bottomSheetView.viewModel = categoryViewModel
        bottomSheetView.lifecycleOwner = this

        bottomDialog = BottomSheetDialog(this).apply {
            setContentView(bottomSheetView.root)
        }

        bottomSheetView.buttonChooseColor.setOnClickListener(this)
        bottomSheetView.imageClose.setOnClickListener(this)
    }

    private fun showMenu(v: View, @MenuRes menuRes: Int, item: CategoryUI, position: Int) {
        val popup = android.widget.PopupMenu(this, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when(menuItem.itemId){
                R.id.edit->{

                    categoryViewModel.categoryItem = item

                    categoryViewModel._isActionAdd.value = false
                    categoryViewModel._titleManageCategory.value = resources.getString(R.string.text_button_edit_category)
                    categoryViewModel.categoryName.value = item.categoryName
                    categoryViewModel._categoryColorCode.value = item.categoryColor

                    bottomDialog.show()
                }
                R.id.delete ->{
                    categoryViewModel.checkDataBeforeDelete(item)
                    categoryAddItemAdapter.notifyItemChanged(position)
                }
            }
            false
        }
        popup.show()
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.buttonAdd -> {
                categoryViewModel._isActionAdd.value = true
                categoryViewModel._titleManageCategory.value = resources.getString(R.string.text_button_add_category)
                bottomDialog.show()
            }

            binding.imageClose -> {
                finish()
            }

            bottomSheetView.buttonChooseColor -> {
                ColorPicker(this)
                        .setOnFastChooseColorListener(object : ColorPicker.OnFastChooseColorListener {
                            override fun setOnFastChooseColorListener(position: Int, color: Int) {

                                colorSelected = "#" + color.toHexString().substring(2)

                                categoryViewModel._categoryColorCode.value = colorSelected
                            }

                            override fun onCancel() {
                            }
                        })
                        .setColors(colors)
                        .setColumns(5)
                        .setRoundColorButton(true)
                        .show()
            }

            bottomSheetView.imageClose -> {
                bottomDialog.dismiss()
            }

        }
    }

    override fun onSuccess(successType: SuccessType) {
        if (successType == SuccessType.DATA_SUCCESS)
            bottomDialog.dismiss()

        if(successType == SuccessType.WARING_HAS_DATA)
            alertDialogBuilder.showMessageAlertDialog("Failure", resources.getString(R.string.message_have_data_in_category))
    }

    override fun onFailure(message: String) {
        alertDialogBuilder.showMessageAlertDialog("Failure", message)
    }
}

