package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.paiwad.myapp.paiwadpos.data.db.entity.OrderDetailEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.OrderEntity

@Dao
interface OrderDao {
    @Query("SELECT *FROM ops_order")
    suspend fun getOrders(): List<OrderEntity>

    @Query("SELECT *FROM ops_order WHERE orderId IN (:orderId)")
    suspend fun getOrderByOrderId(orderId: Long): OrderEntity

    /*@Query("SELECT orderDetailId,ops_orderdetail.productId,productPrice,productName,imagePath,product.categoryCode,categoryName FROM ops_orderdetail LEFT JOIN product ON ops_orderdetail.productId = product.productId LEFT JOIN category ON product.categoryCode = category.categoryCode WHERE orderDetailId IN (:orderId)")
    suspend fun getOrderDetailByOrderId(orderId: Long): OrderDetailEntity*/

    @Insert
    suspend fun insertOrder(order: OrderEntity)

    @Insert
    suspend fun insertOrderDetail(orderDetail: OrderDetailEntity)

    @Delete
    suspend fun deleteOrder(order: OrderEntity)
}