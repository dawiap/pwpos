package com.paiwad.myapp.paiwadpos.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityBadgeBinding
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.adapter.BadgeProductListAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.BadgeViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.PaymentViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BadgeActivity : AppCompatActivity() {

    val badgeViewModel: BadgeViewModel by viewModels()

    val userViewModel: UserViewModel by viewModels()

    val orderViewModel: OrderViewModel by viewModels()

    val paymentViewModel: PaymentViewModel by viewModels()

    @Inject
    lateinit var badgeProductListAdapter: BadgeProductListAdapter

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    @Inject
    lateinit var alertDialogBuilder: AlertDialogBuilder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityBadgeBinding>(this, R.layout.activity_badge)

        supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        userViewModel.getUserProfile()
        //val navController = navHostFragment.navController
    }

    override fun onBackPressed() {
        println("do someting")
    }
}