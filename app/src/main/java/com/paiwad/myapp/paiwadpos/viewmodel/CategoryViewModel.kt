package com.paiwad.myapp.paiwadpos.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.data.repository.CategoryRepository
import com.paiwad.myapp.paiwadpos.data.repository.ProductRepository
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(
        private val categoryRepository: CategoryRepository,
        private val productRepository: ProductRepository
): ViewModel() {

    var categoryName = MutableLiveData<String?>()
    val _categoryColorCode = MutableLiveData<String?>()
    var branchId: String? = null

    val _titleManageCategory = MutableLiveData<String>()
    val titleManageCategory: LiveData<String> = _titleManageCategory

    val _isActionAdd = MutableLiveData<Boolean>()
    val isAction: LiveData<Boolean> = _isActionAdd

    val categoryColorCode: LiveData<String?> = _categoryColorCode

    var authListener: UserAuthListener? = null

    private val _categoryList = MutableLiveData<List<CategoryUI>>()

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()

    val loading: LiveData<SingleEvent<Boolean>> = _loading

    var categoryItem: CategoryUI? = null

    val categoryList: LiveData<List<CategoryUI>> = _categoryList

    private val disposables = CompositeDisposable()

    fun getCategoriesFromFb(branchId: String, isNetworkAvailable: Boolean? = null) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        categoryRepository.getCategories(branchId).collect { result ->
            _loading.value = SingleEvent(false)
            when (result) {
                is Result.Success -> {
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                    _categoryList.value = result.data!!
                }
                is Result.Error -> {
                    authListener?.onFailure(result.exception.message.toString())
                }
            }
        }
    }

    fun addCategoryToFb() {
        if (categoryName.value.isNullOrEmpty() || categoryColorCode.value.isNullOrEmpty() || branchId.isNullOrEmpty()) {
            authListener?.onFailure("Please enter require field")
            return
        }

        _loading.value = SingleEvent(true)
        val disposable = categoryRepository.addCategory(toCategoryUIMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)

                    updateCache(toCategoryUIMapper().branchId)

                    clearView()
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun checkDataBeforeDelete(category: CategoryUI) {
        _loading.value = SingleEvent(true)
        val disposable = categoryRepository.checkProductBeforeDeleteCategory(category.categoryCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it) {
                        authListener?.onSuccess(SuccessType.WARING_HAS_DATA)
                        _loading.value = SingleEvent(false)
                    } else {
                        deleteCategory(category)
                    }
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    private fun deleteCategory(category: CategoryUI) {
        _loading.value = SingleEvent(true)
        val disposable = categoryRepository.deleteCategory(category)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)

                    updateCache(category.branchId)

                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun updateCategory() {
        if (categoryName.value.isNullOrEmpty() || categoryColorCode.value.isNullOrEmpty() || categoryItem == null) {
            authListener?.onFailure("Please enter require field")
            return
        }

        _loading.value = SingleEvent(true)
        val disposable = categoryRepository.updateCategoryInFb(toCategoryUIUpdateMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)

                    updateCache(toCategoryUIUpdateMapper().branchId)

                    clearView()
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                }, { err ->
                    _loading.value = SingleEvent(false)
                    err.message?.let {
                        authListener?.onFailure(it)
                    } ?: authListener?.onFailure(err.toString())
                })

        disposables.add(disposable)
    }

    private fun updateCache(branchId: String) = viewModelScope.launch {
        categoryRepository.updateCache(branchId).collect { result ->
            when(result){
                is Result.Success -> {
                    getCategoriesFromFb(branchId)
                }
                is Result.Error -> {
                    Log.e("VM Update Error", result.exception.message.toString())
                }
            }
        }
    }

    private fun clearView() {
        categoryName.value = null
        _categoryColorCode.value = null
        categoryItem = null
    }

    private fun toCategoryUIMapper() = CategoryUI(
            categoryCode = "",
            categoryName = categoryName.value!!,
            categoryColor = _categoryColorCode.value!!,
            branchId = branchId!!
    )

    private fun toCategoryUIUpdateMapper() = CategoryUI(
            categoryCode = categoryItem!!.categoryCode,
            categoryName = categoryName.value!!,
            categoryColor = _categoryColorCode.value!!,
            branchId = categoryItem!!.branchId,
    )

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
