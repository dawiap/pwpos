package com.paiwad.myapp.paiwadpos.util.adapter

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.util.view.SimpleItemDecor
import com.paiwad.myapp.paiwadpos.view.adapter.*
import com.paiwad.myapp.paiwadpos.view.model.*


@BindingAdapter("app:url")
fun setImage(imageView: ImageView, url: String?) {
    url?.let {
        Glide.with(imageView.context)
            .load(it)
            .error(R.drawable.ic_action_empty)
            .placeholder(R.drawable.ic_action_empty)
            .into(imageView)
    }
}

@BindingAdapter("backgroundTintBinding")
fun backgroundTintBinding(view: View, colorCode: String?) {
    colorCode?.let {
        val gradientDrawable = view.background as GradientDrawable
        gradientDrawable.setColor(Color.parseColor(it))
    }
}

@BindingAdapter("android:shouldClose")
fun setShouldClose(viewGroup: ViewGroup, shouldClose: Boolean){
    if(shouldClose){
        val binding = DataBindingUtil.getBinding<ViewDataBinding>(viewGroup)
        if(binding!=null)
            (binding.lifecycleOwner as Activity).finish()
    }
}

@BindingAdapter("app:setImageTile")
fun setImageTile(imageView: ImageView, count: Int?) {
    count?.let {
        if (imageView.id == R.id.imgPlus) {
            imageView.setColorFilter(imageView.context.resources.getColor(R.color.colorPrimary))
        }else{
            if (it > 1) {
                imageView.setColorFilter(imageView.context.resources.getColor(R.color.colorPrimary))
            } else {
                imageView.setColorFilter(Color.GRAY)
            }
        }
    }
}

@BindingAdapter("app:setImageActionTile")
fun setImageActionTile(imageView: ImageView, isComplete: Boolean?) {
    isComplete?.let {
        if (it) {
            imageView.isEnabled = true
            imageView.setColorFilter(imageView.context.resources.getColor(R.color.colorPrimary))
        }else{
            imageView.isEnabled = false
            imageView.setColorFilter(Color.GRAY)
        }
    }
}

@BindingAdapter("app:iconSetting")
fun setIconSetting(imageView: ImageView, drawable: Int?) {
    drawable?.let {
        imageView.setImageResource(it)
    }
}


