package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.db.relation.ProductWithUnitAndOptions

@Dao
interface ProductDao {
    @Transaction
    @Query("SELECT *FROM product")
    suspend fun getProducts(): List<ProductWithUnitAndOptions>

    @Transaction
    @Query("SELECT *FROM product WHERE productName LIKE :q")
    suspend fun searchProducts(q: String): List<ProductWithUnitAndOptions>

    @Transaction
    @Query("SELECT *FROM product where categoryCode IN (:categoryCode)")
    suspend fun getProductsWithUnitAndOptions(categoryCode: String): List<ProductWithUnitAndOptions>

    @Insert
    suspend fun insertProductList(products: List<ProductEntity>): List<Long>

    @Query("DELETE FROM product")
    suspend fun clearAllProduct()

    @Transaction
    suspend fun updateProduct(products: List<ProductEntity>){
        clearAllProduct()
        insertProductList(products)
    }
}