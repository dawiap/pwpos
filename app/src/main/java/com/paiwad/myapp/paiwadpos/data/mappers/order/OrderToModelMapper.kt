package com.paiwad.myapp.paiwadpos.data.mappers.order

import com.paiwad.myapp.paiwadpos.data.api.model.OptionModel
import com.paiwad.myapp.paiwadpos.data.api.model.OrderDetailModel
import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.SingleMapper
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import java.text.SimpleDateFormat
import java.util.*

class OrderToModelMapper {

    private val formatterOut = SimpleDateFormat("dd MMM yyyy")

    fun orderModel(badgeWithProduct: List<BadgeWithProduct>) = OrderModel(
            orderDate = formatterOut.format(Date()),
            branchId = badgeWithProduct.first().product?.branchId,
            isPayment = 0,
            isCancelOrder = 0,
            cashReceived = null,
            cashChange = null,
            orderDetailModel = badgeWithProduct.map {
                OrderDetailModel(
                        productCode = it.product?.productCode,
                        productPrice = it.product?.productPrice.toString(),
                        productCount = it.badge.badgeCount,
                        productTotalAmount = (if (it.product?.isOption == true)
                            it.badge.badgeOptionPrice!! * it.badge.badgeCount else
                            it.product?.productPrice!! * it.badge.badgeCount).toString(),
                        productName = it.product.productName,
                        imagePath = it.product.imagePath,
                        unitModel = null,
                        optionModel = it.badge.badgeOptionCode?.let { opt ->
                            OptionModel(
                                    optionId = it.badge.badgeOptionCode,
                                    optionPrice = it.badge.badgeOptionPrice.toString(),
                                    optionName = it.badge.badgeOptionName
                            )
                        }
                )
            }
    )

    fun orderModel(orderUI: OrderUI) = OrderModel(
        orderId = orderUI.orderCode,
        orderDate = orderUI.orderDate,
        branchId = orderUI.branchId,
        isPayment = orderUI.isPayment,
            isCancelOrder = orderUI.isCancelOrder,
        cashReceived = orderUI.cashReceived,
        cashChange = orderUI.cashChange,
        totalAmount = orderUI.totalAmount,
        orderDetailModel = orderUI.orderDetailModel.map {
            OrderDetailModel(
                orderDetailId = it.orderDetailCode,
                productCode = it.productCode,
                productPrice = it.productPrice.toString(),
                productCount = it.productCount,
                productTotalAmount = it.productTotalAmount,
                productName = it.productName,
                imagePath = it.imagePath,
                unitModel = null,
                optionModel = it.optionModel?.let { opt ->
                    OptionModel(
                        optionId = opt.optionCode,
                        optionPrice = opt.optionPrice.toString(),
                        optionName = opt.optionName
                    )
                }
            )
        }
    )
}