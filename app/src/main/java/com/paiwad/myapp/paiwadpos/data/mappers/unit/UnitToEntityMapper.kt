package com.paiwad.myapp.paiwadpos.data.mappers.unit

import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.Mapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.SingleMapper
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

class UnitToEntityMapper: FromToMapper<UnitModel, UnitUI, UnitEntity> {
    override fun fromWayOne(data: UnitModel) = UnitEntity(
            branchId = data.branchId!!,
            unitCode = data.unitId!!,
            unitName = data.unitName!!,
    )

    override fun fromWayTwo(data: UnitUI) = UnitEntity(
            branchId = data.branchId,
            unitCode = data.unitCode,
            unitName = data.unitName,
    )
}