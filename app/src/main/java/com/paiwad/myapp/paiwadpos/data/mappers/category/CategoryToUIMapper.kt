package com.paiwad.myapp.paiwadpos.data.mappers.category

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.Mapper
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI

class CategoryToUIMapper: FromToMapper<CategoryModel, CategoryEntity, CategoryUI> {

    override fun fromWayOne(data: CategoryModel) = CategoryUI(
            categoryCode = data.categoryId!!,
            categoryName = data.categoryName!!,
            categoryColor = data.categoryColor!!,
            branchId = data.branchId!!
    )

    override fun fromWayTwo(data: CategoryEntity) = CategoryUI(
            categoryCode = data.categoryCode!!,
            categoryName = data.categoryName,
            categoryColor = data.categoryColor,
            branchId = data.branchId
    )

}