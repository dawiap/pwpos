package com.paiwad.myapp.paiwadpos.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.annotation.MenuRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.BottomSheetManageLayoutBinding
import com.paiwad.myapp.paiwadpos.databinding.CustomCategoryTabViewBinding
import com.paiwad.myapp.paiwadpos.databinding.FragmentManageBinding
import com.paiwad.myapp.paiwadpos.viewmodel.CategoryViewModel
import com.paiwad.myapp.paiwadpos.view.adapter.ProductListAdapter
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import com.paiwad.myapp.paiwadpos.util.view.SimpleItemDecor
import com.paiwad.myapp.paiwadpos.viewmodel.ManageViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.ProductViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ManageFragment : Fragment() {

    private lateinit var categoryViewModel: CategoryViewModel

    private lateinit var productViewModel: ProductViewModel

    private val manageViewModel: ManageViewModel by viewModels()

    @Inject
    lateinit var productListAdapter: ProductListAdapter

    private lateinit var binding: FragmentManageBinding

    private lateinit var bottomSheetView: BottomSheetManageLayoutBinding

    private lateinit var bottomDialog: BottomSheetDialog

    private var branchId: String? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_manage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentManageBinding.bind(view)
        binding.lifecycleOwner = this.viewLifecycleOwner
        binding.viewModel = manageViewModel

        categoryViewModel = (activity as MainActivity).categoryViewModel

        productViewModel = (activity as MainActivity).productViewModel

        setupBottomSheet()

        binding.buttonCateAdd.setOnClickListener { bottomDialog.show() }
    }

    override fun onResume() {
        super.onResume()

        (activity as MainActivity).userViewModel.userProfile.observe(viewLifecycleOwner,{ user ->
            user?.let {
                branchId = it.uid

                initTabCategory(it.uid)
                categoryViewModel.getCategoriesFromFb(it.uid, true)
            }
        })

        productViewModel.productList.observe(this, {

            val itemDecor = SimpleItemDecor(requireContext())
            binding.rcvPro.addItemDecoration(itemDecor)

            productListAdapter.differ.submitList(it)

            binding.rcvPro.adapter = productListAdapter

            productListAdapter.clickListener = { v, item, position ->
                showMenu(v, R.menu.manage_context_menu, item, position)
            }
        })
    }

    private fun setupBottomSheet() {
        bottomSheetView = BottomSheetManageLayoutBinding.inflate(layoutInflater, null, false)
        bottomSheetView.viewModel = manageViewModel
        bottomSheetView.lifecycleOwner = this

        bottomDialog = BottomSheetDialog(requireContext()).apply {
            setContentView(bottomSheetView.root)
        }

        bottomSheetView.imageClose.setOnClickListener{
            bottomDialog.dismiss()
        }
    }

    private fun initTabCategory(uid: String) {

        categoryViewModel.categoryList.observe(viewLifecycleOwner, { categories ->

            binding.tabCategory.removeAllTabs()

            categories.forEach {

                val tabBinding: CustomCategoryTabViewBinding =
                        DataBindingUtil.inflate(layoutInflater, R.layout.custom_category_tab_view, binding.tabCategory, false)

                tabBinding.category = it

                binding.tabCategory.addTab(
                        binding.tabCategory.newTab().setTag(it.categoryCode)
                                .setText(it.categoryName)
                                .setCustomView(tabBinding.root)
                )
            }

            val initTab = binding.tabCategory.getTabAt(0)?.tag as String?

            initTab?.let {
                productViewModel.getProductsFromFb(uid, it)
            }
        })

        binding.tabCategory.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val categoryCode = tab?.tag.toString()
                productViewModel.getProductsFromFb(branchId!!, categoryCode)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

    }

    private fun showMenu(v: View, @MenuRes menuRes: Int, item: ProductUI, position: Int) {
        val popup = android.widget.PopupMenu(requireContext(), v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when(menuItem.itemId){
                R.id.edit->{

                }
                R.id.delete ->{
                    productViewModel.deleteProductInFb(item)
                    productListAdapter.notifyItemChanged(position)
                }
            }
            false
        }
        popup.show()
    }
}