package com.paiwad.myapp.paiwadpos.view.di.hilt

import com.paiwad.myapp.paiwadpos.data.paging.PixabayPagingRepository
import com.paiwad.myapp.paiwadpos.data.repository.PixabayRepository
import com.paiwad.myapp.paiwadpos.viewmodel.PixabayViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class ViewmodelModule {

    @Provides
    fun providePixabayViewModel(
            pixabayPagingRepository: PixabayPagingRepository,
            pixabayRepository: PixabayRepository
    ): PixabayViewModel {
        return PixabayViewModel(pixabayPagingRepository,pixabayRepository)
    }
}