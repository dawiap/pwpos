package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity

@Dao
interface UserDao {
    @Query("SELECT *FROM user")
    suspend fun getUser(): UserEntity?

    @Query("SELECT COUNT(*) FROM user WHERE PIN IN(:pin)")
    suspend fun checkYourPIN(pin: String): Int

    @Insert
    suspend fun insertUser(userEntity: UserEntity): Long

    @Query("UPDATE user SET photoUrl = (:uri)")
    suspend fun updateProfileImage(uri: String): Int

    @Update
    suspend fun updateProfile(userEntity: UserEntity): Int

    @Query("DELETE FROM user")
    suspend fun clearUser()
}