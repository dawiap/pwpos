package com.paiwad.myapp.paiwadpos.view.ui.setting

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityChangePinBinding
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.viewmodel.*
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ChangePinActivity : AppCompatActivity(), View.OnClickListener {

    private val pinViewModel: PINViewModel by viewModels()

    private val settingViewModel: SettingViewModel by viewModels()

    private val userViewModel: UserViewModel by viewModels()

    @Inject lateinit var progressBuilder: ProgressBuilder

    private lateinit var binding: ActivityChangePinBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_pin)
        binding.viewModel = pinViewModel
        binding.lifecycleOwner = this

        userViewModel.getUserProfile()
        userViewModel.userProfile.observe(this,{user ->
            user?.let { settingViewModel.setUserUI(it) }
        })

        settingViewModel.viewState.observe(this, {
            when (it) {
                is ViewState.Loaded -> {
                    progressBuilder.dismissProgressDialog()
                    finish()
                }
                is ViewState.LoadFailure -> {
                    progressBuilder.dismissProgressDialog()
                    Toast.makeText(this, it.errorMessage, Toast.LENGTH_LONG).show()
                }
                is ViewState.Loading -> {
                    progressBuilder.showProgressDialog()
                }
            }
        })

        binding.pinChange.setOnClickListener(this)
        binding.imageClose.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.pinChange -> {
                pinViewModel.yourPIN.observe(this, {
                    settingViewModel.updateProfile(
                        EditorType.PIN,
                        it
                    )
                    //pinViewModel.checkYourPIN(it)
                })
                /*pinViewModel.isCorrect.observe(this,SingleEventObserver{
                    println(it)
                    if(it){
                        pinViewModel.clearPIN()
                    }
                })*/
            }

            binding.imageClose -> {
                finish()
            }
        }
    }
}