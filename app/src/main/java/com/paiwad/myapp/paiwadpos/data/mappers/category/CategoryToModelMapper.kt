package com.paiwad.myapp.paiwadpos.data.mappers.category

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.Mapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.SingleMapper
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI

class CategoryToModelMapper: SingleMapper<CategoryUI, CategoryModel> {
    override fun toData(currentLayerEntity: CategoryUI) = CategoryModel(
            categoryId = currentLayerEntity.categoryCode,
            categoryName = currentLayerEntity.categoryName,
            categoryColor = currentLayerEntity.categoryColor,
            branchId = currentLayerEntity.branchId
    )

}