package com.paiwad.myapp.paiwadpos.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.paiwad.myapp.paiwadpos.data.api.PixabayApiService
import com.paiwad.myapp.paiwadpos.data.api.model.Hit

private const val GITHUB_STARTING_PAGE_INDEX = 1

class PixabayPagingSource(
    private val service: PixabayApiService,
    private val query: String
): PagingSource<Int, Hit>() {
    override fun getRefreshKey(state: PagingState<Int, Hit>): Int = 1

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Hit> {
        val pageNumber = params.key ?: 1
        return try {
            //val response = service.searchImageFromPixabay(query,pageNumber)
            val response = service.searchImageFromPixabay(query)
            val pagedResponse = response.body()
            val data = pagedResponse?.hits

            val nextPageNumber = if (data!!.isEmpty()) {
                null
            } else {
                pageNumber + 1
            }

            LoadResult.Page(
                data = data.orEmpty(),
                prevKey = if (pageNumber == GITHUB_STARTING_PAGE_INDEX) null else pageNumber - 1,
                nextKey = nextPageNumber
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}