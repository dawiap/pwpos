package com.paiwad.myapp.paiwadpos.data.api.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class OrderDetailModel(
    var orderDetailId: String? = null,
    var productCode: String? = null,
    var productName: String? = null,
    var imagePath: String? = null,
    var productPrice: String? = null,
    var productCount: Int? = null,
    var productTotalAmount: String? = null,
    var unitModel: UnitModel? = null,
    var optionModel: OptionModel? = null
)