package com.paiwad.myapp.paiwadpos.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavArgs
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.navArgs
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityOrderDetailBinding
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderDetailActivity : AppCompatActivity() {

    val args: OrderDetailActivityArgs by navArgs()

    val orderViewModel: OrderViewModel by viewModels()

    lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityOrderDetailBinding>(this,R.layout.activity_order_detail)
        supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        userViewModel.userProfile.observe(this,{
            println(it.toString())
        })
    }

    override fun onBackPressed() {
        println("do something")
    }
}