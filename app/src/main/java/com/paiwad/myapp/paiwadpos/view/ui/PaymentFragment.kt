package com.paiwad.myapp.paiwadpos.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.FragmentPaymentBinding
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.viewmodel.BadgeViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.PaymentViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PaymentFragment : Fragment(), UserAuthListener {

    private val userViewModel: UserViewModel by viewModels()

    private lateinit var paymentViewModel: PaymentViewModel

    private val orderViewModel: OrderViewModel by viewModels()

    private lateinit var badgeViewModel: BadgeViewModel

    @Inject lateinit var progressBuilder: ProgressBuilder

    @Inject lateinit var alertDialogBuilder: AlertDialogBuilder

    private val args: PaymentFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        paymentViewModel = ViewModelProvider(requireActivity()).get(PaymentViewModel::class.java)
        badgeViewModel = ViewModelProvider(requireActivity()).get(BadgeViewModel::class.java)

        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.fade)
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentPaymentBinding.bind(view)
        binding.lifecycleOwner = this

        /*(activity as BadgeActivity).apply {
            this@PaymentFragment.userViewModel = userViewModel
            this@PaymentFragment.badgeViewModel = badgeViewModel
            this@PaymentFragment.orderViewModel = orderViewModel
            this@PaymentFragment.paymentViewModel = paymentViewModel
            this@PaymentFragment.progressBuilder = progressBuilder
            this@PaymentFragment.alertDialogBuilder = alertDialogBuilder
        }*/

        try {
            paymentViewModel.setInitTotal(args.order.totalAmount)
            paymentViewModel.setIsAfterPayment()

            binding.orderUI = args.order

        }catch (e: Exception){
            badgeViewModel.badge.observe(viewLifecycleOwner,{badge ->
                paymentViewModel.setInitBadges(badge)
                val total = badge.sumByDouble {
                    if (it.product!!.isOption)
                        it.badge.badgeCount * it.badge.badgeOptionPrice!!
                    else
                        it.badge.badgeCount * it.product.productPrice
                }
                paymentViewModel.setInitTotal(total.toFloat())
            })

            badgeViewModel.totalAmount.observe(viewLifecycleOwner,{
                paymentViewModel.setInitTotal(it.toFloat())
            })
        }

        binding.viewModel = paymentViewModel
        binding.orderViewModel = orderViewModel

        orderViewModel.authListener = this

        orderViewModel.loading.observeLoading()
        orderViewModel.message.observeMessage()
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading(){
        this.observe(viewLifecycleOwner, SingleEventObserver{
            if (it) {
                if (progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun onSuccess(successType: SuccessType) {
        badgeViewModel.clearBadge()

        userViewModel.getUserProfile()
        userViewModel.userProfile.observe(viewLifecycleOwner,{user ->
            user?.let { orderViewModel.getOrders(it.uid) }
        })
        orderViewModel.orders.observe(viewLifecycleOwner, {
            val directions = PaymentFragmentDirections.actionPaymentFragmentToPreviewFragment(
                    order = it.last()
            )
            findNavController().navigate(directions)
        })
    }

    override fun onFailure(message: String) {
        TODO("Not yet implemented")
    }
}