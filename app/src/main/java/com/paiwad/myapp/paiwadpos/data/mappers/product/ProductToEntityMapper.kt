package com.paiwad.myapp.paiwadpos.data.mappers.product

import com.paiwad.myapp.paiwadpos.data.api.model.ProductModel
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.view.model.ProductUI

class ProductToEntityMapper: FromToMapper<ProductModel, ProductUI, ProductEntity> {
    override fun fromWayOne(data: ProductModel) = ProductEntity(
            productCode = data.productId!!,
            productName = data.productName!!,
            productPrice = data.productPrice!!.toDouble(),
            imagePath = data.imagePath!!,
            categoryCode = data.categoryCode!!,
            branchId = data.branchId!!,
            inStock = data.inStock!!,
            outStock = data.outStock!!,
            demandStock = data.demandStock!!,
            isOption = data.isOption,
            isStock = data.isStock,
            unitCode = data.unitModel?.unitId!!,
    )

    override fun fromWayTwo(data: ProductUI) = ProductEntity(
            productCode = data.productCode,
            productName = data.productName,
            productPrice = data.productPrice,
            imagePath = data.imagePath!!,
            categoryCode = data.categoryCode,
            branchId = data.branchId,
            inStock = data.inStock,
            outStock = data.outStock,
            demandStock = data.demandStock,
            isOption = data.isOption,
            isStock = data.isStock,
            unitCode = data.unitUI.unitCode,
    )

}