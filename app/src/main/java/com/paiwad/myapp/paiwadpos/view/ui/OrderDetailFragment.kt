package com.paiwad.myapp.paiwadpos.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.FragmentOrderDetailBinding
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.adapter.OrderDetailListAdapter
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OrderDetailFragment : Fragment(), UserAuthListener {

    private lateinit var orderUI: OrderUI

    private lateinit var orderViewModel: OrderViewModel

    private lateinit var userViewModel: UserViewModel

    @Inject lateinit var orderDetailListAdapter: OrderDetailListAdapter

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    private lateinit var binding: FragmentOrderDetailBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        orderViewModel = ViewModelProvider(requireActivity()).get(OrderViewModel::class.java)
        return inflater.inflate(R.layout.fragment_order_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentOrderDetailBinding.bind(view)

        orderUI = try {
            (arguments?.getSerializable("orderUI") as? OrderUI)!!
        }catch (e: Exception){
            (activity as OrderDetailActivity).args.order
        }

        orderViewModel.authListener = this

        binding.orderUI = orderUI
        binding.viewModel = orderViewModel
        binding.lifecycleOwner = this

        userViewModel = (activity as OrderDetailActivity).userViewModel

        binding.rcvOrderDetail.adapter = orderDetailListAdapter
        binding.imageClose.setOnClickListener { requireActivity().finish() }

        orderViewModel.loading.observeLoading()
        orderViewModel.message.observeMessage()
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading(){
        this.observe(viewLifecycleOwner, SingleEventObserver{
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun onSuccess(successType: SuccessType) {
        if (successType == SuccessType.CANCEL_ORDER_SUCCESS) {
            orderViewModel.singleOrder.observe(viewLifecycleOwner, {
                binding.orderUI = it
            })
        }
    }

    override fun onFailure(message: String) {
        TODO("Not yet implemented")
    }
}