package com.paiwad.myapp.paiwadpos.data.repository

import android.net.Uri
import com.paiwad.myapp.paiwadpos.view.model.UserUI
import io.reactivex.Completable
import io.reactivex.Observable

interface ProfileSettingRepository {

    fun uploadChangeImageUser(uri: Uri): Completable

    fun updateUserProfile(userUI: UserUI): Completable
}