package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.databinding.ShrProductBadgeListBinding
import com.paiwad.myapp.paiwadpos.viewmodel.BadgeViewModel

class BadgeProductListAdapter: RecyclerView.Adapter<BadgeProductListAdapter.BadgeProductViewHolder>() {

    lateinit var badgeViewModel: BadgeViewModel

    inner class BadgeProductViewHolder(val binding: ShrProductBadgeListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: BadgeWithProduct) {
            binding.badge = item
            binding.viewModel = badgeViewModel
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<BadgeWithProduct>() {
        override fun areItemsTheSame(oldItem: BadgeWithProduct, newItem: BadgeWithProduct): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: BadgeWithProduct, newItem: BadgeWithProduct): Boolean {
            return oldItem.badge.badgeId == newItem.badge.badgeId
        }

    }

    val differ = AsyncListDiffer(this, callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BadgeProductViewHolder {
        return BadgeProductViewHolder(
                ShrProductBadgeListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: BadgeProductViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size
}