package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity

@Dao
interface OptionDao {

    @Query("DELETE FROM option")
    suspend fun clearAllOption()

    @Insert
    suspend fun insertOptionList(options: List<OptionEntity>): List<Long>

    @Query("SELECT *FROM option WHERE productCode IN (:productCode)")
    suspend fun getOptions(productCode: String): List<OptionEntity>

}