package com.paiwad.myapp.paiwadpos.data.mappers.base

interface SingleMapper<From, To> {
    fun toData(currentLayerEntity: From): To
}