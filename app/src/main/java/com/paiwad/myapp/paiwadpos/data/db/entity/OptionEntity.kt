package com.paiwad.myapp.paiwadpos.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "option")
data class OptionEntity(
        @PrimaryKey(autoGenerate = true)
        val optionId: Long = 0,
        val optionCode: String? = null,
        val productCode: String ,
        val optionName: String,
        val optionPrice: Double
)
