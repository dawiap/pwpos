package com.paiwad.myapp.paiwadpos.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "productUnit")
data class UnitEntity (
    @PrimaryKey(autoGenerate = true)
    val unitId: Long = 0,
    val unitCode: String? = null,
    val branchId: String,
    val unitName: String)
