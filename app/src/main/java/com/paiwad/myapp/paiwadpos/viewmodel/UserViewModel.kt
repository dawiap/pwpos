package com.paiwad.myapp.paiwadpos.viewmodel

import android.content.Intent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseUser
import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity
import com.paiwad.myapp.paiwadpos.data.mappers.user.UserUIEntityNullMapper
import com.paiwad.myapp.paiwadpos.view.ui.user.RegisterActivity
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.data.repository.UserRepository
import com.paiwad.myapp.paiwadpos.view.model.UserUI
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.ui.user.CreatePinFragmentDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val repository: UserRepository
): ViewModel() {

    var email: String? = null
    var password: String? = null

    var emailForSend: String? = null

    var authListener: UserAuthListener? = null

    private val _firebaseUser = MutableLiveData<FirebaseUser?>()

    val firebaseUser: LiveData<FirebaseUser?> = _firebaseUser

    private val _userProfile = MutableLiveData<UserUI?>(null)

    val userProfile: LiveData<UserUI?> = _userProfile

    private val disposables = CompositeDisposable()

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()

    val loading: LiveData<SingleEvent<Boolean>> = _loading

    private val _userEntity = MutableLiveData<UserEntity>()
    val userEntity: LiveData<UserEntity> = _userEntity

    fun getUserProfile() {
        _firebaseUser.value = repository.currentUser()
        _firebaseUser.value?.let { firebaseUser ->
            _userProfile.value = null

            _loading.value = SingleEvent(true)
            viewModelScope.launch {
                repository.getDataUser(firebaseUser.uid).collect { result ->
                    _loading.value = SingleEvent(false)
                    when (result) {
                        is Result.Success -> {
                            result.data?.let {
                                authListener?.onSuccess(SuccessType.LOGIN_SUCCESS)
                                _userProfile.value = UserUIEntityNullMapper().upstream(it)
                            } ?: run {
                                authListener?.onSuccess(SuccessType.ON_CREATE_USER)
                            }
                        }
                        is Result.Error -> {
                            authListener?.onFailure(result.exception.toString())
                        }
                    }
                }
            }

        } ?: run {
            _userProfile.value = null
        }
    }

    fun setUserEntity(user: UserEntity){
        _userEntity.value = user
    }

    fun setYourPIN(PIN: String){
        _userEntity.value!!.PIN = PIN
    }

    fun createUsers(v: View) {
        _loading.value = SingleEvent(true)
        val disposable = repository.createUserToDb(_userEntity.value!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)
                    v.findNavController().navigate(CreatePinFragmentDirections.actionCreatePINFragmentToMainActivity())
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun loginWithGoogle(credential: AuthCredential) {
        _loading.value = SingleEvent(true)
        val disposable = repository.loginWithGoogle(credential)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getUserProfile()
                }, {
                    authListener?.onFailure(it.message!!)
                    getUserProfile()
                })

        disposables.add(disposable)
    }

    fun loginWithFacebook(credential: AuthCredential) {
        _loading.value = SingleEvent(true)
        val disposable = repository.loginWithFacebook(credential)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    getUserProfile()
                }, {
                    authListener?.onFailure(it.message!!)
                    getUserProfile()
                })

        disposables.add(disposable)
    }

    fun loginWithEmailAndPassword() {
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            authListener?.onFailure("Invalid email or password")
            return
        }

        _loading.value = SingleEvent(true)
        val disposable = repository.loginWithEmailAndPassword(email!!, password!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)
                    authListener?.onSuccess(SuccessType.LOGIN_SUCCESS)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })
        disposables.add(disposable)
    }

    fun signUp() {
        _loading.value = SingleEvent(true)
        val disposable = repository.registerWithEmailAndPassword(email!!, password!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)
                    authListener?.onSuccess(SuccessType.SIGN_UP_SUCCESS)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun resetPassword() {
        if (emailForSend.isNullOrEmpty()) {
            authListener?.onFailure("Please enter your email")
            return
        }
        _loading.value = SingleEvent(true)
        val disposable = repository.resetPasswordWithEmail(emailForSend!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    _loading.value = SingleEvent(false)
                    authListener?.onSuccess(SuccessType.SEND_TO_RMAIL_SUCCESS)
                }, {
                    _loading.value = SingleEvent(false)
                    authListener?.onFailure(it.message!!)
                })

        disposables.add(disposable)
    }

    fun userLogout(user: UserUI?) = viewModelScope.launch{
        _loading.value = SingleEvent(true)
        delay(2000)
        repository.logout()
        user?.let { user ->
            val disposable = repository.updateUserLogout(toUserEntity(user))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        getUserProfile()
                    }, {
                        authListener?.onFailure(it.message!!)
                    })
            disposables.add(disposable)
        }
        _loading.value = SingleEvent(false)
    }

    private fun toUserEntity(user: UserUI) = UserUIEntityNullMapper().downstream(user)

    fun goToRegister(view: View) {
        view.context.startActivity(Intent(view.context, RegisterActivity::class.java))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
