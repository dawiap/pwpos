package com.paiwad.myapp.paiwadpos.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.data.paging.PixabayPagingRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class PixabayViewModel @Inject constructor(
        private val pagingRepository: PixabayPagingRepository,
        private val pixabayRepository: com.paiwad.myapp.paiwadpos.data.repository.PixabayRepository
) : ViewModel() {

    val currentQueryValue = MutableLiveData<String>()

    private var currentSearchResult: Flow<PagingData<Hit>>? = null

    fun searchPixabay(): Flow<PagingData<Hit>> {
        val newResult: Flow<PagingData<Hit>> = pagingRepository.getSearchResultStream(
            currentQueryValue.value.toString()
        )
            .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }

    private val _images = MutableLiveData<List<Hit>>()
    val image: LiveData<List<Hit>> = _images

    fun searchImage() = liveData {
        val response = pixabayRepository.searchImage(currentQueryValue.value.toString())
        emit(response)
    }
}