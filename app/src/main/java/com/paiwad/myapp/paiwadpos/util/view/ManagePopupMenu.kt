package com.paiwad.myapp.paiwadpos.util.view

import android.content.Context
import android.view.MenuItem
import android.view.View
import androidx.annotation.MenuRes
import com.paiwad.myapp.paiwadpos.R

class ManagePopupMenu<T>(private val context: Context) {

    interface PopupClickListener{
        fun editClicked(item: Any?)
        fun deleteClicked(item: Any?, position: Int)
    }

    private var clickListener: PopupClickListener? = null

    fun setListener(listener: PopupClickListener){
        clickListener = listener
    }

    fun<T> showPopupMenu(v: View, @MenuRes menuRes: Int, item: T, position: Int) {
        val popup = android.widget.PopupMenu(context, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when(menuItem.itemId){
                R.id.edit->{
                    clickListener?.editClicked(item)
                }
                R.id.delete ->{
                    clickListener?.deleteClicked(item, position)
                }
            }
            false
        }
        popup.show()
    }
}


