package com.paiwad.myapp.paiwadpos.view.ui.manage

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.databinding.ActivityAddProductBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomSelectImageLayoutBinding
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.CategoryViewModel
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.InternetCheck
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.adapter.CategoryDropDownAdapter
import com.paiwad.myapp.paiwadpos.view.adapter.UnitDropDownAdapter
import com.paiwad.myapp.paiwadpos.view.model.OptionUI
import com.paiwad.myapp.paiwadpos.viewmodel.ProductViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UnitViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.util.ArrayList
import javax.inject.Inject

@AndroidEntryPoint
class AddProductActivity : AppCompatActivity(), UserAuthListener, View.OnClickListener {

    private val productViewModel: ProductViewModel by viewModels()

    private val categoryViewModel: CategoryViewModel by viewModels()

    private val unitViewModel: UnitViewModel by viewModels()

    private val userViewModel: UserViewModel by viewModels()

    @Inject
    lateinit var categoryDropDownAdapter: CategoryDropDownAdapter

    @Inject
    lateinit var unitDropDownAdapter: UnitDropDownAdapter

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    @Inject
    lateinit var alertDialogBuilder: AlertDialogBuilder

    @Inject lateinit var internetCheck: InternetCheck

    private lateinit var binding: ActivityAddProductBinding

    private lateinit var bottomSelectImageLayoutBinding: BottomSelectImageLayoutBinding

    private lateinit var bottomDialog: BottomSheetDialog

    companion object {
        private const val REQUEST_CODE = 11
        private const val REQUEST_STORAGE_CODE = 12
        private const val REQUEST_CAPTURE_CODE = 13
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_product)
        binding.lifecycleOwner = this
        binding.viewModel = productViewModel

        userViewModel.getUserProfile()
        userViewModel.userProfile.observe(this,{user ->
            user?.let {
                productViewModel.branchId = it.uid

                categoryViewModel.getCategoriesFromFb(it.uid, internetCheck.isNetworkAvailable())

                unitViewModel.getUnitsFromFb(it.uid)
            }
        })

        productViewModel.authListener = this

        setupBottomSheet()

        initDropdownCategoryAndUnit()

        binding.textButton.setOnClickListener(this)

        binding.imageView7.setOnClickListener(this)

        binding.buttonAddOther.setOnClickListener(this)

        binding.buttonSave.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        productViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    private fun setupBottomSheet() {
        bottomSelectImageLayoutBinding = BottomSelectImageLayoutBinding.inflate(layoutInflater, null, false)
        bottomDialog = BottomSheetDialog(this).apply {
            setContentView(bottomSelectImageLayoutBinding.root)
        }

        bottomSelectImageLayoutBinding.buttonDirectory.setOnClickListener(this)
        bottomSelectImageLayoutBinding.buttonCapture.setOnClickListener(this)
        bottomSelectImageLayoutBinding.buttonPixabay.setOnClickListener(this)
        bottomSelectImageLayoutBinding.imageClose.setOnClickListener(this)
    }

    private fun initDropdownCategoryAndUnit() {
        categoryViewModel.categoryList.observe(this, {
            it?.let { category ->
                categoryDropDownAdapter.setItems(category)
                binding.autoCompleteCategory.setAdapter(categoryDropDownAdapter)

                binding.autoCompleteCategory.setOnItemClickListener { parent, _, position, _ ->

                    val selectedCategory = parent.adapter.getItem(position) as CategoryUI

                    productViewModel.categoryCode = selectedCategory.categoryCode

                    binding.autoCompleteCategory.setText(selectedCategory.categoryName)
                }
            }
        })

        unitViewModel.unitList.observe(this, { units ->

            unitDropDownAdapter.setItems(units)

            binding.autoCompleteUnit.setAdapter(unitDropDownAdapter)

            binding.autoCompleteUnit.setOnItemClickListener { parent, _, position, _ ->

                val selectedUnit = parent.adapter.getItem(position) as UnitUI

                productViewModel.unitUiItem = selectedUnit

                binding.autoCompleteUnit.setText(selectedUnit.unitName)

            }
        })
    }

    private fun onClearFocus() {
        binding.editProductName.clearFocus()
        binding.editProductPrice.clearFocus()
        binding.editProductStock.clearFocus()
    }

    private fun addNewOptionView(){
        val inflater = LayoutInflater.from(this).inflate(R.layout.row_add_option, null)
        binding.parentLinearLayout.addView(inflater, binding.parentLinearLayout.childCount)
    }

    private fun saveProduct(){

        if(productViewModel.isOther.value == true) {

            val options = ArrayList<OptionUI>()
            val count = binding.parentLinearLayout.childCount
            var v: View?

            for (i in 0 until count) {
                v = binding.parentLinearLayout.getChildAt(i)

                val optionName: TextInputEditText = v.findViewById(R.id.edit_product_option_name)
                val optionPrice: TextInputEditText = v.findViewById(R.id.edit_product_option_price)

                if (optionName.text.isNullOrEmpty() || optionPrice.text.isNullOrEmpty()) {
                    productViewModel.authListener?.onFailure("Please enter require field")
                    break
                } else {

                    val option = OptionUI(
                            optionName = optionName.text.toString(),
                            optionPrice = optionPrice.text.toString().toDouble()
                    )

                    options.add(option)
                }
            }

            productViewModel.optionItems.value = options
        }

        productViewModel.uploadImageToFbStorage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK)
            return

        when (requestCode) {
            REQUEST_CODE -> {
                data?.let {

                    val pixabayImage = it.getSerializableExtra("pixabay") as Hit?

                    productViewModel.uploadImageUri = pixabayImage?.previewURL

                    pixabayImage.let { hit ->
                        Glide.with(this@AddProductActivity)
                                .load(hit?.previewURL)
                                .into(binding.imageProduct)
                    }
                }
            }

            REQUEST_STORAGE_CODE -> {
                val uri = data?.data
                Glide.with(this@AddProductActivity)
                        .load(uri)
                        .into(binding.imageProduct)

                productViewModel.imageUri = uri
            }

            REQUEST_CAPTURE_CODE -> {
                val photoFile = File(photoPath)
                val uri = Uri.fromFile(photoFile)
                Glide.with(this@AddProductActivity)
                        .load(uri)
                        .into(binding.imageProduct)

                productViewModel.imageUri = uri
            }

        }
    }

    var photoPath: String = ""

    private fun takePhoto() {
        val extDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        val photoFile = File.createTempFile("IMG${System.currentTimeMillis()}", ".jpg", extDir)

        photoPath = photoFile.absolutePath

        val uri = FileProvider.getUriForFile(this, "com.paiwad.myapp.paiwadpos", photoFile)

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_CAPTURE_CODE)
    }

    override fun onSuccess(successType: SuccessType) {
        if (successType == SuccessType.DATA_SUCCESS) {
            binding.imageProduct.setImageResource(0)
            binding.autoCompleteCategory.setText("")
            binding.autoCompleteUnit.setText("")
            binding.parentLinearLayout.removeAllViewsInLayout()
            onClearFocus()
        }
    }

    override fun onFailure(message: String) {
        alertDialogBuilder.showMessageAlertDialog("Failure", message)
    }

    override fun onClick(v: View?) {
        when (v) {
            bottomSelectImageLayoutBinding.buttonDirectory -> {
                bottomDialog.dismiss()

                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.type = "image/*"
                startActivityForResult(intent, REQUEST_STORAGE_CODE)
            }

            bottomSelectImageLayoutBinding.buttonCapture -> {
                bottomDialog.dismiss()
                takePhoto()
            }

            bottomSelectImageLayoutBinding.buttonPixabay -> {
                bottomDialog.dismiss()

                val intent = Intent(this, PixabayImageSearchActivity::class.java)
                startActivityForResult(intent, REQUEST_CODE)
            }

            binding.textButton -> {
                bottomDialog.show()
            }

            bottomSelectImageLayoutBinding.imageClose -> {
                bottomDialog.dismiss()
            }

            binding.imageView7 -> {
                finish()
            }

            binding.buttonAddOther -> {
                addNewOptionView()
            }

            binding.buttonSave -> {
                saveProduct()
            }
        }
    }
}