package com.paiwad.myapp.paiwadpos.data.db.entity

import android.os.Parcelable
import androidx.room.*
import kotlinx.parcelize.Parcelize

@Entity(tableName = "product")
@Parcelize
data class ProductEntity (
        @PrimaryKey(autoGenerate = true)
        val productId: Long = 0,
        val productCode: String? = null,
        val categoryCode: String,
        val unitCode: String,
        val productName: String,
        val imagePath: String?,
        val productPrice: Double,
        val inStock: Int,
        val outStock: Int,
        val demandStock: Int,
        val branchId: String,
        var isStock: Boolean = false,
        var isOption: Boolean = false
): Parcelable
