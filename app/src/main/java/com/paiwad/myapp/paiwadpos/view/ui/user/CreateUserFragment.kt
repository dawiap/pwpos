package com.paiwad.myapp.paiwadpos.view.ui.user

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseUser
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity
import com.paiwad.myapp.paiwadpos.databinding.FragmentCreateUserBinding
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.ui.MainActivity
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CreateUserFragment : Fragment(), TextWatcher, View.OnClickListener {

    private lateinit var binding: FragmentCreateUserBinding

    private lateinit var userViewModel: UserViewModel

    private var currentUser: FirebaseUser? = null
    private var photoUrl: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        userViewModel = (activity as CreateUserActivity).userViewModel
        return inflater.inflate(R.layout.fragment_create_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCreateUserBinding.bind(view)
        binding.lifecycleOwner = this

        userViewModel.getUserProfile()
        userViewModel.firebaseUser.observe(viewLifecycleOwner, {
            it?.let { user ->

                currentUser = user

                photoUrl = currentUser?.photoUrl

                user.email?.let { email ->
                    binding.editEmail.setText(email)
                    if (email.isNotEmpty())
                        binding.editEmail.isEnabled = false
                }
                user.displayName?.let { name ->
                    binding.editBranchName.setText(name)
                }
                user.phoneNumber?.let { phone ->
                    binding.editPhone.setText(phone)
                }
            }
        })

        binding.editBranchName.addTextChangedListener(this)
        binding.editPhone.addTextChangedListener(this)

        binding.imageClose.setOnClickListener(this)
        binding.bottomAction.buttonAction.setOnClickListener(this)

        binding.bottomAction.buttonAction.text = getString(R.string.button_text_next)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        binding.bottomAction.buttonAction.isEnabled =
            binding.editBranchName.text.toString().isNotEmpty() && binding.editPhone.text.toString()
                .isNotEmpty()
    }

    override fun afterTextChanged(s: Editable?) {}

    override fun onClick(v: View?) {
        when (v) {
            binding.imageClose -> {
                userViewModel.userLogout(null)
                startActivity(Intent(requireActivity(), MainActivity::class.java))
            }
            binding.bottomAction.buttonAction -> {
                userViewModel.setUserEntity(toUserEntity())
                findNavController().navigate(CreateUserFragmentDirections.actionCreateUserFragmentToCreatePINFragment())
            }
        }
    }

    private fun toUserEntity() = UserEntity(
        uid = currentUser?.uid!!,
        photoUrl = currentUser?.photoUrl.toString(),
        email = binding.editEmail.text.toString(),
        branchName = binding.editBranchName.text.toString(),
        phone = binding.editPhone.text.toString(),
        signOn = 1,
    )
}