package com.paiwad.myapp.paiwadpos.data.api

import android.net.Uri
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import io.reactivex.Observable

class FirebaseStorageReference {

    private val storageRef = Firebase.storage.reference

    fun upLoadImageToFirebaseStorage(file: Uri) = Observable.create<Uri> { emitter ->

        val riversRef = storageRef.child("images/product/${file.lastPathSegment}")

        val uploadTask = riversRef.putFile(file)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            riversRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {

                val downloadUri = task.result

                if (downloadUri != null)
                    emitter.onNext(downloadUri)

            } else {
                emitter.onError(task.exception!!)
            }
        }

    }

    fun upLoadImageUserToFirebaseStorage(file: Uri) = Observable.create<Uri> { emitter ->

        val riversRef = storageRef.child("images/user/${file.lastPathSegment}")

        val uploadTask = riversRef.putFile(file)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            riversRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {

                val downloadUri = task.result

                if (downloadUri != null)
                    emitter.onNext(downloadUri)

            } else {
                emitter.onError(task.exception!!)
            }
        }

    }
}