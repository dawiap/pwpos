package com.paiwad.myapp.paiwadpos.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.paiwad.myapp.paiwadpos.data.repository.ProductRepository
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val repository: ProductRepository
): ViewModel() {

    private val _products = MutableLiveData<List<ProductUI>>()
    val products: LiveData<List<ProductUI>> = _products

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()
    val loading: LiveData<SingleEvent<Boolean>> = _loading

    fun searchProducts(q: String) = viewModelScope.launch {
        _loading.postValue(SingleEvent(true))
        when (val products = repository.searchProduct(q)) {
            is Result.Success -> {
                _loading.postValue(SingleEvent(false))
                products.data?.let {
                    _products.postValue(it)
                }
            }
            is Result.Error -> {
                _loading.postValue(SingleEvent(false))
                _message.postValue(SingleEvent(products.exception.message.toString()))
            }
        }
    }

    fun getProducts() = viewModelScope.launch {
        _loading.postValue(SingleEvent(true))
        when (val products = repository.getProducts()) {
            is Result.Success -> {
                _loading.postValue(SingleEvent(false))
                products.data?.let {
                    _products.postValue(it)
                }
            }
            is Result.Error -> {
                _loading.postValue(SingleEvent(false))
                _message.postValue(SingleEvent(products.exception.message.toString()))
            }
        }
    }
}