package com.paiwad.myapp.paiwadpos.view.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.databinding.adapters.TextViewBindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivitySearchProductBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomSheetAddToCartBinding
import com.paiwad.myapp.paiwadpos.databinding.LayoutChipOptionBinding
import com.paiwad.myapp.paiwadpos.util.view.ProductGridItemDecoration
import com.paiwad.myapp.paiwadpos.view.adapter.ProductCardAdapter
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import com.paiwad.myapp.paiwadpos.viewmodel.BadgeViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.MenuViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchProductActivity : AppCompatActivity() {

    private val searchViewModel: SearchViewModel by viewModels()

    private val menuViewModel: MenuViewModel by viewModels()

    private val badgeViewModel: BadgeViewModel by viewModels()

    @Inject
    lateinit var productCardAdapter: ProductCardAdapter

    private lateinit var binding: ActivitySearchProductBinding

    private lateinit var bottomSheetAddToCartBinding: BottomSheetAddToCartBinding

    private lateinit var bottomDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_search_product)
        binding.lifecycleOwner = this

        setupBottomAddToCart()

        searchViewModel.getProducts()

        binding.editSearch.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    searchViewModel.searchProducts("%${it.toString()}%")
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        getProducts()
    }

    private fun getProducts() {
        searchViewModel.products.observe(this, { products ->
            val layoutManager = GridLayoutManager(this, 2)
            binding.rcvProducts.layoutManager = layoutManager

            productCardAdapter.setItems(products)
            binding.rcvProducts.adapter = productCardAdapter

            val largePadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing)
            val smallPadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small)
            binding.rcvProducts.removeItemDecorations()
            binding.rcvProducts.addItemDecoration(ProductGridItemDecoration(largePadding, smallPadding))

            productCardAdapter.clickListener = {
                productClicked(it)
            }
        })
    }

    private fun RecyclerView.removeItemDecorations() {
        while (this.itemDecorationCount > 0) {
            this.removeItemDecorationAt(0)
        }
    }

    private fun setupBottomAddToCart() {
        bottomSheetAddToCartBinding = BottomSheetAddToCartBinding.inflate(layoutInflater, null, false)
        bottomSheetAddToCartBinding.viewModel = menuViewModel
        bottomSheetAddToCartBinding.badgeViewModel = badgeViewModel
        bottomSheetAddToCartBinding.lifecycleOwner = this
        bottomDialog = BottomSheetDialog(this).apply {
            setContentView(bottomSheetAddToCartBinding.root)
        }
        bottomSheetAddToCartBinding.imageClose.setOnClickListener { bottomDialog.dismiss() }
    }

    private fun productClicked(item: ProductUI) {
        menuViewModel._productCount.value = 1
        menuViewModel.setInitPrice(item.productPrice)
        menuViewModel._totalProductPrice.value = item.productPrice

        bottomSheetAddToCartBinding.product = item

        bottomSheetAddToCartBinding.parentChipGroup.removeAllViewsInLayout()
        item.optionUI?.let {
            for (i in it.indices) {
                val chip: LayoutChipOptionBinding = LayoutChipOptionBinding.inflate(
                    layoutInflater,
                    bottomSheetAddToCartBinding.parentChipGroup,
                    false
                )
                chip.option = it[i]
                chip.chip1.id = i + 1
                bottomSheetAddToCartBinding.parentChipGroup.addView(chip.root)
            }

            bottomSheetAddToCartBinding.parentChipGroup.invalidate()
        }

        val count = bottomSheetAddToCartBinding.parentChipGroup.childCount
        var v: View?
        for (i in 0 until count) {
            v = bottomSheetAddToCartBinding.parentChipGroup.getChildAt(i)
            val chip = DataBindingUtil.getBinding<LayoutChipOptionBinding>(v)
            chip?.chip1?.let {
                it.isCheckable = true
                it.tag = i
                it.setOnCheckedChangeListener { view, isChecked ->
                    for (j in 0 until count) {
                        if (view.tag != j) {
                            v = bottomSheetAddToCartBinding.parentChipGroup.getChildAt(j)
                            val chp = DataBindingUtil.getBinding<LayoutChipOptionBinding>(v!!)
                            chp!!.chip1.isChecked = false
                        }
                        view.isChecked = isChecked
                    }
                    println(chip.option)
                    menuViewModel.setInitPrice(chip.option!!)
                    menuViewModel.calcPrice()
                }
            }
        }
        bottomDialog.show()
    }
}