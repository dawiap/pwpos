package com.paiwad.myapp.paiwadpos.data.mappers.unit

import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.Mapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.SingleMapper
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

class UnitToModelMapper: SingleMapper<UnitUI, UnitModel> {
    override fun toData(currentLayerEntity: UnitUI) = UnitModel(
            unitId = currentLayerEntity.unitCode,
            branchId = currentLayerEntity.branchId,
            unitName = currentLayerEntity.unitName,
    )
}