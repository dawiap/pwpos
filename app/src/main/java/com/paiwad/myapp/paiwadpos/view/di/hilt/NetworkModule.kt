package com.paiwad.myapp.paiwadpos.view.di.hilt

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.paiwad.myapp.paiwadpos.BuildConfig
import com.paiwad.myapp.paiwadpos.data.api.FirebaseAuthService
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.FirebaseStorageReference
import com.paiwad.myapp.paiwadpos.data.api.PixabayApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    private val connectTimeout: Long = 40
    private val readTimeOut: Long = 40

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(connectTimeout, TimeUnit.SECONDS)
            .readTimeout(readTimeOut, TimeUnit.SECONDS)
        okHttpClientBuilder.build()
        return okHttpClientBuilder.build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(): PixabayApiService {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.BASE_URL)
            .client(provideHttpClient())
            .build()
        return retrofit.create(PixabayApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideFirebase(): DatabaseReference{
        return FirebaseDatabase.getInstance().reference
    }

    @Singleton
    @Provides
    fun provideFirebaseService(): FirebaseAuthService{
        return FirebaseAuthService()
    }

    @Singleton
    @Provides
    fun provideFirebaseDatabaseReference(): FirebaseDatabaseReference{
        return FirebaseDatabaseReference()
    }

    @Singleton
    @Provides
    fun provideFirebaseStorageReference(): FirebaseStorageReference{
        return FirebaseStorageReference()
    }
}