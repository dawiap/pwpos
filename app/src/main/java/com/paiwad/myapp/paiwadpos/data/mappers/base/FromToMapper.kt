package com.paiwad.myapp.paiwadpos.data.mappers.base

interface FromToMapper<T, U, V> {
    fun fromWayOne(data: T): V

    fun fromWayTwo(data: U): V
}