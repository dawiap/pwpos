package com.paiwad.myapp.paiwadpos.view.ui.user

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.FragmentCreatePinBinding
import com.paiwad.myapp.paiwadpos.databinding.FragmentPinBinding
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.ui.MainActivity
import com.paiwad.myapp.paiwadpos.viewmodel.PINViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreatePinFragment : Fragment(), UserAuthListener {

    private lateinit var binding: FragmentCreatePinBinding

    private val pinViewModel: PINViewModel by viewModels()

    private lateinit var progressBuilder: ProgressBuilder

    private lateinit var alertDialogBuilder: AlertDialogBuilder

    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        userViewModel = (activity as CreateUserActivity).userViewModel
        progressBuilder = (activity as CreateUserActivity).progressBuilder
        alertDialogBuilder = (activity as CreateUserActivity).alertDialogBuilder
        return inflater.inflate(R.layout.fragment_create_pin, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentCreatePinBinding.bind(view)
        binding.viewModel = pinViewModel
        binding.userViewModel = userViewModel
        binding.lifecycleOwner = this

        pinViewModel.yourPIN.observe(viewLifecycleOwner,{
            userViewModel.setYourPIN(it)
        })

        userViewModel.authListener = this
    }

    override fun onResume() {
        super.onResume()
        userViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun onSuccess(successType: SuccessType) {
        println("successType : $successType")
    }

    override fun onFailure(message: String) {
        alertDialogBuilder.showMessageAlertDialog("Failure", message)
    }

}