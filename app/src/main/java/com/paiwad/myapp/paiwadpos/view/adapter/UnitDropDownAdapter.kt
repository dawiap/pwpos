package com.paiwad.myapp.paiwadpos.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import com.paiwad.myapp.paiwadpos.databinding.ViewTextSpinnerItemBinding
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import java.util.*
import kotlin.collections.ArrayList


class UnitDropDownAdapter : BaseAdapter(), Filterable {

    private var units: List<UnitUI> = emptyList()
    internal var tempItems = ArrayList<UnitUI>()
    internal var suggestions = ArrayList<UnitUI>()

    /*init {
        tempItems.addAll(dataSource)
        suggestions.addAll(dataSource)
    }*/

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewTextSpinnerItemBinding.inflate(layoutInflater, parent, false)
        val vh = ItemHolder(binding)
        vh.bind(units[position])

        return binding.root
    }

    override fun getItem(position: Int): Any {
        return units[position]
    }

    override fun getCount(): Int {
        return units.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setItems(items: List<UnitUI>){
        units = items

        tempItems.addAll(items)
        suggestions.addAll(items)
    }

    private class ItemHolder(val binding: ViewTextSpinnerItemBinding) {
        fun bind(item: UnitUI){
            binding.unit = item
            binding.executePendingBindings()
        }
    }

    override fun getFilter(): Filter {
        return filter
    }

    private var filter: Filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            return if (constraint != null) {
                suggestions.clear()
                tempItems.forEach {
                    if (it.unitName.toLowerCase(Locale.ROOT).contains(constraint.toString().toLowerCase(
                            Locale.ROOT
                        )
                        )
                    ) {
                        suggestions.add(it)
                    }
                }

                val filterResults = FilterResults()
                filterResults.values = suggestions
                filterResults.count = suggestions.size
                filterResults
            } else {
                FilterResults()
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            val filterList = results.values as? List<*>
            notifyDataSetChanged()
        }
    }

}