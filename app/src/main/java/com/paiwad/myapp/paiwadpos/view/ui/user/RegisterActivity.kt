package com.paiwad.myapp.paiwadpos.view.ui.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityRegisterBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomResetPasswordLayoutBinding
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity(), TextWatcher, View.OnClickListener, UserAuthListener {
    @Inject
    lateinit var progressBuilder: ProgressBuilder

    private val userViewModel: UserViewModel by viewModels()

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var bottomResetPasswordView: BottomResetPasswordLayoutBinding
    private lateinit var bottomDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        binding.viewModel = userViewModel

        userViewModel.authListener = this

        binding.editEmail.addTextChangedListener(this)
        binding.editPassword.addTextChangedListener(this)
        binding.topAction.actionNext.setOnClickListener(this)

        binding.buttonSignUp.setOnClickListener(this)

        binding.topAction.actionBack.visibility = View.GONE
        binding.topAction.actionNext.setImageResource(R.drawable.ic_action_close)

        setupBottomSheetResetPassword()
    }

    override fun onResume() {
        super.onResume()
        userViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        binding.buttonSignUp.isEnabled =
                binding.editEmail.text.toString().isNotEmpty() && binding.editPassword.text.toString().isNotEmpty()
    }

    override fun afterTextChanged(s: Editable?) {}

    override fun onClick(v: View?) {
        when (v) {
            binding.topAction.actionNext -> {
                finish()
            }
            bottomResetPasswordView.imageClose ->{
                bottomDialog.dismiss()
            }
        }
    }

    private fun setupBottomSheetResetPassword() {
        bottomResetPasswordView = BottomResetPasswordLayoutBinding.inflate(layoutInflater, null, false)
        bottomResetPasswordView.viewModel = userViewModel
        bottomDialog = BottomSheetDialog(this).apply {
            setContentView(bottomResetPasswordView.root)
        }
        bottomResetPasswordView.imageClose.setOnClickListener(this)
    }

    override fun onSuccess(successType: SuccessType) {
        when(successType){
            SuccessType.SIGN_UP_SUCCESS  ->{
                startActivity(Intent(this@RegisterActivity, CreateUserActivity::class.java))
            }
            else ->{}
        }

    }

    override fun onFailure(message: String) {
        Toast.makeText(this@RegisterActivity,message,Toast.LENGTH_LONG).show()
    }
}