package com.paiwad.myapp.paiwadpos.view.model

import java.io.Serializable

data class OrderDetailUI(
    var orderDetailId: String? = null,
    val orderDetailCode: String,
    val productCode: String,
    val productName: String,
    val imagePath: String,
    val productPrice: Double,
    val productCount: Int,
    val productTotalAmount: String,
    val optionModel: OptionUI? = null
): Serializable