package com.paiwad.myapp.paiwadpos.data.api.model

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class OptionModel (
        var optionId: String? = null,
        var productCode: String? = null,
        var optionName: String? = null,
        var optionPrice: String? = null
        )