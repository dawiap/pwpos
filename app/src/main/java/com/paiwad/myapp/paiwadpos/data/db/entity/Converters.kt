package com.paiwad.myapp.paiwadpos.data.db.entity

import androidx.room.TypeConverter
import com.google.gson.Gson

class Converters {
    @TypeConverter
    fun stringToUnitEntity(string: String): UnitEntity {
        return Gson().fromJson(string, UnitEntity::class.java)
    }

    @TypeConverter
    fun unitEntityToString(unitEntity: UnitEntity?): String {
        return Gson().toJson(unitEntity)
    }

    @TypeConverter
    fun stringToOptionEntity(string: String): List<OptionEntity> {
        val objects = Gson().fromJson(string, Array<OptionEntity>::class.java)
        return objects.toList()
    }

    @TypeConverter
    fun optionEntityToString(optionEntity: List<OptionEntity>?): String {
        return Gson().toJson(optionEntity)
    }

    @TypeConverter
    fun stringToProductEntity(string: String): ProductEntity {
        return Gson().fromJson(string, ProductEntity::class.java)
    }

    @TypeConverter
    fun productEntityToString(productEntity: ProductEntity): String {
        return Gson().toJson(productEntity)
    }
}