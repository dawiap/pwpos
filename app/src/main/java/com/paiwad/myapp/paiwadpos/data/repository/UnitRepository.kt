package com.paiwad.myapp.paiwadpos.data.repository

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import io.reactivex.Completable
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow

interface UnitRepository {
    fun getUnitsFromFb(branchId: String): Flow<Result<List<UnitUI>>>

    fun insertUnitToFb(unitUI: UnitUI): Completable

    fun updateUnitToFb(unitUI: UnitUI): Completable

    fun deleteUnitToFb(unit: String): Completable

    fun checkProductBeforeDeleteUnitInFb(unitId: String): Observable<Boolean>

    suspend fun updateCache(branchId: String): Flow<Result<List<UnitModel>>>
}