package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.databinding.ViewPixabayItemBinding
import com.paiwad.myapp.paiwadpos.view.model.ProductUI


class PixabayListAdapter: RecyclerView.Adapter<PixabayListAdapter.PixabayItemViewHolder>() {

    lateinit var onItemClick: ((Hit) -> Unit)

    inner class PixabayItemViewHolder(private val binding: ViewPixabayItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Hit, clickListener: (Hit) -> Unit) = with(binding) {
            hit = item
            root.setOnClickListener {
                clickListener(item)
            }

        }
    }

    override fun onBindViewHolder(holder: PixabayItemViewHolder, position: Int) {
        differ.currentList[position]?.let {
            holder.bind(it, onItemClick)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PixabayItemViewHolder {
        return PixabayItemViewHolder(
            ViewPixabayItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount() = differ.currentList.size

    private val callBack = object : DiffUtil.ItemCallback<Hit>() {
        override fun areItemsTheSame(oldItem: Hit, newItem: Hit): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Hit, newItem: Hit): Boolean {
            return oldItem.id == newItem.id
        }

    }

    val differ = AsyncListDiffer(this, callBack)
}

