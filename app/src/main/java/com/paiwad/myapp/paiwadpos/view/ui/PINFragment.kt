package com.paiwad.myapp.paiwadpos.view.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.FragmentPinBinding
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import com.paiwad.myapp.paiwadpos.view.ui.user.CreateUserActivity
import com.paiwad.myapp.paiwadpos.viewmodel.OrderViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.PINViewModel
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.Executor
import javax.inject.Inject

@AndroidEntryPoint
class PINFragment : Fragment(), UserAuthListener {

    private lateinit var binding: FragmentPinBinding

    private val pinViewModel: PINViewModel by viewModels()

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    private lateinit var orderViewModel: OrderViewModel

    private val args: PINFragmentArgs by navArgs()

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        orderViewModel = ViewModelProvider(requireActivity()).get(OrderViewModel::class.java)
        return inflater.inflate(R.layout.fragment_pin, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPinBinding.bind(view)
        binding.viewModel = pinViewModel
        binding.lifecycleOwner = this

        setupBiometricAuth()
        biometricPrompt.authenticate(promptInfo)

        pinViewModel.yourPIN.observe(viewLifecycleOwner,{ yourPIN ->
            yourPIN?.let {
                if (yourPIN.length == 4)
                    pinViewModel.checkYourPIN(yourPIN)
            }
        })

        pinViewModel.isCorrect.observe(viewLifecycleOwner, SingleEventObserver{
            if(it)
                orderViewModel.onCancelOrder(args.order)
        })

        orderViewModel.authListener = this

        orderViewModel.loading.observeLoading()
        orderViewModel.message.observeMessage()
        pinViewModel.message.observeMessage()
    }

    private fun setupBiometricAuth(){
        executor = ContextCompat.getMainExecutor(requireContext())
        biometricPrompt = BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback(){
            override fun onAuthenticationError(errorCode: Int,
                                               errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Toast.makeText(requireContext(),
                        "Authentication error: $errString", Toast.LENGTH_SHORT)
                        .show()
            }

            override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                orderViewModel.onCancelOrder(args.order)
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Toast.makeText(requireContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show()
            }
        })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle(requireActivity().getString(R.string.prompt_info_title))
                .setSubtitle(requireActivity().getString(R.string.prompt_info_subtitle))
                .setDescription(requireActivity().getString(R.string.prompt_info_description))
                .setConfirmationRequired(false)
                .setNegativeButtonText(requireActivity().getString(R.string.prompt_info_use_app_password))
                .build()

        binding.pinC.setOnClickListener {
            biometricPrompt.authenticate(promptInfo)
        }
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading(){
        this.observe(viewLifecycleOwner, SingleEventObserver{
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    override fun onSuccess(successType: SuccessType) {
        if(successType == SuccessType.CANCEL_ORDER_SUCCESS)
            findNavController().navigate(PINFragmentDirections.actionNavPinToOrderDetailFragment(null))
    }

    override fun onFailure(message: String) {
        TODO("Not yet implemented")
    }
}