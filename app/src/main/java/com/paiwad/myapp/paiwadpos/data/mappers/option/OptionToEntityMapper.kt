package com.paiwad.myapp.paiwadpos.data.mappers.option

import com.paiwad.myapp.paiwadpos.data.api.model.OptionModel
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.view.model.OptionUI

class OptionToEntityMapper: FromToMapper<OptionModel, OptionUI, OptionEntity> {
    override fun fromWayOne(data: OptionModel) = OptionEntity(
            optionPrice = data.optionPrice!!.toDouble(),
            optionName = data.optionName!!,
            productCode = data.productCode!!,
            optionCode = data.optionId
    )

    override fun fromWayTwo(data: OptionUI) = OptionEntity(
            optionPrice = data.optionPrice,
            optionName = data.optionName,
            productCode = data.productCode!!,
            optionCode = data.optionCode,
    )
}