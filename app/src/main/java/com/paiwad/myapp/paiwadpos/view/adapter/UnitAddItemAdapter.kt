package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ViewAddUnitItemBinding
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

class UnitAddItemAdapter: RecyclerView.Adapter<UnitAddItemAdapter.UnitAddItemViewHolder>() {

    lateinit var clickListener: ((View, UnitUI, Int) -> Unit)

    inner class UnitAddItemViewHolder(val binding: ViewAddUnitItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UnitUI, clickListener: (View, UnitUI, Int) -> Unit) {
            binding.unit = item
            binding.imgRemove.setOnClickListener {
                clickListener(binding.root, item, adapterPosition)
            }
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<UnitUI>(){
        override fun areItemsTheSame(oldItem: UnitUI, newItem: UnitUI): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: UnitUI, newItem: UnitUI): Boolean {
            return oldItem.unitId == newItem.unitId
        }

    }

    val differ = AsyncListDiffer(this, callBack)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UnitAddItemViewHolder {
        return UnitAddItemViewHolder(
                ViewAddUnitItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: UnitAddItemViewHolder, position: Int) {
        holder.bind(differ.currentList[position], clickListener)
    }

    override fun getItemCount(): Int = differ.currentList.size
}