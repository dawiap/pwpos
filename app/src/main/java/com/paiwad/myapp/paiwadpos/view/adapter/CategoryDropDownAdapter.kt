package com.paiwad.myapp.paiwadpos.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import com.paiwad.myapp.paiwadpos.databinding.ViewCategorySpinnerItemBinding
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import java.util.*
import kotlin.collections.ArrayList


class CategoryDropDownAdapter : BaseAdapter(), Filterable {

    private var categories: List<CategoryUI> = emptyList()
    internal var tempItems = ArrayList<CategoryUI>()
    internal var suggestions = ArrayList<CategoryUI>()

    /*init {
        tempItems.addAll(dataSource)
        suggestions.addAll(dataSource)
    }*/

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewCategorySpinnerItemBinding.inflate(layoutInflater, parent, false)
        val vh = ItemHolder(binding)
        vh.bind(categories[position])

        return binding.root
    }

    override fun getItem(position: Int): Any {
        return categories[position]
    }

    override fun getCount(): Int {
        return categories.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setItems(items: List<CategoryUI>){
        categories = items

        tempItems.addAll(items)
        suggestions.addAll(items)
    }

    private class ItemHolder(val binding: ViewCategorySpinnerItemBinding) {
        fun bind(item: CategoryUI){
            binding.category = item
            binding.executePendingBindings()
        }
    }

    override fun getFilter(): Filter {
        return filter
    }

    private var filter: Filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            return if (constraint != null) {
                suggestions.clear()
                tempItems.forEach {
                    if (it.categoryName.toLowerCase(Locale.ROOT).contains(constraint.toString().toLowerCase(
                            Locale.ROOT
                        )
                        )
                    ) {
                        suggestions.add(it)
                    }
                }

                val filterResults = FilterResults()
                filterResults.values = suggestions
                filterResults.count = suggestions.size
                filterResults
            } else {
                FilterResults()
            }
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            val filterList = results.values as? List<*>
            notifyDataSetChanged()
        }
    }

}