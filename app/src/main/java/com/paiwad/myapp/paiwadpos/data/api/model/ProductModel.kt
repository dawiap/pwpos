package com.paiwad.myapp.paiwadpos.data.api.model

import com.google.firebase.database.IgnoreExtraProperties
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

@IgnoreExtraProperties
data class ProductModel (
    var productId: String? = null,
    var categoryCode: String? = null,
    var unitModel: UnitModel? = null,
    var productName: String? = null,
    var imagePath: String? = null,
    var productPrice: String? = null,
    var branchId: String? = null,
    var isStock: Boolean = false,
    var inStock: Int? = null,
    var outStock: Int? = null,
    var demandStock: Int? = null,
    var isOption: Boolean = false,
    var options: List<OptionModel>? = null
        )
