package com.paiwad.myapp.paiwadpos.data.repository

import android.net.Uri
import com.paiwad.myapp.paiwadpos.data.api.model.ProductModel
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
    suspend fun getProducts(branchId: String, categoryCode: String): Flow<Result<List<ProductUI>>>

    suspend fun addProduct(productModel: ProductModel): Flow<Boolean>

    suspend fun updateProductInFb(productModel: ProductEntity): Flow<Boolean>

    suspend fun deleteProduct(productUI: ProductUI): Flow<Boolean>

    fun uploadImageToFbStorage(uri: Uri): Observable<Uri>

    suspend fun updateCache(branchId: String, categoryCode: String): Flow<Result<List<ProductModel>>>

    suspend fun searchProduct(q: String): Result<List<ProductUI>>

    suspend fun getProducts(): Result<List<ProductUI>>
}