package com.paiwad.myapp.paiwadpos.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.paiwad.myapp.paiwadpos.data.api.model.OrderDetailModel

@Entity(tableName = "ops_order")
data class OrderEntity(
        @PrimaryKey(autoGenerate = true)
        val orderId: Long = 0,
        val orderCode: String,
        val orderDate: String,
        val branchId: String,
        val isPayment: Int,
        val isCancelOrder: Int,
        val totalAmount: Float,
        val cashReceived: Int,
        val cashChange: Float,
        val orderDetailModel: List<OrderDetailEntity>
)
