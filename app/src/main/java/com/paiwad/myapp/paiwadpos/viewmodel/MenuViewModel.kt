package com.paiwad.myapp.paiwadpos.viewmodel

import android.view.View
import androidx.lifecycle.*
import androidx.navigation.findNavController
import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.mappers.option.OptionToEntityMapper
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.model.OptionUI
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import com.paiwad.myapp.paiwadpos.view.ui.MenuFragmentDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MenuViewModel @Inject constructor() : ViewModel() {

    val _productCount = MutableLiveData(1)
    val productCount: LiveData<Int> = _productCount
    val _optionUI = MutableLiveData<OptionUI?>()
    val _totalProductPrice = MutableLiveData<Double?>()
    val totalProductPrice: LiveData<Double?> = _totalProductPrice
    private val productPrice = MutableLiveData<Double>()

    private val _message = MutableLiveData<SingleEvent<String>>()
    val message: LiveData<SingleEvent<String>> = _message

    fun setInitPrice(price: Double) {
        productPrice.value = price
    }

    fun setInitPrice(optionUI: OptionUI) {
        _optionUI.value = optionUI
        productPrice.value = optionUI.optionPrice.toDouble()
    }

    fun minusProduct() {
        var number = getNumberValue()
        if (number > 1) {
            number -= 1
            _productCount.value = number
            calcPrice()
        }
    }

    fun plusProduct() {
        var number = getNumberValue()
        if (productPrice.value!! > 0) {
            number += 1
            _productCount.value = number
            calcPrice()
        }
    }

    fun calcPrice() {
        productPrice.value?.let {
            _totalProductPrice.value = it * getNumberValue()
        }
    }

    private fun getNumberValue(): Int = _productCount.value!!.toInt()

    fun toBadgeEntityMapper(productUI: ProductUI) = BadgeEntity(
            badgeProductCode = productUI.productCode,
            badgeCount = _productCount.value!!,
            badgeOptionCode = _optionUI.value?.optionCode,
            badgeOptionName = _optionUI.value?.optionName,
            badgeOptionPrice = _optionUI.value?.optionPrice?.toDouble()
    )

    fun goToManageFragment(v: View){
        v.findNavController().navigate(MenuFragmentDirections.actionNavMenuToNavPin())
    }
}