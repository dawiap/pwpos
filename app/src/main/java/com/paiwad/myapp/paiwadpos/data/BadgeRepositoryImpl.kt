package com.paiwad.myapp.paiwadpos.data

import android.util.Log
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.db.dao.BadgeDao
import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.data.repository.BadgeRepository
import com.paiwad.myapp.paiwadpos.util.state.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class BadgeRepositoryImpl(
    private val badgeDao: BadgeDao,
    private val firebaseRef: FirebaseDatabaseReference
    ):
        BadgeRepository {

    override suspend fun getBadge(): Flow<Result<List<BadgeWithProduct>>> = flow {
        try {
            val badges = badgeDao.getBadgesWithProductsAndOptions()
            emit(Result.Success(badges))
        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    override suspend fun updateProductInBadge(products: BadgeEntity): Flow<Result<Boolean>> = flow {
        try {
            if (products.badgeCount == 0) {
                val id = badgeDao.removeProductInBadge(products)
                Log.i("Badge","removed, $id")
                if(id==1)
                    emit(Result.Success(true))
            } else {
                badgeDao.updateProductCountInBadge(products.badgeCount, products.badgeId)
                Log.i("Badge","updated -> ${products.badgeCount}")
                emit(Result.Success(true))
            }

        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    override suspend fun addProductToBadge(products: BadgeEntity): Flow<Result<Boolean>> = flow {
        try {
            val badgeEntity: BadgeEntity? = if (products.badgeOptionCode.isNullOrEmpty())
                badgeDao.hasProductWhitProductCode(products.badgeProductCode) else
                badgeDao.hasProductWhitOptionCode(products.badgeOptionCode!!)

            badgeEntity?.let {
                products.badgeCount += badgeEntity.badgeCount
                products.badgeId = badgeEntity.badgeId
                badgeDao.updateProductCountInBadge(products.badgeCount, products.badgeId)
                Log.i("Badge", "updated -> ${products.badgeCount}")
                emit(Result.Success(true))
            } ?: kotlin.run {
                badgeDao.insertProductToBadge(products)
                Log.i("Badge", "added")
                emit(Result.Success(true))
            }

        } catch (e: Exception) {
            emit(Result.Error(e))
        }
    }

    override suspend fun clearBadge() = badgeDao.clearBadge()
}