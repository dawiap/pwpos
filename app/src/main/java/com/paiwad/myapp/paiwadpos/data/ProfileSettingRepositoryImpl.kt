package com.paiwad.myapp.paiwadpos.data

import android.net.Uri
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.FirebaseStorageReference
import com.paiwad.myapp.paiwadpos.data.db.dao.UserDao
import com.paiwad.myapp.paiwadpos.data.mappers.user.UserEntityNetworkNullMapper
import com.paiwad.myapp.paiwadpos.data.mappers.user.UserUIModelMapper
import com.paiwad.myapp.paiwadpos.data.repository.ProfileSettingRepository
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.UserUI
import io.reactivex.Completable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlin.Exception

class ProfileSettingRepositoryImpl(
    private val userDao: UserDao,
    private val firebaseRef: FirebaseDatabaseReference,
    private val storageRef: FirebaseStorageReference
    ): ProfileSettingRepository {
    override fun uploadChangeImageUser(uri: Uri): Completable = Completable.create { emitter ->
        val respUri = storageRef.upLoadImageUserToFirebaseStorage(uri).blockingFirst()
        if (respUri != null) {
            try {
                CoroutineScope(Dispatchers.IO).launch {
                    userDao.updateProfileImage(respUri.toString())
                    emitter.onComplete()
                }
            } catch (e: Exception) {
                emitter.onError(e)
            }
        } else {
            emitter.onError(Throwable("Not Uri, Upload failed."))
        }
    }

    override fun updateUserProfile(userUI: UserUI): Completable = Completable.create { emitter ->
        val userModel = UserUIModelMapper().downstream(userUI)
        val resp = firebaseRef.updateUserLogout(userModel).blockingAwait()
        if (resp != null) {
            CoroutineScope(Dispatchers.IO).launch {
                firebaseRef.getDataUser(userUI.uid)
                    .onEach {
                        when (it) {
                            is Result.Success -> {
                                try {
                                    val userEntity = UserEntityNetworkNullMapper().upstream(it.data)
                                    val respId = userDao.updateProfile(userEntity!!)
                                    println(respId)
                                } catch (e: Exception) {
                                    emitter.onError(e)
                                }
                            }
                            else -> {
                            }
                        }
                    }
                    .collect {
                        when (it) {
                            is Result.Success -> {
                                emitter.onComplete()
                            }
                            is Result.Error -> {
                                emitter.onError(it.exception)
                            }
                        }
                    }
            }
        }
    }

}