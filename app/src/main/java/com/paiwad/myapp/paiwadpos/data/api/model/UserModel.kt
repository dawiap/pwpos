package com.paiwad.myapp.paiwadpos.data.api.model
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class UserModel (
    var uid: String? = null,
    var email: String? = null,
    var branchName: String? = null,
    var phone: String? = null,
    var photoUrl: String? = null,
    var signOn: Int? = null,
    var PIN: String? = null
        )