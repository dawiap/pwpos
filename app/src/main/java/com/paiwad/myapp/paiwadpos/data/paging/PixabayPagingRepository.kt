package com.paiwad.myapp.paiwadpos.data.paging

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.paiwad.myapp.paiwadpos.data.api.PixabayApiService
import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import kotlinx.coroutines.flow.Flow

class PixabayPagingRepository(private val apiService: PixabayApiService) {

    fun getSearchResultStream(query: String): Flow<PagingData<Hit>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { PixabayPagingSource(apiService, query) }
        ).flow
    }


    companion object {
        private const val NETWORK_PAGE_SIZE = 20
    }

}