package com.paiwad.myapp.paiwadpos.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category")
data class CategoryEntity (
    @PrimaryKey(autoGenerate = true)
    val categoryId: Long = 0,
    val categoryCode: String? = null,
    val categoryName: String,
    val categoryColor: String,
    val branchId: String
        )
