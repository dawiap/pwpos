package com.paiwad.myapp.paiwadpos.data

import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.mappers.order.OrderToUIMapper
import com.paiwad.myapp.paiwadpos.data.repository.OrderRepository
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import io.reactivex.Completable
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

class OrderRepositoryImpl(
    private val firebaseDatabaseReference: FirebaseDatabaseReference
): OrderRepository {

    override fun getOrders(branchId: String): Flow<Result<List<OrderUI>>> = flow {
        firebaseDatabaseReference.getDataOrders(branchId).collect {
            when (it) {
                is Result.Success -> {
                    val orderUI = it.data?.map {orderModel -> OrderToUIMapper().toData(orderModel) }
                    emit(Result.Success(orderUI))
                }
                is Result.Error -> {
                    emit(Result.Error(it.exception))
                }
            }
        }
    }

    override fun getOrderByOrderId(orderId: String): Flow<Result<OrderUI>> = flow{
        firebaseDatabaseReference.getDataOrderByOrderId(orderId).collect {
            when (it) {
                is Result.Success -> {
                    val orderUI = OrderToUIMapper().toData(it.data!!)
                    emit(Result.Success(orderUI))
                }
                is Result.Error -> {
                    emit(Result.Error(it.exception))
                }
            }
        }
    }

    override fun addOrder(orderModel: OrderModel): Completable {
        return firebaseDatabaseReference.addDataOrder(orderModel)
    }

    override fun updateOrderToFb(orderModel: OrderModel): Completable {
        return firebaseDatabaseReference.updateDataOrder(orderModel)
    }

    override fun cancelOrder(orderModel: OrderModel): Completable {
        TODO("Not yet implemented")
    }
}