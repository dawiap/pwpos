package com.paiwad.myapp.paiwadpos.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.databinding.ShrProductListBinding
import com.paiwad.myapp.paiwadpos.view.model.ProductUI

class ProductListAdapter: RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    lateinit var clickListener: ((View, ProductUI, Int) -> Unit)

    inner class ProductViewHolder(val binding: ShrProductListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ProductUI, position: Int, clickListener: (View, ProductUI, Int) -> Unit) {
            binding.product = item
            binding.root.setOnClickListener {
                clickListener(binding.root, item, position)
            }
        }
    }

    private val callBack = object : DiffUtil.ItemCallback<ProductUI>() {
        override fun areItemsTheSame(oldItem: ProductUI, newItem: ProductUI): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ProductUI, newItem: ProductUI): Boolean {
            return oldItem.productId == newItem.productId
        }

    }

    val differ = AsyncListDiffer(this, callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
                ShrProductListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(differ.currentList[position], position, clickListener)
    }

    override fun getItemCount(): Int = differ.currentList.size
}