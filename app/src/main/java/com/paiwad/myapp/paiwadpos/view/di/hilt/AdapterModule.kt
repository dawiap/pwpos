package com.paiwad.myapp.paiwadpos.view.di.hilt

import com.paiwad.myapp.paiwadpos.view.adapter.*
import com.paiwad.myapp.paiwadpos.view.adapter.PixabayPagingListAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AdapterModule {
    @Singleton
    @Provides
    fun providePixabayPagingListAdapter(): PixabayPagingListAdapter {
        return PixabayPagingListAdapter()
    }

    @Singleton
    @Provides
    fun providePixabayListAdapter(): PixabayListAdapter {
        return PixabayListAdapter()
    }

    @Singleton
    @Provides
    fun provideProductListAdapter(): ProductListAdapter {
        return ProductListAdapter()
    }

    @Singleton
    @Provides
    fun provideProductCardAdapter(): ProductCardAdapter {
        return ProductCardAdapter()
    }

    @Singleton
    @Provides
    fun provideCategoryAddItemAdapter(): CategoryAddItemAdapter {
        return CategoryAddItemAdapter()
    }

    @Singleton
    @Provides
    fun provideUnitAddItemAdapter(): UnitAddItemAdapter {
        return UnitAddItemAdapter()
    }

    @Singleton
    @Provides
    fun provideCategoryDropDownAdapter(): CategoryDropDownAdapter {
        return CategoryDropDownAdapter()
    }

    @Singleton
    @Provides
    fun provideUnitDropDownAdapter(): UnitDropDownAdapter {
        return UnitDropDownAdapter()
    }

    @Singleton
    @Provides
    fun provideBadgeProductListAdapter(): BadgeProductListAdapter {
        return BadgeProductListAdapter()
    }

    @Singleton
    @Provides
    fun provideOrderListAdapter(): OrderListAdapter {
        return OrderListAdapter()
    }

    @Singleton
    @Provides
    fun provideOrderDetailListAdapter(): OrderDetailListAdapter {
        return OrderDetailListAdapter()
    }

    @Singleton
    @Provides
    fun provideSettingListAdapter(): SettingListAdapter {
        return SettingListAdapter()
    }
}