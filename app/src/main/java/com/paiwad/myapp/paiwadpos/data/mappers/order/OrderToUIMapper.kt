package com.paiwad.myapp.paiwadpos.data.mappers.order

import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.mappers.base.SingleMapper
import com.paiwad.myapp.paiwadpos.view.model.OptionUI
import com.paiwad.myapp.paiwadpos.view.model.OrderDetailUI
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import java.text.SimpleDateFormat

class OrderToUIMapper: SingleMapper<OrderModel, OrderUI> {

    override fun toData(currentLayerEntity: OrderModel) = OrderUI(
            orderCode = currentLayerEntity.orderId!!,
            orderDate = currentLayerEntity.orderDate!!,
            branchId = currentLayerEntity.branchId!!,
            totalAmount = currentLayerEntity.totalAmount!!,
            isPayment = currentLayerEntity.isPayment!!,
            isCancelOrder = currentLayerEntity.isCancelOrder!!,
            cashReceived = currentLayerEntity.cashReceived,
            cashChange = currentLayerEntity.cashChange,
            orderDetailModel = currentLayerEntity.orderDetailModel!!.map {
                OrderDetailUI(
                        orderDetailCode = it.orderDetailId!!,
                        productCode = it.productCode!!,
                        productName = it.productName!!,
                        productPrice = it.productPrice!!.toDouble(),
                        productCount = it.productCount!!,
                        productTotalAmount = it.productTotalAmount!!,
                        imagePath = it.imagePath!!,
                        optionModel = it.optionModel?.let { opt ->
                            OptionUI(
                                    optionCode = opt.optionId,
                                    optionName = opt.optionName!!,
                                    optionPrice = opt.optionPrice!!.toDouble()
                            )
                        }
                )
            }
    )
}