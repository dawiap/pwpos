package com.paiwad.myapp.paiwadpos.view.model

data class ProductUI (
    val productId: Long? = null,
    val productCode: String,
    val categoryCode: String,
    val unitUI: UnitUI,
    val productName: String,
    val imagePath: String?,
    val productPrice: Double,
    val inStock: Int,
    val outStock: Int,
    val demandStock: Int,
    val branchId: String,
    var isStock: Boolean,
    var isOption: Boolean,
    var optionUI: List<OptionUI>?
        )
