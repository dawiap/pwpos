package com.paiwad.myapp.paiwadpos.data.model


const val UserNode = "users"
const val BranchUserNode = "branches"
const val CustomerUserNode = "customers"
const val CategoryNode = "categories"
const val ProductNode = "products"
const val UnitNode = "units"
const val OrderNode = "orders"
const val OrderDetailNode = "orderDetails"