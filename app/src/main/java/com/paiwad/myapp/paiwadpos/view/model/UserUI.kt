package com.paiwad.myapp.paiwadpos.view.model

data class UserUI (
    val userId: Long? = 0,
    val uid: String,
    var email: String,
    var branchName: String,
    var phone: String,
    val photoUrl: String?,
    var signOn: Int,
    var PIN: String
        )