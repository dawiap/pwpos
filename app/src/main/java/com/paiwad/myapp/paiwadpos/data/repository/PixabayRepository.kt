package com.paiwad.myapp.paiwadpos.data.repository

import com.paiwad.myapp.paiwadpos.data.api.model.Hit
import com.paiwad.myapp.paiwadpos.util.state.Result

interface PixabayRepository {
    suspend fun searchImage(q: String): Result<List<Hit>>
}