package com.paiwad.myapp.paiwadpos.view.model

data class UnitUI (
    val unitId: Long? = null,
    val unitCode: String,
    val unitName: String,
    val branchId: String
    )
