package com.paiwad.myapp.paiwadpos.data.mappers.base

interface NullMapper<From, To> {
    fun downstream(currentLayerEntity: From): To

    fun upstream(nextLayerEntity: To?): From?
}