package com.paiwad.myapp.paiwadpos.view.ui.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.GoogleAuthProvider
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.ActivityLoginBinding
import com.paiwad.myapp.paiwadpos.databinding.BottomResetPasswordLayoutBinding
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.AlertDialogBuilder
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.view.ui.MainActivity
import com.paiwad.myapp.paiwadpos.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), TextWatcher, UserAuthListener, View.OnClickListener {
    @Inject
    lateinit var progressBuilder: ProgressBuilder
    @Inject
    lateinit var alertDialogBuilder: AlertDialogBuilder

    private val userViewModel: UserViewModel by viewModels()

    private lateinit var binding: ActivityLoginBinding

    private lateinit var bottomResetPasswordView: BottomResetPasswordLayoutBinding

    private lateinit var bottomDialog: BottomSheetDialog

    private lateinit var googleClient: GoogleSignInClient

    private lateinit var mCallbackManager: CallbackManager

    companion object{
        private const val RC_SIGN_IN = 200
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_login)
        binding.lifecycleOwner = this
        binding.viewModel = userViewModel

        bottomResetPasswordView = BottomResetPasswordLayoutBinding.inflate(layoutInflater, null, false)
        bottomResetPasswordView.viewModel = userViewModel
        bottomDialog = BottomSheetDialog(this).apply {
            setContentView(bottomResetPasswordView.root)
        }
        bottomResetPasswordView.imageClose.setOnClickListener(this)

        binding.editEmail.addTextChangedListener(this)
        binding.editPassword.addTextChangedListener(this)

        binding.textButtonFogotPassword.setOnClickListener(this)
        binding.icClose.setOnClickListener(this)

        userViewModel.authListener = this

        setupSigninWithFacebook()
        setupSigninWithGoogle()
    }

    override fun onResume() {
        super.onResume()

        userViewModel.loading.observe(this, SingleEventObserver {
            if (it) {
                if (!progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    private fun setupSigninWithGoogle(){
        val googleSignInOptions = GoogleSignInOptions.Builder(
            GoogleSignInOptions.DEFAULT_SIGN_IN
        )
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleClient = GoogleSignIn.getClient(this, googleSignInOptions)

        binding.signInGoogleButton.setOnClickListener(this)
    }

    private fun setupSigninWithFacebook(){
        mCallbackManager = CallbackManager.Factory.create()
        binding.signInFacebookButton.apply {
            setReadPermissions("email", "public_profile")
            setOnClickListener(this@LoginActivity)
        }
    }

    private fun getGoogleCredential(token: String): AuthCredential {
        return GoogleAuthProvider.getCredential(token, null)
    }

    private fun getFacebookCredential(token: AccessToken): AuthCredential {
        return FacebookAuthProvider.getCredential(token.token)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mCallbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == RC_SIGN_IN){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            val exception = task.exception
            if(task.isSuccessful){
                try {

                    val token = task.getResult(ApiException::class.java)!!
                    val credential = getGoogleCredential(token.idToken!!)

                    userViewModel.loginWithGoogle(credential)

                }catch (e: ApiException){
                    Toast.makeText(this, e.message.toString(), Toast.LENGTH_LONG).show()
                }
            }else{
                Toast.makeText(this, exception.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        binding.buttonLogin.isEnabled = !userViewModel.email.isNullOrEmpty()
                && !userViewModel.password.isNullOrEmpty()
    }

    override fun afterTextChanged(s: Editable?) {
    }

    override fun onClick(v: View?) {
        when(v){
            binding.signInGoogleButton ->{
                val intent = googleClient.signInIntent
                startActivityForResult(intent, RC_SIGN_IN)
            }
            binding.textButtonFogotPassword -> {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(400)
                    bottomDialog.show()
                }
            }
            bottomResetPasswordView.imageClose ->{
                bottomDialog.dismiss()
            }
            binding.icClose ->{
                finish()
            }
            binding.signInFacebookButton ->{
                LoginManager.getInstance().registerCallback(mCallbackManager, object :
                    FacebookCallback<LoginResult?> {
                    override fun onSuccess(loginResult: LoginResult?) {

                        val credential = getFacebookCredential(loginResult!!.accessToken)

                        userViewModel.loginWithFacebook(credential)
                    }

                    override fun onCancel() {
                        Log.d("TAG", "facebook:onCancel")
                    }

                    override fun onError(exception: FacebookException) {
                        Log.d("TAG", "facebook:onError : ${exception.localizedMessage} ..")
                    }
                })
            }
        }
    }

    override fun onSuccess(successType: SuccessType) {

        when(successType){

            SuccessType.LOGIN_SUCCESS ->{
                startActivity(Intent(this, MainActivity::class.java))
            }

            SuccessType.ON_CREATE_USER ->{
                startActivity(Intent(this, CreateUserActivity::class.java))
            }

            else ->{}
        }

    }

    override fun onFailure(message: String) {
        alertDialogBuilder.showMessageAlertDialog("Failure", message)
    }

}