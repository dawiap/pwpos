package com.paiwad.myapp.paiwadpos.view.ui.setting

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.FragmentSettingBinding
import com.paiwad.myapp.paiwadpos.view.adapter.SettingListAdapter
import com.paiwad.myapp.paiwadpos.view.model.MenuSettingUI
import com.paiwad.myapp.paiwadpos.viewmodel.SettingViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SettingFragment : Fragment() {

    private val settingViewModel: SettingViewModel by viewModels()

    @Inject lateinit var settingListAdapter: SettingListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        settingViewModel.setItemsSetting(menuSettings())
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentSettingBinding.bind(view)
        binding.viewModel = settingViewModel
        binding.lifecycleOwner = this

        binding.rcvSetting.adapter = settingListAdapter
        settingListAdapter.onItemClickListener = {it -> onItemClick(it)}
    }

    private fun onItemClick(menuSettingUI: MenuSettingUI){
        when(menuSettingUI.menuName){
            getString(R.string.setting_profile) -> {
                findNavController().navigate(SettingFragmentDirections.actionNavSettingToProfileSettingActivity())
            }
            getString(R.string.setting_pin) -> {
                findNavController().navigate(SettingFragmentDirections.actionNavSettingToPinSettingActivity())
            }
            else ->{}
        }
    }

    private fun menuSettings() = listOf(
        MenuSettingUI(
            menuName = getString(R.string.setting_profile),
            menuIcon = R.drawable.ic_action_person
        ),
        MenuSettingUI(
            menuName = getString(R.string.setting_pin),
            menuIcon = R.drawable.ic_action_keys
        )
    )
}