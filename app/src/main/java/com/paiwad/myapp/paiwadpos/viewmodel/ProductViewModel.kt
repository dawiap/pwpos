package com.paiwad.myapp.paiwadpos.viewmodel

import android.net.Uri
import android.util.Log
import androidx.lifecycle.*
import com.paiwad.myapp.paiwadpos.data.api.model.OptionModel
import com.paiwad.myapp.paiwadpos.data.api.model.ProductModel
import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.data.repository.ProductRepository
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.view.model.OptionUI
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val productRepository: ProductRepository
): ViewModel() {

    val productName = MutableLiveData<String?>()
    val productPrice = MutableLiveData<String?>()
    val productInStock = MutableLiveData<String?>()
    val isStock = MutableLiveData(false)
    val isOther = MutableLiveData(false)

    val optionItems = MutableLiveData<List<OptionUI>>()

    var categoryCode: String? = null
    var unitUiItem: UnitUI? = null
    var uploadImageUri: String? = null
    var branchId: String? = null

    var imageUri: Uri? = null

    var authListener: UserAuthListener? = null

    private val _productList = MutableLiveData<List<ProductUI>>()

    val productList: LiveData<List<ProductUI>> = _productList

    private val _loading = MutableLiveData<SingleEvent<Boolean>>()

    val loading: LiveData<SingleEvent<Boolean>> = _loading

    private val disposables = CompositeDisposable()

    private fun addProductToFb() {
        if (productName.value.isNullOrEmpty() || (isOther.value == false && productPrice.value.isNullOrEmpty())
                || branchId.isNullOrEmpty() || (isStock.value == true && productInStock.value.isNullOrEmpty())
                || categoryCode.isNullOrEmpty() || unitUiItem == null || uploadImageUri.isNullOrEmpty()
                || (isOther.value == true && optionItems.value.isNullOrEmpty())) {
            authListener?.onFailure("Please enter require field")
            return
        }

        _loading.value = SingleEvent(true)
        viewModelScope.launch {
            try {
                productRepository.addProduct(toProductModel())
                        .collect {
                            _loading.value = SingleEvent(false)

                            updateCache(branchId!!,categoryCode!!)

                            authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                            clearText()
                        }
            } catch (ex: CancellationException) {
                throw  ex
            } catch (e: Exception) {
                _loading.value = SingleEvent(false)
                authListener?.onFailure(e.message!!)
            }
        }
    }

    fun getProductsFromFb(branchId: String, categoryCode: String) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        productRepository.getProducts(branchId, categoryCode).collect { result ->
            _loading.value = SingleEvent(false)
            when (result) {
                is Result.Success -> {
                    authListener?.onSuccess(SuccessType.DATA_SUCCESS)
                    result.data?.let { _productList.value = it }
                }
                is Result.Error -> {
                    authListener?.onFailure(result.exception.message!!)
                }
            }
        }
    }

    fun uploadImageToFbStorage() {
        if (imageUri == null) {
            addProductToFb()
        } else {
            _loading.value = SingleEvent(true)
            val disposable = productRepository.uploadImageToFbStorage(imageUri!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        _loading.value = SingleEvent(false)
                        uploadImageUri = it.toString()
                        addProductToFb()
                    }, {
                        _loading.value = SingleEvent(false)
                        authListener?.onFailure(it.message!!)
                    })

            disposables.add(disposable)
        }
    }

    fun deleteProductInFb(product: ProductUI) = viewModelScope.launch {
        _loading.value = SingleEvent(true)
        try {
            productRepository.deleteProduct(product)
                    .collect {
                        if (it) {
                            _loading.value = SingleEvent(false)

                            updateCache(product.branchId, product.categoryCode)
                        }
                    }
        } catch (ex: CancellationException) {
            throw  ex
        } catch (e: Exception) {
            _loading.value = SingleEvent(false)
            authListener?.onFailure(e.message!!)
        }
    }

    private fun updateCache(branchId: String, categoryCode: String) = viewModelScope.launch {

        try {
            productRepository.updateCache(branchId, categoryCode).collect {
                when (it) {
                    is Result.Success -> {
                        getProductsFromFb(branchId, categoryCode)
                    }
                    is Result.Error -> {
                        Log.e("VM Update Error", it.exception.message.toString())
                    }
                }
            }
        }catch (e: Exception){
            Log.e("VM Update Error", e.message.toString())
        }
    }

    private fun clearText() {
        productName.value = null
        productPrice.value = null
        productInStock.value = null
        categoryCode = null
        unitUiItem = null
        uploadImageUri = null
        imageUri = null
        isOther.value = false
        isStock.value = false
    }

    private fun toProductModel() = ProductModel(
            categoryCode = categoryCode!!,
            productPrice = (if (isOther.value == true) 0 else productPrice.value!!).toString(),
            productName = productName.value!!,
            imagePath = uploadImageUri!!,
            branchId = branchId!!,
            inStock = if (isStock.value == true) productInStock.value!!.toInt() else 0,
            outStock = 0,
            demandStock = 0,
            unitModel = UnitModel(
                    unitId = unitUiItem?.unitCode,
                    unitName = unitUiItem?.unitName,
                    branchId = unitUiItem?.branchId
            ),
            isStock = isStock.value!!,
            isOption = isOther.value!!,
            options = toOptionModel()
    )

    private fun toOptionModel() = optionItems.value?.map {
        OptionModel(
                optionName = it.optionName,
                optionPrice = it.optionPrice.toString(),
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}