package com.paiwad.myapp.paiwadpos.util.view

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.CustomAlertDialogBinding

class AlertDialogBuilder(activity: Activity) {

    private var customDialog: AlertDialog = AlertDialog.Builder(activity, 0).create()
    private var positiveClickListener: PositiveClickListener? = null

    private var dialogBinding: CustomAlertDialogBinding =
            DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.custom_alert_dialog, null, false)

    init {
        customDialog.apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setView(dialogBinding.root)
            setCancelable(false)
        }
    }

    fun showAlertDialog(title: String, message: String, positiveButton: String, negativeButton: String) {

        customDialog.show()

        dialogBinding.tvTitle.text = title
        dialogBinding.tvMessage.text = message
        dialogBinding.buttonPositive.apply {
            text = positiveButton
            setOnClickListener {
                customDialog.dismiss()
                positiveClickListener?.positiveButtonClick()
            }
        }
        dialogBinding.buttonNevigation.apply {
            text = negativeButton
            setOnClickListener {
                customDialog.dismiss()
            }
        }
    }

    fun showMessageAlertDialog(title: String, message: String) {

        customDialog.show()

        dialogBinding.tvTitle.text = title
        dialogBinding.tvMessage.text = message
        dialogBinding.buttonPositive.apply {
            text = resources.getString(R.string.text_button_ok)
            setOnClickListener {
                customDialog.dismiss()
                positiveClickListener?.positiveButtonClick()
            }
        }
        dialogBinding.buttonNevigation.visibility = View.GONE
    }

    fun setPositiveClickListener(listener: PositiveClickListener){
        positiveClickListener = listener
    }

    interface PositiveClickListener{
        fun positiveButtonClick()
    }
}