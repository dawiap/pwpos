package com.paiwad.myapp.paiwadpos.util

import android.app.Application
import com.paiwad.myapp.paiwadpos.view.di.*
import com.paiwad.myapp.paiwadpos.view.di.koin.KoinModule
import dagger.hilt.android.HiltAndroidApp
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

@HiltAndroidApp
class PWPOSApp: Application(){
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@PWPOSApp)
            modules(listOf(KoinModule))
        }
    }
}