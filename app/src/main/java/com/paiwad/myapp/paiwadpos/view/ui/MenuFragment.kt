package com.paiwad.myapp.paiwadpos.view.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.paiwad.myapp.paiwadpos.R
import com.paiwad.myapp.paiwadpos.databinding.*
import com.paiwad.myapp.paiwadpos.util.SingleEvent
import com.paiwad.myapp.paiwadpos.view.model.ProductUI
import com.paiwad.myapp.paiwadpos.util.view.ProductGridItemDecoration
import com.paiwad.myapp.paiwadpos.util.state.SuccessType
import com.paiwad.myapp.paiwadpos.util.state.UserAuthListener
import com.paiwad.myapp.paiwadpos.util.view.ProgressBuilder
import com.paiwad.myapp.paiwadpos.util.SingleEventObserver
import com.paiwad.myapp.paiwadpos.view.adapter.ProductCardAdapter
import com.paiwad.myapp.paiwadpos.viewmodel.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MenuFragment : Fragment(), UserAuthListener {

    private lateinit var menuViewModel: MenuViewModel

    private lateinit var badgeViewModel: BadgeViewModel

    @Inject
    lateinit var productCardAdapter: ProductCardAdapter

    @Inject
    lateinit var progressBuilder: ProgressBuilder

    private lateinit var userViewModel: UserViewModel

    private lateinit var categoryViewModel: CategoryViewModel

    private lateinit var productViewModel: ProductViewModel

    private lateinit var binding: FragmentMenuBinding

    private var UID: String? = null

    private lateinit var bottomSheetAddToCartBinding: BottomSheetAddToCartBinding

    private lateinit var bottomDialog: BottomSheetDialog

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        (activity as MainActivity).apply {
            this@MenuFragment.userViewModel = userViewModel
            this@MenuFragment.categoryViewModel = categoryViewModel
            this@MenuFragment.productViewModel = productViewModel
            this@MenuFragment.menuViewModel = menuViewModel
            this@MenuFragment.badgeViewModel = badgeViewModel
        }

        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMenuBinding.bind(view)
        binding.viewModel = menuViewModel
        binding.lifecycleOwner = this.viewLifecycleOwner

        userViewModel.authListener = this
        badgeViewModel.authListener = this

        setupBottomAddToCart()

    }

    override fun onResume() {
        super.onResume()

        getUserProfile()
        categoryViewModel.loading.observeLoading()
        productViewModel.loading.observeLoading()
        menuViewModel.message.observeMessage()
        badgeViewModel.message.observeMessage()
    }

    private fun LiveData<SingleEvent<String>>.observeMessage() {
        this.observe(viewLifecycleOwner, SingleEventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
    }

    private fun LiveData<SingleEvent<Boolean>>.observeLoading(){
        this.observe(viewLifecycleOwner, SingleEventObserver{
            if (it) {
                if (progressBuilder.isShowing())
                    progressBuilder.showProgressDialog()
            } else {
                progressBuilder.dismissProgressDialog()
            }
        })
    }

    private fun stateProgressDialog(isLoading: Boolean) {
        if (isLoading) {
            if (progressBuilder.isShowing())
                progressBuilder.showProgressDialog()
        } else {
            progressBuilder.dismissProgressDialog()
        }
    }

    private fun getUserProfile() {
        userViewModel.firebaseUser.observe(viewLifecycleOwner, {
            it?.let { _user ->
                UID = _user.uid
                initTabCategory()
            } ?: run {
                binding.layoutEmpty.visibility = View.VISIBLE
                binding.layoutMain.visibility = View.GONE
            }
        })
    }

    private fun initTabCategory() {

        binding.layoutMain.visibility = View.VISIBLE
        binding.layoutEmpty.visibility = View.GONE

        categoryViewModel.categoryList.observe(viewLifecycleOwner, { categories ->
            if (categories.isEmpty())
                binding.layoutEmpty.visibility = View.VISIBLE

            binding.tabCategory.removeAllTabs()

            categories.forEach {

                val tabBinding: CustomCategoryTabViewBinding =
                        DataBindingUtil.inflate(layoutInflater, R.layout.custom_category_tab_view, binding.tabCategory, false)

                tabBinding.category = it

                binding.tabCategory.addTab(
                        binding.tabCategory.newTab().setTag(it.categoryCode)
                                .setText(it.categoryName)
                                .setCustomView(tabBinding.root)
                )

            }
        })

        binding.tabCategory.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                val categoryCode = tab?.tag.toString()
                getProducts(categoryCode)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })

    }

    private fun getProducts(categoryCode: String) {
        productViewModel.getProductsFromFb(UID!!, categoryCode)
        productViewModel.productList.observe(viewLifecycleOwner, { products ->
            val layoutManager = GridLayoutManager(requireContext(), 2)
            binding.rcvProduct.layoutManager = layoutManager

            productCardAdapter.setItems(products)
            binding.rcvProduct.adapter = productCardAdapter

            val largePadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing)
            val smallPadding = resources.getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small)
            binding.rcvProduct.removeItemDecorations()
            binding.rcvProduct.addItemDecoration(ProductGridItemDecoration(largePadding, smallPadding))


            productCardAdapter.clickListener = {
                productClicked(it)
            }
        })
    }

    private fun RecyclerView.removeItemDecorations() {
        while (this.itemDecorationCount > 0) {
            this.removeItemDecorationAt(0)
        }
    }

    private fun setupBottomAddToCart() {
        bottomSheetAddToCartBinding = BottomSheetAddToCartBinding.inflate(layoutInflater, null, false)
        bottomSheetAddToCartBinding.viewModel = menuViewModel
        bottomSheetAddToCartBinding.badgeViewModel = badgeViewModel
        bottomSheetAddToCartBinding.lifecycleOwner = this
        bottomDialog = BottomSheetDialog(requireContext()).apply {
            setContentView(bottomSheetAddToCartBinding.root)
        }
        bottomSheetAddToCartBinding.imageClose.setOnClickListener { bottomDialog.dismiss() }
    }

    private fun productClicked(item: ProductUI) {
        menuViewModel._productCount.value = 1
        menuViewModel.setInitPrice(item.productPrice)
        menuViewModel._totalProductPrice.value = item.productPrice

        bottomSheetAddToCartBinding.product = item

        bottomSheetAddToCartBinding.parentChipGroup.removeAllViewsInLayout()
        item.optionUI?.let {
            for (i in it.indices) {
                val chip: LayoutChipOptionBinding = LayoutChipOptionBinding.inflate(
                    layoutInflater,
                    bottomSheetAddToCartBinding.parentChipGroup,
                    false
                )
                chip.option = it[i]
                chip.chip1.id = i + 1
                bottomSheetAddToCartBinding.parentChipGroup.addView(chip.root)
            }

            bottomSheetAddToCartBinding.parentChipGroup.invalidate()
        }

        val count = bottomSheetAddToCartBinding.parentChipGroup.childCount
        var v: View?
        for (i in 0 until count) {
            v = bottomSheetAddToCartBinding.parentChipGroup.getChildAt(i)
            val chip = DataBindingUtil.getBinding<LayoutChipOptionBinding>(v)
            chip?.chip1?.let {
                it.isCheckable = true
                it.tag = i
                it.setOnCheckedChangeListener { view, isChecked ->
                    for (j in 0 until count) {
                        if (view.tag != j) {
                            v = bottomSheetAddToCartBinding.parentChipGroup.getChildAt(j)
                            val chp = DataBindingUtil.getBinding<LayoutChipOptionBinding>(v!!)
                            chp!!.chip1.isChecked = false
                        }
                        view.isChecked = isChecked
                    }
                    println(chip.option)
                    menuViewModel.setInitPrice(chip.option!!)
                    menuViewModel.calcPrice()
                }
            }
        }
        bottomDialog.show()
    }

    override fun onSuccess(successType: SuccessType) {
        bottomDialog.dismiss()
        menuViewModel._optionUI.value = null
    }

    override fun onFailure(message: String) {
        Toast.makeText(requireActivity(), message, Toast.LENGTH_LONG).show()
    }

}