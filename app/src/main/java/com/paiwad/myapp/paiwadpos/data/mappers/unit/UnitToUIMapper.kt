package com.paiwad.myapp.paiwadpos.data.mappers.unit

import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.FromToMapper
import com.paiwad.myapp.paiwadpos.data.mappers.base.Mapper
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

class UnitToUIMapper: FromToMapper<UnitEntity, UnitModel, UnitUI> {

    override fun fromWayOne(data: UnitEntity) = UnitUI(
            unitName = data.unitName,
            unitCode = data.unitCode!!,
            branchId = data.branchId
    )

    override fun fromWayTwo(data: UnitModel) = UnitUI(
            unitName = data.unitName!!,
            unitCode = data.unitId!!,
            branchId = data.branchId!!
    )
}