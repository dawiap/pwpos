package com.paiwad.myapp.paiwadpos.data.mappers.user

import com.paiwad.myapp.paiwadpos.data.db.entity.UserEntity
import com.paiwad.myapp.paiwadpos.data.mappers.base.NullMapper
import com.paiwad.myapp.paiwadpos.view.model.*

class UserUIEntityNullMapper: NullMapper<UserUI, UserEntity> {
    override fun downstream(currentLayerEntity: UserUI) = UserEntity(
        uid = currentLayerEntity.uid,
        branchName = currentLayerEntity.branchName,
        email = currentLayerEntity.email,
        phone = currentLayerEntity.phone,
        photoUrl = currentLayerEntity.photoUrl,
        signOn = currentLayerEntity.signOn,
        PIN = currentLayerEntity.PIN
    )

    override fun upstream(nextLayerEntity: UserEntity?) = nextLayerEntity?.let {
        UserUI(
            uid = it.uid,
            branchName = it.branchName,
            email = it.email,
            phone = it.phone,
            photoUrl = it.photoUrl,
            signOn = it.signOn,
            PIN = it.PIN!!
        )
    }
}