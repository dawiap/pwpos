package com.paiwad.myapp.paiwadpos.data.repository

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.api.model.OrderModel
import com.paiwad.myapp.paiwadpos.data.api.model.UnitModel
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.OrderUI
import com.paiwad.myapp.paiwadpos.view.model.UnitUI
import io.reactivex.Completable
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow

interface OrderRepository {
    fun getOrders(branchId: String): Flow<Result<List<OrderUI>>>

    fun getOrderByOrderId(orderId: String): Flow<Result<OrderUI>>

    fun addOrder(orderModel: OrderModel): Completable

    fun updateOrderToFb(orderModel: OrderModel): Completable

    fun cancelOrder(orderModel: OrderModel): Completable
}