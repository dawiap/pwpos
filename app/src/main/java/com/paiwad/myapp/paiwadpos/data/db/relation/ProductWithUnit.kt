package com.paiwad.myapp.paiwadpos.data.db.relation

import androidx.room.Embedded
import androidx.room.Relation
import com.paiwad.myapp.paiwadpos.data.db.entity.OptionEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.ProductEntity
import com.paiwad.myapp.paiwadpos.data.db.entity.UnitEntity

data class ProductWithUnit(
        @Embedded val product: ProductEntity,
        @Relation(
                parentColumn = "unitCode",
                entityColumn = "unitCode"
        )
        val unit: UnitEntity
)