package com.paiwad.myapp.paiwadpos.data.api.model

import com.google.firebase.database.IgnoreExtraProperties
import com.paiwad.myapp.paiwadpos.view.model.UnitUI

@IgnoreExtraProperties
data class OrderModel (
    var orderId: String? = null,
    var orderDate: String? = null,
    var branchId: String? = null,
    var totalAmount: Float? = null,
    var isPayment: Int? = null,
    var isCancelOrder: Int? = null,
    var cashReceived: Int? = null,
    var cashChange: Float? = null,
    var orderDetailModel: List<OrderDetailModel>? = null
        )
