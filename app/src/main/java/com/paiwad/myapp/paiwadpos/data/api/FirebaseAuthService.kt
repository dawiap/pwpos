package com.paiwad.myapp.paiwadpos.data.api

import com.facebook.login.LoginManager
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Completable

class FirebaseAuthService {
    private val firebaseAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

    fun loginWithEmailAndPassword(email: String, password: String) = Completable.create {emitter ->
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                emitter.onComplete()
            }else{
                emitter.onError(task.exception!!)
            }
        }
    }

    fun loginWithGoogle(credential: AuthCredential) = Completable.create {emitter ->
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                emitter.onComplete()
            }else{
                emitter.onError(task.exception!!)
            }
        }
    }

    fun loginWithFacebook(credential: AuthCredential) = Completable.create {emitter ->
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                emitter.onComplete()
            }else{
                emitter.onError(task.exception!!)
            }
        }
    }

    fun registerWithEmailAndPassword(email: String, password: String) = Completable.create { emitter ->
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                emitter.onComplete()
            }else{
                emitter.onError(task.exception!!)
            }
        }
    }

    fun resetPasswordWithEmail(email: String)= Completable.create { emitter ->
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                emitter.onComplete()
            }else{
                emitter.onError(task.exception!!)
            }
        }
    }

    fun logout() {
        firebaseAuth.signOut()
        LoginManager.getInstance().logOut()
    }

    fun currentUser() = firebaseAuth.currentUser
}