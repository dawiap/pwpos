package com.paiwad.myapp.paiwadpos.util.state

interface UserAuthListener {

    fun onSuccess(successType: SuccessType)
    fun onFailure(message: String)
}