package com.paiwad.myapp.paiwadpos.data.db.entity

import android.os.Parcelable
import androidx.room.*
import kotlinx.parcelize.Parcelize

@Entity(tableName = "badge")
@Parcelize
data class BadgeEntity(
        @PrimaryKey(autoGenerate = true)
        var badgeId: Long = 0,
        val badgeProductCode: String,
        var badgeCount: Int,
        var badgeOptionCode: String? = null,
        var badgeOptionName: String? = null,
        var badgeOptionPrice: Double? = null
): Parcelable