package com.paiwad.myapp.paiwadpos.view.di.hilt

import com.paiwad.myapp.paiwadpos.data.*
import com.paiwad.myapp.paiwadpos.data.api.FirebaseAuthService
import com.paiwad.myapp.paiwadpos.data.api.FirebaseDatabaseReference
import com.paiwad.myapp.paiwadpos.data.api.FirebaseStorageReference
import com.paiwad.myapp.paiwadpos.data.api.PixabayApiService
import com.paiwad.myapp.paiwadpos.data.db.dao.*
import com.paiwad.myapp.paiwadpos.data.paging.PixabayPagingRepository
import com.paiwad.myapp.paiwadpos.data.repository.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Singleton
    @Provides
    fun provideCategoryRepository(
        categoryDao: CategoryDao,
        firebaseDatabaseReference: FirebaseDatabaseReference
    ): CategoryRepository {
        return CategoryRepositoryImpl(categoryDao, firebaseDatabaseReference)
    }

    @Singleton
    @Provides
    fun provideProductRepository(
        productDao: ProductDao,
        optionDao: OptionDao,
        firebaseDatabaseReference: FirebaseDatabaseReference,
        firebaseStorageReference: FirebaseStorageReference
    ): ProductRepository {
        return ProductRepositoryImpl(
            productDao,
            optionDao,
            firebaseDatabaseReference,
            firebaseStorageReference
        )
    }

    @Singleton
    @Provides
    fun provideUnitRepository(
        unitDao: ProductUnitDao,
        firebaseDatabaseReference: FirebaseDatabaseReference
    ): UnitRepository {
        return UnitRepositoryImpl(unitDao, firebaseDatabaseReference)
    }

    @Singleton
    @Provides
    fun providePixabayPagingRepository(apiService: PixabayApiService): PixabayPagingRepository {
        return PixabayPagingRepository(apiService)
    }

    @Singleton
    @Provides
    fun providePixabayRepository(apiService: PixabayApiService): PixabayRepository {
        return PixabayRepositoryImpl(apiService)
    }

    @Singleton
    @Provides
    fun provideUserRepository(
        firebaseAuthService: FirebaseAuthService,
        firebaseDatabaseReference: FirebaseDatabaseReference,
        userDao: UserDao,
        categoryDao: CategoryDao,
        productDao: ProductDao,
        optionDao: OptionDao,
        unitDao: ProductUnitDao
    ): UserRepository {
        return UserRepositoryImpl(
            firebaseAuthService,
            firebaseDatabaseReference,
            userDao,
            categoryDao,
            productDao,
            optionDao,
            unitDao
        )
    }

    @Singleton
    @Provides
    fun provideCartRepository(
        badgeDao: BadgeDao,
        firebaseDatabaseReference: FirebaseDatabaseReference
    ): BadgeRepository {
        return BadgeRepositoryImpl(badgeDao, firebaseDatabaseReference)
    }

    @Singleton
    @Provides
    fun provideOrderRepository(
        firebaseDatabaseReference: FirebaseDatabaseReference
    ): OrderRepository {
        return OrderRepositoryImpl(firebaseDatabaseReference)
    }

    @Singleton
    @Provides
    fun provideProfileSettingRepository(
        userDao: UserDao,
        firebaseDatabaseReference: FirebaseDatabaseReference,
        firebaseStorageReference: FirebaseStorageReference
    ): ProfileSettingRepository {
        return ProfileSettingRepositoryImpl(userDao, firebaseDatabaseReference, firebaseStorageReference)
    }
}