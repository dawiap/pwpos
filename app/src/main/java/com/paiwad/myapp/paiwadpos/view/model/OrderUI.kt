package com.paiwad.myapp.paiwadpos.view.model

import java.io.Serializable

data class OrderUI(
        val orderId: Long? = null,
        val orderCode: String,
        var orderDate: String,
        var branchId: String,
        var isPayment: Int,
        var isCancelOrder: Int,
        var totalAmount: Float,
        var cashReceived: Int?,
        var cashChange: Float?,
        var orderDetailModel: List<OrderDetailUI>
): Serializable
