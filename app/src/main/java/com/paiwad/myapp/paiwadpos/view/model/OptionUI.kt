package com.paiwad.myapp.paiwadpos.view.model

import java.io.Serializable

data class OptionUI(
        val optionId: Long? = null,
        val optionCode: String? = null,
        val productCode: String? = null,
        val optionName: String,
        val optionPrice: Double
): Serializable
