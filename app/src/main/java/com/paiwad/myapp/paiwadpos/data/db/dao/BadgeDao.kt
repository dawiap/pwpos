package com.paiwad.myapp.paiwadpos.data.db.dao

import androidx.room.*
import com.paiwad.myapp.paiwadpos.data.db.entity.BadgeEntity
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct

@Dao
interface BadgeDao {
    @Transaction
    @Query("SELECT * FROM badge")
    suspend fun getBadgesWithProductsAndOptions(): List<BadgeWithProduct>

    @Insert
    suspend fun insertProductToBadge(badges: BadgeEntity)

    @Query("UPDATE badge set badgeCount = :badgeCount WHERE badgeId IN (:badgeId)")
    suspend fun updateProductCountInBadge(badgeCount: Int, badgeId: Long)

    @Query("SELECT * FROM badge WHERE badgeProductCode IN(:productCode)")
    suspend fun hasProductWhitProductCode(productCode: String): BadgeEntity?

    @Query("SELECT * FROM badge WHERE badgeOptionCode IN(:optionCode)")
    suspend fun hasProductWhitOptionCode(optionCode: String): BadgeEntity?

    @Delete
    suspend fun removeProductInBadge(badges: BadgeEntity): Int

    @Query("DELETE FROM badge")
    suspend fun clearBadge()
}