package com.paiwad.myapp.paiwadpos.util.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.paiwad.myapp.paiwadpos.data.db.relation.BadgeWithProduct
import com.paiwad.myapp.paiwadpos.util.view.SimpleItemDecor
import com.paiwad.myapp.paiwadpos.view.adapter.*
import com.paiwad.myapp.paiwadpos.view.model.*

@BindingAdapter("app:itemsListProduct")
fun setItemsListProduct(rcv: RecyclerView, items: List<ProductUI>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as ProductListAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("app:itemsListBadge")
fun setItemsListBadge(rcv: RecyclerView, items: List<BadgeWithProduct>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as BadgeProductListAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("app:itemsOrderList")
fun setItemsOrderList(rcv: RecyclerView, items: List<OrderUI>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as OrderListAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("app:itemsOrderDetailList")
fun setItemsOrderDetailList(rcv: RecyclerView, items: List<OrderDetailUI>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as OrderDetailListAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("app:itemsListCategory")
fun setItemsListCategory(rcv: RecyclerView, items: List<CategoryUI>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as CategoryAddItemAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("app:itemsListUnit")
fun setItemsListUnit(rcv: RecyclerView, items: List<UnitUI>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as UnitAddItemAdapter).apply {
            differ.submitList(data)
            notifyDataSetChanged()
        }
    }
}

@BindingAdapter("app:itemsListSetting")
fun setItemsListSetting(rcv: RecyclerView, items: List<MenuSettingUI>?){
    val itemDecor = SimpleItemDecor(rcv.context)
    rcv.addItemDecoration(itemDecor)
    items?.let { data ->
        (rcv.adapter as SettingListAdapter).items = data
        (rcv.adapter as SettingListAdapter).notifyDataSetChanged()
    }
}


