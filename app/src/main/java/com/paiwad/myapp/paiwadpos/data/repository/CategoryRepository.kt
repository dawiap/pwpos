package com.paiwad.myapp.paiwadpos.data.repository

import com.paiwad.myapp.paiwadpos.data.api.model.CategoryModel
import com.paiwad.myapp.paiwadpos.data.api.model.ProductModel
import com.paiwad.myapp.paiwadpos.data.db.entity.CategoryEntity
import com.paiwad.myapp.paiwadpos.util.state.Result
import com.paiwad.myapp.paiwadpos.view.model.CategoryUI
import io.reactivex.Completable
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow

interface CategoryRepository {

    suspend fun getCategories(branchId: String): Flow<Result<List<CategoryUI>>>

    fun addCategory(categoryUI: CategoryUI): Completable

    fun deleteCategory(categoryUI: CategoryUI): Completable

    fun checkProductBeforeDeleteCategory(categoryId: String): Observable<Boolean>

    fun updateCategoryInFb(categoryUI: CategoryUI): Completable

    suspend fun updateCache(branchId: String): Flow<Result<List<CategoryModel>>>
}